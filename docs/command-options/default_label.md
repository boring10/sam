# default_label

## Alias

- `--default_label`
- `--d_l`
- `--dl`
- `--defaultlabel`

## statuslabels patch byid

- **Description:** Upddate whether it should be bubbled up to the top of the list of available statuses.
- **Type:** boolean

## statuslabels post create

- **Description:** Determine whether it should be bubbled up to the top of the list of available statuses.
- **Default:** false
- **Type:** boolean

## Usage

```bash
# Set to true
--default_label

# Set to false
--default_label false
# or
--no-default_label
```

