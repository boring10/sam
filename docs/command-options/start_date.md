# start_date

## Alias

- `--start_date`
- `--startdate`

## maintenances post create

- **Description:** Date the the maintenance was started or logged.
- **Required:** true
- **Type:** string

## Usage

```bash
--start_date "2021-02-19"
```

