# limit

## Alias

- `--limit`
- `-l`

## accessories get checkedout byid

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## accessories get search

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## categories get search

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## companies get search

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## components get assets byid

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## components get search

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## consumables get byid

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## departments get search

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## hardware get audit due

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## hardware get audit overdue

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## hardware get licenses byid

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## hardware get search

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## licenses get search

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## locations get search

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## maintenances get search

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## manufacturers get search

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## models get search

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## statuslabels get assetlist byid

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## statuslabels get search

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## users get accessories byid

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## users get assets byid

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## users get licenses byid

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## users get search

- **Description:** Specify the number of results you with to return.
- **Default:** 50
- **Type:** string

## Usage

```bash
--limit 50
```

