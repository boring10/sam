# show_in_nav

## Alias

- `--show_in_nav`
- `--s_i_n`
- `--sin`
- `--showinnav`

## statuslabels patch byid

- **Description:** Update whether the status label should show in the left-side nav of the web GUI.
- **Type:** boolean

## statuslabels post create

- **Description:** Determine whether the status label should show in the left-side nav of the web GUI.
- **Default:** true
- **Type:** boolean

## Usage

```bash
# Set to true
--show_in_nav

# Set to false
--show_in_nav false
# or
--no-show_in_nav
```

