# api_version

- [Alias](#alias)
- [config profiles create](#config-profiles-create)
- [config profiles update](#config-profiles-update)
- [Usage](#usage)

## Alias

- `--api_version`
- `--a_v`
- `--apiversion`

## config profiles create

- **Description:** The API version for this profile to use.
- **Default:** v1
- **Choices:** v1
- **Type:** string

## config profiles update

- **Description:** The API version for this profile to use.
- **Choices:** v1
- **Type:** string

## Usage

If `--api_version` is omitted then the default value is used for that command. Only applies if a default is set for that command.

```bash
--api_version "v1"
```
