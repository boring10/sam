# currency

## Alias

- `--cy`
- `--cur`
- `--curr`

## locations patch byid

- **Description:** Update the currency type for the location. The default is based off of your localization setting in your Snipe-IT instance.
- **Type:** string

## locations post create

- **Description:** The currency type for the location. The default is based off of your localization setting in your Snipe-IT instance.
- **Type:** string

## Usage

```bash
--currency "USD"
```

