# checkin_email

- [Alias](#alias)
- [categories patch byid](#categories-patch-byid)
- [categories post create](#category-post-create)
- [Usage](#usage)

## Alias

- `--checkin_email`
- `--checkinemail`

## categories patch byid

- **Description:** Should the user be emailed the EULA and/or an acceptance confirmation email when this item is checked in/out.
- **Type:** boolean

## categories post create

- **Description:** Should the user be emailed the EULA and/or an acceptance confirmation email when this item is checked in/out.
- **Default:** false
- **Type:** boolean

## Usage

```bash
# Set to true
--checkin_email

# Set to false
--checkin_email false
# or
--no-checkin_email
```
