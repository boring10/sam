# enabled

## Alias

- `--enabled`
- `-e`

## config settings checkforupdates

- **Description:** States whether to allow for automatic checking of updates.
- **Default:** true
- **Type:** boolean

## Usage

```bash
# Sets to true
--enabled

# Sets to false
--enabled false
# or
--no-enabled
```

