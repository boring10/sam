# support_phone

## Alias

- `--support_phone`
- `--sup_phone`
- `--supphone`
- `--supportphone`

## manufacturers patch byid

- **Description:** Update the support phone for the manufacturer.
- **Type:** string

## manufacturers post create

- **Description:** Support phone for the manufacturer.
- **Type:** string

## Usage

```bash
--support_phone "+1 555-555-5555"
```

