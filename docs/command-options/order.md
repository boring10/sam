# order

## Alias

- `--order`
- `--ord`

## accessories get checkedout byid

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## accessories get search

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## categories get search

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## companies get search

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## components get assets byid

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## components get search

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## consumables get search

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## departments get search

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## hardware get audit due

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## hardware get audit overdue

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## hardware get licenses byid

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## hardware get search

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## licenses get search

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## locations get search

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## maintenances get search

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## manufacturers get search

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## models get search

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## statuslabels get assetlist byid

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## statuslabels get search

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## users get accessories byid

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## users get assets byid

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## users get licenses byid

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## users get search

- **Description:** Specify the order for the sort field.
- **Choices:** asc, desc
- **Default:** asc
- **Type:** string

## Usage

```bash
--order "asc"
```

