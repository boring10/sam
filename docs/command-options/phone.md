# phone

## Alias

- `--phone`
- `--ph`

## users patch byid

- **Description:** Update the phone for the user.
- **Type:** string

## users post create

- **Description:** The phone for the user.
- **Type:** string

## Usage

```bash
--phone "+1 555.555.5555"
```

