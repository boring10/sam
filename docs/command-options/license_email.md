# license_email

## Alias

- `--license_email`
- `--lic_email`
- `--license_email`

## licenses patch byid

- **Description:** Update the email address associated with the license.
- **Type:** string

## licenses post create

- **Description:** Email address associated with the license.
- **Type:** string

## Usage

```bash
--license_email "registered_email@domain.com"
```

