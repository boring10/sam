# checkout_at

- [Alias](#alias)
- [hardware post checkout byid](#hardware-post-checkout-byid)
- [Usage](#usage)

## Alias

- `--checkout_at`
- `--chkout_at`
- `--checkoutat`

## hardware post checkout byid

- **Description:** Date that the asset was checked out.
- **Type:** string

## Usage

```bash
--checkout_at "2021-08-20"
```
