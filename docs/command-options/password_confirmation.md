# password_confirmation

## Alias

- `--password_confirmation`
- `--pass_conf`

## users patch byid

- **Description:** Update the password for the user.
- **Type:** string

## users post create

- **Description:** The password for the user.
- **Required:** true
- **Type:** string

## Usage

```bash
--password_confirmation "Correct Horse Battery Stapler"
```

