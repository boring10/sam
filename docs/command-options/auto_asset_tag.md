# auto_asset_tag

- [Alias](#alias)
- [config profiles create](#config-profiles-create)
- [config profiles update](#config-profiles-update)
- [Usage](#usage)

## Alias

- `--auto_asset_tag`
- `--a_a_t`
- `--aat`
- `--autoassettag`

## config profiles create

- **Description:** Set to true if you have enabled auto-incrementing asset IDs.
- **Default:** true
- **Type:** boolean

## config profiles update

- **Description:** Set to true if you have enabled auto-incrementing asset IDs.
- **Type:** boolean

## Usage

If `--auto_asset_tag` is omitted when using the command then the default value will be used.

```bash
# Set to true
--auto_asset_tag

# Set to false
--auto_asset_tag false
# or
--no-auto_asset_tag
```
