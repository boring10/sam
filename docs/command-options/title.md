# title

## Alias

- `--title`

## maintenances post create

- **Description:** Title for the maintenance.
- **Required:** true
- **Type:** string

## Usage

```bash
--title "Something is wrong with this thing..."
```

