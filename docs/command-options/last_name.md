# last_name

## Alias

- `--last_name`
- `--lastname`

## users patch byid

- **Description:** Update the last name for the user.
- **Type:** string

## users post create

- **Description:** The last name for the user.
- **Type:** string

## Usage

```bash
--last_name "Doe"
```

