# email

## Alias

- `--email`

## users patch byid

- **Description:** Update the email for the user.
- **Type:** string

## users post create

- **Description:** The email for the user.
- **Type:** string

## Usage

```bash
--email "user@email.com"
```

