# maintained

## Alias

- `--maintained`
- `--maint`

## licenses patch byid

- **Description:** Update the maintained status of the license.
- **Type:** boolean

## licenses post create

- **Description:** Maintained status of the license.
- **Type:** boolean

## Usage

```bash
# Set to true
--maintained

# Set to false
--maintained false
# or
--no-maintained
```

