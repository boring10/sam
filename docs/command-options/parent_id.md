# parent_id

## Alias

- `--prnt_id`
- `--prntid`
- `--parentid`

## locations patch byid

- **Description:** Update the ID of the location that will be the parent of the new location.
- **Type:** number

## locations post create

- **Description:** The ID of the location that will be the parent of the new location.
- **Type:** number

## Usage

```bash
--parent_id 13
```

