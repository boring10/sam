# type

## Alias

- `--type`
- `-t`

## statuslabels patch byid

- **Description:** The type of status designation for the status label.
- **Choices:** deployable, pending, undeployable, archived
- **Type:** string

## statuslabels post create

- **Description:** The type of status designation for the status label.
- **Choices:** deployable, pending, undeployable, archived
- **Default:** deployable
- **Type:** string

## Usage

```bash
--type "pending"
```

