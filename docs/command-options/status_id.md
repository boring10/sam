# status_id

## Alias

- `--status_id`
- `--statid`
- `--stsid`
- `--stat_id`
- `--sts_id`
- `--statusid`

## hardware patch byid

- **Description:** Update the ID of the corresponding status label.
- **Type:** number

## hardware post create

- **Description:** The ID of the corresponding status label.
- **Required:** true
- **Type:** number

## Usage

```bash
--status_id 2
```

