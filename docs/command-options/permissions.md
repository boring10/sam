# permissions

## Alias

- `--permissions`
- `--perm`

## users patch byid

- **Description:** Update the permissions for the user.
- **Type:** string

## users post create

- **Description:** The permissions for the user.
- **Type:** string

## Usage

```bash
--permissions "assets.view:1,assets.create:-1"

# OR
--permissions "{assets.view:1,assets.create:-1}"

# OR
--permissions '{"assets.view":1}'

# OR
--permissions "assets.view:1" --permissions "assets.create:-1"
```

