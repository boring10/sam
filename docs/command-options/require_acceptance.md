# require_acceptance

## Alias

- `--require_acceptance`
- `--requireacceptance`
- `--req_accept`
- `--reqaccept`

## categoriess patch byid

- **Description:** Require the user to confirm acceptance of the item in this category.
- **Type:** boolean

## categoriess post create

- **Description:** Require the user to confirm acceptance of the item in this category.
- **Default:** false
- **Type:** boolean

## Usage

```bash
# Set to true
--require_acceptance

# Set to false
--require_acceptance false
# or
--no-require_acceptance
```

