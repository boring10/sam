# termination_date

## Alias

- `--termination_date`
- `--term_date`
- `--terminationdate`

## licenses patch byid

- **Description:** Update the termination date for the licenses.
- **Type:** string

## licenses post create

- **Description:** Termination date for the licenses.
- **Type:** string

## Usage

```bash
--termination_date "2021-04-13"
```

