# seats

## Alias

- `--seats`

## licenses patch byid

- **Description:** Update the number of license seats owned.
- **Required:** true
- **Type:** number

## licenses post create

- **Description:** Number of license seats owned.
- **Required:** true
- **Type:** number

## Usage

```bash
--seats 100
```

