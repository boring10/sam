# first_name

## Alias

- `--first_name`
- `--firstname`

## users patch byid

- **Description:** Update the first name for the user.
- **Type:** string

## users post create

- **Description:** The first name for the user.
- **Required:** true
- **Type:** string

## Usage

```bash
--first_name "John"
```

