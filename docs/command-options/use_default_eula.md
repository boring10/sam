# use_default_eula

## Alias

- `--use_default_eula`
- `--usedefaulteula`

## categoriess patch byid

- **Description:** Use the primary default EULA instead.
- **Type:** boolean

## categoriess post create

- **Description:** Use the primary default EULA instead.
- **Type:** boolean

## Usage

```bash
# Set to true
--use_default_eula

# Set to false
--use_default_eula false
# or
--no-use_default_eula
```

