# supplier_id

## Alias

- `--supplier_id`
- `--splrid`
- `--splr_id`
- `--supplierid`

## accessories patch byid

- **Description:** Update the ID of the supplier for this accessory.
- **Type:** number

## accessories post create

- **Description:** ID of the supplier for this accessory.
- **Type:** number

## hardware patch byid

- **Description:** Update the ID that corresponds with the supplier.
- **Type:** number

## hardware post create

- **Description:** The ID that corresponds with the supplier.
- **Type:** number

## licenses patch byid

- **Description:** Update the ID of the license supplier.
- **Type:** number

## licenses post create

- **Description:** ID of the license supplier.
- **Type:** number

## maintenances post create

- **Description:** ID of the maintenance supplier.
- **Required:** true
- **Type:** number

## Usage

```bash
--supplier_id 11
```

