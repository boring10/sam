# manufacturer_id

## Alias

- `--manufacturer_id`
- `--mfrid`
- `--mfr_id`
- `--manufacturerid`

## accessories patch byid

- **Description:** Update the ID of the manufacturer for this accessory.
- **Type:** number

## accessories post create

- **Description:** ID of the manufacturer for this accessory.
- **Type:** number

## consumables get search

- **Description:** Manufacturer ID to filter by.
- **Type:** number

## consumables patch byid

- **Description:** Update the ID of the manufacturer for this consumable.
- **
## consumables post create

- **Description:** ID of the manufacturer for this consumable.
- **Type:** number

## hardware get search

- **Description:** Optionally restrict the hardware results to this manufacturer ID.
- **Type:** number

## licenses patch byid

- **Description:** Update the ID of the manufacturer of the license.
- **Required:** true
- **Type:** number

## licenses post create

- **Description:** ID of the manufacturer of the license.
- **Required:** true
- **Type:** number

## models patch byid

- **Description:** Update the ID of the manufacturer of the model.
- **Type:** number

## models post create

- **Description:** ID of the manufacturer of the model.
- **Required:** true
- **Type:** number

## Usage

```bash
--manufacturer_id 3
```

