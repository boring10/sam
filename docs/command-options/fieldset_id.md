# fieldset_id

## Alias

- `--fieldset_id`
- `--f_id`
- `--flds_id`
- `--fieldsetid`

## models post create

- **Description:** ID of the fieldset for the model.
- **Type:** number

## Usage

```bash
--fieldset_id 3
```

