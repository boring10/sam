# active

- [Alias](#alias)
- [config profiles create](#config-profiles-create)
- [config profiles update](#config-profiles-update)
- [Usage](#usage)

## Alias

- `--active`
- `-a`

## config profiles create

- **Description:** States whether to make this profile the active profile.
- **Default:** false
- **Type:** boolean

## config profiles update

- **Description:** Update the active status of the profile. If enabled, all other profiles will have their active status set to false.
- **Type:** boolean

## Usage

```bash
# Set to true
--active

# Set to false
--active false
# or
--no-active
```
