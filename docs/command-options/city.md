# city

## Alias

- `--city`

## locations patch byid

- **Description:** Update the city for the location.
- **Type:** string

## locations post create

- **Description:** The city for the location.
- **Type:** string

## users patch byid

- **Description:** Update the city for the user.
- **Type:** string

## users post create

- **Description:** The city for the user.
- **Type:** string

## Usage

```bash
--city "Somewhere Street"
```

