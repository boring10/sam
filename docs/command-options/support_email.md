# support_email

## Alias

- `--support_email`
- `--sup_em`
- `--supem`
- `--supportemail`

## manufacturers patch byid

- **Description:** Update the support email for the manufacturer.
- **Type:** string

## manufacturers post create

- **Description:** Support email for the manufacturer.
- **Type:** string

## Usage

```bash
--support_email "email@example.com"
```

