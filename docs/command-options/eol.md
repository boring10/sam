# eol

## Alias

- `--eol`

## models patch byid

- **Description:** Update the number of months until this model's assets are considered EOL.
- **Type:** number

## models post create

- **Description:** Number of months until this model's assets are considered EOL.
- **Type:** number

## Usage

```bash
--eol 12
```

