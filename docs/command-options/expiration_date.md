# expiration_date

## Alias

- `--expiration_date`
- `--exp_date`
- `--expirationdate`

## licenses patch byid

- **Description:** Update the date of license expiration.
- **Type:** string

## licenses post create

- **Description:** Date of license expiration.
- **Type:** string

## Usage

```bash
--expiration_date "2021-04-13"
```

