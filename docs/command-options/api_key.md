# api_key

- [Alias](#alias)
- [config profiles create](#config-profiles-create)
- [config profiles update](#config-profiles-update)
- [Usage](#usage)

## Alias

- `--api_key`
- `--a_k`
- `--apikey`

## config profiles create

- **Description:** The API key to authenticate with your instance of Snipe-IT.
- **DemandOption:** true
- **Type:** string

## config profiles update

- **Description:** The API key to authenticate with your instance of Snipe-IT.
- **Type:** string

## Usage

```bash
--api_key "aethaeteathbn90qaeyhg3q90ygh30g349vb3bv3qbhq34fgh34qg34b"
```
