# output

## Alias

- `--output`
- `-o`

## accessories get byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## accessories get checkedout byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## accessories get search

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## categories get byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## categories get search

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## companies get byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## companies get search

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## components get assets byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## components get byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## components get search

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## consumables get byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## consumables get search

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## departments get byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## departments get search

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## hardware get audit due

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## hardware get audit overdue

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## hardware get licenses byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## hardware get byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## hardware get byserial

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## hardware get bytag

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## hardware get search

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## licenses get byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## licenses get search

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## locations get byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## locations get search

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## maintenances get byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## maintenances get search

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## manufacturers get byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## manufacturers get search

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## models get byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## models get search

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## statuslabels get assetlist byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## statuslabels get byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## statuslabels get search

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## users get accessories byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## users get assets byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## users get byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## users get licenses byid

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## users get search

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## config profiles list active

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## config profiles list all

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## config profiles list byname

- **Description:** Specify the output format.
- **Default:** default
- **Choices:** default, json, csv
- **Type:** string

## Usage

```bash
--output "json"
```

