# website

## Alias

- `--website`

## users post create

- **Description:** The website for the user.
- **Type:** string

## Usage

```bash
--website "https://user-website.com"
```

