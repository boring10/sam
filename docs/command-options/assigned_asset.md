# assigned_asset

- [Alias](#alias)
- [hardware post checkout byid](#hardware-post-checkout-byid)
- [Usage](#usage)

## Alias

- `--assigned_asset`
- `--assg_a`
- `--assignedasset`

## hardware post checkout byid

- **Description:** The ID of the asset to assign the asset out to.
- **Type:** number

## Usage

```bash
--assigned_asset 2341
```
