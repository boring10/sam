# email_user

## Alias

- `--email_user`
- `--emailuser`

## users patch byid

- **Description:** Whether the user is emailed or not when account is created.
- **Type:** boolean

## users post create

- **Description:** Whether the user is emailed or not when account is created.
- **Default:** false
- **Type:** boolean

## Usage

```bash
# Set to true
--email_user

# Set to false
--email_user false
# or
--no-email_user
```

