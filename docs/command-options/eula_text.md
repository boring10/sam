# eula_text

## Alias

- `--eula_text`
- `--eulatext`

## categories patch byid

- **Description:** Customize your EULA for the specific type of item. If there is only one EULA for all of your items then set the "use_default_eula" to true.
- **Type:** string

## categories post create

- **Description:** Customize your EULA for the specific type of item. If there is only one EULA for all of your items then set the "use_default_eula" to true.
- **Type:** string

## Usage

```bash
# The EULA allows GITHUB flavored markdown.
--eula_text "Example EULA text."
```

