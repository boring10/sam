# notes

## Alias

- `--notes`
- `--nt`

## hardware patch byid

- **Description:** Update the notes for the hardware.
- **Type:** string

## hardware post checkin byid

- **Description:** Notes about the asset checkin.
- **Type:** string

## hardware post checkout byid

- **Description:** Notes about the asset checkout.
- **Type:** string

## hardware post audit bytag

- **Description:** Notes about the audit.
- **Type:** string

## hardware post create

- **Description:** Notes for the hardware.
- **Type:** string

## licenses patch byid

- **Description:** Update the notes about the licenses.
- **Type:** string

## licenses post create

- **Description:** Notes about the licenses.
- **Type:** string

## models patch byid

- **Description:** Update the notes about the model.
- **Type:** string

## models post create

- **Description:** Notes about the model.
- **Type:** string

## maintenances post create

- **Description:** Notes about the maintenance.
- **Type:** string

## statuslabels patch byid

- **Description:** Update the notes about the status label.
- **Type:** string

## statuslabels post create

- **Description:** Notes about the status label.
- **Type:** string

## users patch byid

- **Description:** Update the notes about the user.
- **Type:** string

## users post create

- **Description:** Notes about the user.
- **Type:** string

## Usage

```bash
--notes "Please be careful with this laptop since it is getting older..."
```

