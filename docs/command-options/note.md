# note

## Alias

- `--note`

## accessories post checkout byid

- **Description:** Note about the accessory.
- **Type:** string

## consumables post checkout byid

- **Description:** Note about the consumable.
- **Type:** string

## Usage

```bash
--note "This is a USB mouse, it must be plugged in to work."
```

