# expand

## Alias

- `--expand`
- `--exp`

## accessories get search

- **Description:** Whether to include detailed information on categories, etc (true) or just the text name (false).
- **Default:** true
- **Type:** boolean

## consumables get search

- **Description:** Whether to include detailed information on categories, etc (true) or just the text name (false).
- **Default:** true
- **Type:** boolean

## licenses get search

- **Description:** Whether to include detailed information on categories, etc (true) or just the text name (false).
- **Default:** true
- **Type:** boolean

## users get search

- **Description:** Whether to include detailed information on categories, etc (true) or just the text name (false).
- **Default:** true
- **Type:** boolean

## Usage

```bash
--expand

--expand false
# OR
--no-expand
```

