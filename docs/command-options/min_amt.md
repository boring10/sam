# min_amt

## Alias

- `--min_amt`
- `--ma`
- `--minamt`

## accessories patch byid

***At this time, this option does not appear to work through the API.***

- **Description:** Update the minimum quantity that should be available before an alert gets triggered.
- **Type:** number

## accessories post create

***At this time, this option does not appear to work through the API.***

- **Description:** The minimum quantity that should be available before an alert gets triggered.
- **Type:** number

## consumables patch byid

- **Description:** Update the minimum quantity that should be available before an alert gets triggered.
- **Type:** number

## consumables post create

- **Description:** The minimum quantity that should be available before an alert gets triggered.
- **Type:** number

## components patch byid

- **Description:** Update the minimum quantity that should be available before an alert gets triggered.
- **Type:** number

## components post create

- **Description:** The minimum quantity that should be available before an alert gets triggered.
- **Type:** number

## Usage

```bash
--min_amt 2
```

