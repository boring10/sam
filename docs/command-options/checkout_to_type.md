# checkout_to_type

## Alias

- `--checkout_to_type`
- `--chkout_to_type`
- `--checkouttotype`

## hardware post checkout byid

- **Description:** Checkout the asset to an asset, location, or user.
- **Choices:** asset, location, user
- **Default:** user
- **Type:** string

## Usage

If `--checkout_to_type` is omitted when using it with the command then the default value will be used.

```bash
# Checkout to an asset
--checkout_to_type "asset"

# Checkout to a location
--checkout_to_type "location"

# Checkout to a user
--checkout_to_type "user"
```

