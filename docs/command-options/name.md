# name

## Alias

- `--name`
- `-n`

## accessories patch byid

- **Description:** Update the name of the accessory.
- **Type:** string

## accessories post create

- **Description:** Name of the accessory.
- **Required:** true
- **Type:** string

## categories patch byid

- **Description:** Update the name of the category.
- **Type:** string

## categories post create

- **Description:** Name of the category.
- **Required:** true
- **Type:** string

## companies patch byid

- **Description:** Update the name of the company.
- **Required:** true
- **Type:** string

## companies post create

- **Description:** Name of the company.
- **Required:** true
- **Type:** string

## components patch byid

- **Description:** Update the name of the component.
- **Type:** string

## components post create

- **Description:** Name of the component.
- **Required:** true
- **Type:** string

## consumables patch byid

- **Description:** Update the name of the consumable.
- **Type:** string

## consumables post create

- **Description:** Name of the consumable.
- **Required:** true
- **Type:** string

## departments patch byid

- **Description:** Update the name of the department.
- **Required:** true
- **Type:** string

## departments post create

- **Description:** Name of the department.
- **Required:** true
- **Type:** string

## hardware patch byid

- **Description:** Update the name of the asset.
- **Type:** string

## hardware post checkin byid

- **Description:** Name of the asset.
- **Type:** string

## hardware post checkout byid

- **Description:** Name of the asset.
- **Type:** string

## hardware post create

- **Description:** Name of the asset.
- **Type:** string

## licenses patch byid

- **Description:** Update the name of the license.
- **Type:** string

## licenses post create

- **Description:** Name of the license.
- **Required:** true
- **Type:** string

## locations patch byid

- **Description:** Update the name of the location.
- **Type:** string

## locations post create

- **Description:** Name of the location.
- **Required:** true
- **Type:** string

## manufacturer patch byid

- **Description:** Update the name of the manufacturer.
- **Type:** string

## manufacturer post create

- **Description:** Name of the manufacturer.
- **Required:** true
- **Type:** string

## models patch byid

- **Description:** Update the name of the model.
- **Type:** string

## models post create

- **Description:** Name of the model.
- **Required:** true
- **Type:** string

## statuslabels patch byid

- **Description:** Update the name of the status label.
- **Type:** string

## statuslabels post create

- **Description:** Name of the status label.
- **Required:** true
- **Type:** string

## config profiles create

> All profile names have spaces removed and are converted to lowercase automatically.

- **Description:** Name of the profile.
- **Required:** true
- **Type:** string

## config profiles update

> All profile names have spaces removed and are converted to lowercase automatically.

- **Description:** Name of the profile.
- **Required:** true
- **Type:** string

## Usage

```bash
# Hardware
--name "Billy's Laptop" 

# Profiles
--name "My Profile Name" # -> myprofilename
```

