# cost

## Alias

- `--cost`

## maintenances post create

- **Description:** Cost of the maintenance that was performed.
- **Type:** string

## Usage

```bash
--cost "123.29"
```

