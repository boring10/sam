# requestable

## Alias

- `--requestable`
- `--req`

## consumables patch byid

- **Description:** Update whether or not the consumable is requestable by users with the permission to request consumables.
- **Type:** boolean

## consumables post create

- **Description:** Whether or not the consumable is requestable by users with the permission to request consumables.
- **Type:** boolean

## hardware patch byid

- **Description:** Update whether or not the hardware is requestable by users with the permission to request hardware.
- **Type:** boolean

## hardware post create

- **Description:** Whether or not the hardware is requestable by users with the permission to request hardware.
- **Type:** boolean

<!-- ## models patch byid -->

<!-- - **Description:** Update whether or not the model is requestable by users with the permission to request hardware. -->
<!-- - **Type:** boolean -->

<!-- ## models post create -->

<!-- - **Description:** Whether or not the model is requestable by users with the permission to request hardware. -->
<!-- - **Type:** boolean -->

## Usage

```bash
# Set to true
--requestable

# Set to false
--requestable false
# or
--no-requestable
```

