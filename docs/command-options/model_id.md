# model_id

## Alias

- `--model_id`
- `--modid`
- `--mod_id`
- `--modelid`

## hardware get search

- **Description:** Optionally restrict the hardware results to this model ID.
- **Type:** number

## hardware patch byid

- **Description:** Update the ID of the associated hardware model ID.
- **Type:** number

## hardware post create

- **Description:** The ID of the associated hardware model ID.
- **Required:** true
- **Type:** number

## Usage

```bash
--model_id 13 
```

