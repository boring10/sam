# two_factor_optin

## Alias

- `--two_factor_optin`
- `--tfa_optin`
- `--twofactoroptin`

## users patch byid

- **Description:** Whether the user is opted in for two factor authentication.
- **Type:** boolean

## users post create

- **Description:** Whether the user is opted in for two factor authentication.
- **Default:** false
- **Type:** boolean

## Usage

```bash
# Set to true
--two_factor_optin

# Set to false
--two_factor_optin false
# or
--no-two_factor_optin
```

