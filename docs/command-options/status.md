# status

## Alias

- `--status`
- `--stat`
- `--sts`

## hardware get search

- **Description:** Optionally restrict the results to a status type.
- **Choices:** archived, deleted, deployed, requestable, rtd
- **Type:** string

## Usage

```bash
--status "deployed"
```

