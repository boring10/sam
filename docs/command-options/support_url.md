# support_url

## Alias

- `--support_url`
- `--sup_url`
- `--supurl`
- `--supporturl`

## manufacturers patch byid

- **Description:** Update the support url for the manufacturer.
- **Type:** string

## manufacturers post create

- **Description:** Support url for the manufacturer.
- **Type:** string

## Usage

```bash
--support_url "https://supporturl.com"
```

