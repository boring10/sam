# warranty_months

## Alias

- `--warranty_months`
- `--wntymos`
- `--wnty_mos`
- `--warrantymonths`

## hardware patch byid

- **Description:** Update the number of months for the hardware warranty.
- **Type:** number

## hardware post create

- **Description:** Number of months for the hardware warranty.
- **Type:** number

## Usage

```bash
--warranty_months 12
```

