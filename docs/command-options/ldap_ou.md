# ldap_ou

## Alias

- `--ldap_ou`
- `--ldapou`

## locations patch byid

- **Description:** Update the ldap organizational unit.
- **Type:** string

## locations post create

- **Description:** The ldap organizational unit.
- **Type:** string

## Usage

```bash
--ldap_ou "ou=users,dc=domain,dc=com"
```

