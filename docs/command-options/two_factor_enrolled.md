# two_factor_enrolled

## Alias

- `--two_factor_enrolled`
- `--tfa_enrolled`
- `--twofactorenrolled`

## users patch byid

- **Description:** Whether the user is two factor enrolled or not.
- **Type:** boolean

## users post create

- **Description:** Whether the user is two factor enrolled or not.
- **Default:** false
- **Type:** boolean

## Usage

```bash
# Set to true
--two_factor_enrolled

# Set to false
--two_factor_enrolled false
# or
--no-two_factor_enrolled
```

