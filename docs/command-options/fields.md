# fields

## Alias

- `--fields`
- `-f`

## accessories get byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## accessories get checkdout byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## accessories get search

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## categories get byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## categories get search

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## companies get byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## companies get search

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## components get assets byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## components get byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## components get search

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## consumables get byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## consumables get search

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## departments get byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## departments get search

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## hardware get byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## hardware get bytag

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## hardware get byserial

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## hardware get audit due

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## hardware get licenses byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## hardware get search

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## licenses get byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## licenses get search

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## locations get byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## locations get search

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## maintenances get byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## maintenances get search

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## manufacturers get byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## manufacturers get search

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## models get byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## models get search

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## statuslabels get assetlist byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## statuslabels get byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## statuslabels get search

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## users get accessories byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## users get assets byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## users get byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## users get licenses byid

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## users get search

- **Description:** Specify the fields that will be returned. By default, all fields will be returned.
- **Type:** string

## Usage

```bash
--fields "id,name,asset_tag,serial,_snipeit_mac_address_1"
```

