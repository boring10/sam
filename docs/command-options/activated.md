# activated

- [Alias](#alias)
- [users patch byid](#users-patch-byid)
- [users post create](#users-post-create)
- [Usage](#usage)

## Alias

- `--activated`

## users patch byid

- **Description:** Whether the user is able to login or not.
- **Type:** boolean

## users post create

- **Description:** Whether the user is able to login or not.
- **Default:** false
- **Type:** boolean

## Usage

```bash
# Set to true
--activated

# Set to false
--activated false
# or
--no-activated
```
