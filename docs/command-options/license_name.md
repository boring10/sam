# license_name

## Alias

- `--license_name`
- `--lic_name`
- `--licensename`

## license patch byid

- **Description:** Update the name of license contact person.
- **Type:** string

## license post create

- **Description:** Name of license contact person.
- **Type:** string

## Usage

```bash
--license_name "Chocolatey"
```

