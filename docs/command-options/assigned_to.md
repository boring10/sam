# assigned_to

- [Alias](#alias)
- [accessories post checkout byid](#accessories-post-checkout-byid)
- [consumables post checkout byid](#consumables-post-checkout-byid)
- [Usage](#usage)

## Alias

- `--assigned_to`
- `--assg_to`
- `--assignedto`

## accessories post checkout byid

- **Description:** The ID of the user to assign the accessory out to.
- **Type:** number

## consumables post checkout byid

- **Description:** The ID of the user to assign the consumable out to.
- **Type:** number

## Usage

```bash
--assigned_to 12
```
