# purchase_cost

## Alias

- `--purchase_cost`
- `--purcst`
- `--pur_cst`
- `--purchasecost`

## accessories patch byid

- **Description:** Update the cost of item being purchased.
- **Type:** string

## accessories post create

- **Description:** Cost of item being purchased.
- **Type:** string

## components patch byid

- **Description:** Update the cost of item being purchased.
- **Type:** string

## components post create

- **Description:** Cost of item being purchased.
- **Type:** string

## consumables patch byid

- **Description:** Update the cost of item being purchased.
- **Type:** string

## consumables post create

- **Description:** Cost of item being purchased.
- **Type:** string

## hardware patch byid

- **Description:** Update the purchase cost of the hardware, without the currency symbol.
- **Type:** string

## hardware post create

- **Description:** The purchase cost of the hardware, without the currency symbol.
- **Type:** string

## licenses patch byid

- **Description:** Update the cost of the license.
- **Type:** string

## licenses post create

- **Description:** Cost of the license.
- **Type:** string

## Usage

```bash
--purchase_cost "123.29"
```

