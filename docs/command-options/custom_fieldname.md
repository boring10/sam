# custom_fieldname

## Alias

Custom fieldnames all being with `--_snipeit_`. Custom fieldnames are the DB name of the field that you created. One example is listed below.

- `--_snipeit_mac_address_1`

## hardware get search

- **Description:** To include custom fields, you would include the database field name in the payload. Note that this is NOT the human-readable name, but you can get the field name by checking the 'Custom Fields' page and looking for the 'DB Field' column.
- **Type:** string

## hardware patch byid

- **Description:** To include custom fields, you would include the database field name in the payload. Note that this is NOT the human-readable name, but you can get the field name by checking the 'Custom Fields' page and looking for the 'DB Field' column.
- **Type:** string

## hardware post create

- **Description:** To include custom fields, you would include the database field name in the payload. Note that this is NOT the human-readable name, but you can get the field name by checking the 'Custom Fields' page and looking for the 'DB Field' column.
- **Type:** string

## Usage

```bash
--_snipeit_mac_address_1 "12:34:56:aa:bb:cc"
```
