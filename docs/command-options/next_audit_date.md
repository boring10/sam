# next_audit_date

## Alias

- `--next_audit_date`
- `--nextauditdate`

## hardware post audit bytag

- **Description:** Next time the asset will be due for an audit.
- **Type:** string

## Usage

```bash
--next_audit_date "2021-08-01"
```

