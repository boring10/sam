# model_number

## Alias

- `--model_number`
- `--modnum`
- `--mod_num`
- `--modelnumber`

## accessories patch byid

- **Description:** Update the model number of accessory.
- **Type:** number

## accessories post create

- **Description:** Model number of accessory.
- **Type:** number

## consumables patch byid

- **Description:** Update the model number of consumable.
- **Type:** number

## consumables post create

- **Description:** Model number of consumable.
- **Type:** number

## models patch byid

- **Description:** Update the model number for the model.
- **Type:** number

## models post create

- **Description:** Model number for the model.
- **Type:** number

## Usage

```bash
--model_number "sdf12321"
```

