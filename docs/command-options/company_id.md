# company_id

## Alias

- `--company_id`
- `--coid`
- `--co_id`
- `--companyid`

## accessories patch byid

- **Description:** Update the ID of the company the accessory is assigned to.
- **Type:** number

## accessories post create

- **Description:** ID of the company the accessory is assigned to.
- **Type:** number

## component patch byid

- **Description:** Update the ID of the company the component is assigned to.
- **Type:** number

## components post create

- **Description:** ID of the company the component is assigned to.
- **Type:** number

## consumables get search

- **Description:** Company ID to filter by.
- **Type:** number

## consumables patch byid

- **Description:** Update ID of the company the consumable is assigned to.
- **Type:** number

## consumables post create

- **Description:** ID of the company the consumable is assigned to.
- **Type:** number

## department patch byid

- **Description:** Update the ID of the company the department is assigned to.
- **Type:** number

## department post create

- **Description:** ID of the company the department is assigned to.
- **Type:** number

## hardware get search

- **Description:** Optionally restrict the hardware results to a company ID.
- **Type:** number

## hardware patch byid

- **Description:** Update the ID of the company to associate the hardware with.
- **Type:** number

## hardware post byid

- **Description:** The ID of the company to associate the hardware with.
- **Type:** number

## hardware post create

- **Description:** The ID of the company to associate the hardware with.
- **Type:** number

## licenses patch byid

- **Description:** Update the ID of the company to associate the licenses with.
- **Type:** number

## licenses post create

- **Description:** The ID of the company to associate the licenses with.
- **Type:** number

## users patch byid

- **Description:** Update the ID of the company to associate the user with.
- **Type:** number

## users post create

- **Description:** The ID of the company to associate the user with.
- **Type:** number

## Usage

```bash
--company_id 1
```
