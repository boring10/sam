# employee_num

## Alias

- `--employee_num`

## users patch byid

- **Description:** Update the employee number for the user.
- **Type:** string

## users post create

- **Description:** The employee number for the user.
- **Type:** string

## Usage

```bash
--employee_num "12357683"
```

