# department_id

## Alias

- `--department_id`
- `--dept_id`
- `--departmentid`

## users patch byid

- **Description:** Update the department ID for the user.
- **Type:** number

## users post create

- **Description:** The department ID for the user.
- **Type:** number

## Usage

```bash
--department_id 3
```

