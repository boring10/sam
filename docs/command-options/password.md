# password

## Alias

- `--password`
- `--pass`
- `-p`

## users patch byid

- **Description:** Update the password for the user.
- **Type:** string

## users post create

- **Description:** The password for the user.
- **Required:** true
- **Type:** string

## Usage

```bash
--password "Correct Horse Battery Stapler"
```

