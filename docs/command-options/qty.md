# qty

## Alias

- `--qty`
- `--quantity`

## accessories patch byid

- **Description:** Update the quantity of the accessory you have.
- **Type:** number

## accessories post create

- **Description:** Quantity of the accessory you have.
- **Required:** true
- **Type:** number

## components patch byid

- **Description:** Update the quantity of the component you have.
- **Type:** number

## components post create

- **Description:** Quantity of the component you have.
- **Required:** true
- **Type:** number

## consumables patch byid

- **Description:** Update the quantity of the consumables you have.
- **Type:** number

## consumables post create

- **Description:** Quantity of the consumables you have.
- **Required:** true
- **Type:** number

## Usage

```bash
--qty 50
```

