# color

## Alias

- `--color`

## statuslabels patch byid

- **Description:** Update the hex code showing what color the status label should be on the pie chart in the dashboard.
- **Type:** string

## statuslabels post create

- **Description:** Hex code showing what color the status label should be on the pie chart in the dashboard.
- **Type:** string

## Usage

```bash
--color "#e2e2e2"
```

