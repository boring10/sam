# Consumables

- [Delete](#delete)
    - [consumables delete byid](#consumables-delete-byid)
- [Get](#get)
    - [consumables get byid](#consumables-get-byid)
    - [consumables get search](#consumables-get-search)
- [Patch](#patch)
    - [consumables patch byid](#consumables-patch-byid)
- [Post](#post)
    - [consumables post checkin bypivotid](#consumables-post-checkin-bypivotid)
    - [consumables post checkout byid](#consumables-post-checkout-byid)
    - [consumables post create](#consumables-post-create)

## Delete

### consumables delete byid

```bash
sam consumables delete byid 5
> Successfully deleted an accessory with the ID "5".
```

## Get

### consumables get byid

```bash
sam consumables get byid 5
> id: 5
> name: Logitech Mouse
> image: null
> company: null
> manufacturer: null
> supplier: null
> model_number: null
> category.id: 9
> category.name: Mouse
> location: null
> notes: null
> qty: 75
> purchase_date: null
> purchase_cost: null
> order_number: null
> min_qty: null
> remaining_qty: 75
> created_at.datetime: 2021-04-08 06:49:42
> created_at.formatted: Thu Apr 08, 2021 6:49AM
> updated_at.datetime: 2021-04-08 06:49:42
> updated_at.formatted: Thu Apr 08, 2021 6:49AM
> available_actions.checkout: true
> available_actions.checkin: false
> available_actions.update: true
> available_actions.delete: true
> user_can_checkout: true
```

### consumables get checkedout byid

```bash
sam consumables get checkedout byid 4
> assigned_pivot_id: 6
> id: 2
> username: snipe
> name: Snipe E. Head
> first_name: Snipe E.
> last_name: Head
> employee_number: 24988
> checkout_notes: something
> last_checkout.datetime: 2021-04-08 08:41:22
> last_checkout.formatted: Thu Apr 08, 2021 8:41AM
> type: user
> available_actions.checkin: true
> 
> assigned_pivot_id: 7
> id: 5
> username: meredith21
> name: Pearl Boyer
> first_name: Pearl
> last_name: Boyer
> employee_number: 3970
> checkout_notes: something
> last_checkout.datetime: 2021-04-08 08:42:55
> last_checkout.formatted: Thu Apr 08, 2021 8:42AM
> type: user
> available_actions.checkin: true
```

### consumables get search

```bash
sam consumables get search "mouse" \
    --fields "id,name"
> id: 3
> name: Magic Mouse
> 
> id: 4
> name: Sculpt Comfort Mouse
```

## Patch

### consumables patch byid

```bash
sam consumables patch byid 5 \
    --qty 75
> Successfully updated an accessory with the ID "5" named "Logitech Mouse" with a quantity of "75".
```

## Post

### consumables post checkin bypivotid

```bash
sam accessory post checkin bypivotid 13
> Successfully checked in the accessory.
```

### consumables post checkout byid

```bash
sam consumables post checkout byid 7 \
    --assigned_to 5
> Successfully checked out an accessory to a user with the ID of "5".
```

### consumables post create

```bash
sam consumables post create \
    --name "Logitech Mouse" \
    --qty 100 \
    --category_id 9
> Successfully created an accessory with the ID "5" named "Logitech Mouse".
```
