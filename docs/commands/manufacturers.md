# Manufacturers

- [Delete](#delete)
    - [manufacturers delete byid](#manufacturers-delete-byid)
- [Get](#get)
    - [manufacturers get byid](#manufacturers-get-byid)
    - [manufacturers get search](#manufacturers-get-search)
- [Patch](#patch)
    - [manufacturers patch byid](#manufacturers-patch-byid)
- [Post](#post)
    - [manufacturers post create](#manufacturers-post-create)

## Delete

> 2021-03-25: Snipe-IT does not pass any information about the device back when deleting a manufacturer. The only information that is passed back is if it was successfull or if the manufacturer does not exist.

```bash
sam manufacturers delete byid 9 
> Successfully deleted the manufacturer with the ID of "9".
```

## Get

### manufacturers get byid

```bash
sam manufacturers get byid 9 \
    --fields "id,name,url,support_phone"
> id: 9 
> name: Acer-Update
> url: https://acer-update.com
> support_phone: 123-123-1234 x1234
```

### manufacturers get search

```bash
sam manufacturers get search "le" \
    --fields "id,name,url,supplier_phone,assets_count" \
    --output json
> {
>   '1': {
>     id: '1',
>     name: 'Apple',
>     url: 'https://apple.com',
>     assets_count: '1259'
>   },
>   '2': {
>     id: '6',
>     name: 'Lenovo',
>     url: 'https://lenovo.com',
>     assets_count: '37'
>   }
> }
```

## Patch

### manufacturers patch byid

```bash
sam manufacturers patch byid 9 \
    --name "Acer-Update" \
    --support_email "acer-update@mail.com" \
    --support_phone "123-123-1234 x1234" \
    --support_url "https://update.acer.com" \
    --url "https://acer-update.com"
> Successfully updated the manufacturer with the ID "9" named "Acer-Update".
```

## Post

### manufacturers post create

```bash
sam manufacturers post create \
    --name "Acer"
> Successfully created a manufacturer with the ID "9" named "Acer".
```
