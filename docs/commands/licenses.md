# Licenses

- [Delete](#delete)
    - [licenses delete byid](#licenses-delete-byid)
- [Get](#get)
    - [licenses get byid](#licenses-get-byid)
    - [licenses get search](#licenses-get-search)
- [Patch](#patch)
    - [licenses patch byid](#licenses-patch-byid)
- [Post](#post)
    - [licenses post create](#licenses-post-create)

## Delete

### licenses delete byid

```bash
sam licenses delete byid 1
> Successfully deleted the license with the ID "1".
```

## Get

### licenses get byid

```bash
sam licenses get byid 2 \
    --fields "id,name,manufacturer.name,order_number"
> id: 2
> name: Acrobat
> manufacturer.name: Adobe
> order_number: 11815643
```

### licenses get search

```bash
sam licenses get search --fields "id,name,manufacturer.name,purchase_date.date"
> id: 2
> name: Acrobat
> manufacturer.name: Adobe
> 
> id: 3
> name: InDesign
> manufacturer.name: Adobe
> 
> id: 4
> name: Office
> manufacturer.name: Microsoft
```

## Patch

### licenses patch byid

```bash
sam licenses patch byid 1 \
    --seats 50
> Successfully updated the license with the ID "1" named "Photoshop" with "50" seats.
```

## Post

### licenses post create

```bash
sam licenses post create \
    --name "Chocolatey" \
    --seats 100 \
    --category_id 1 \
    --manufacturer_id 2
> Successfully create a licenses with the ID "5" named "Chocolatey" with "100" seats.
```
