# locations

- [Delete](#delete)
    - [locations delete byid](#locations-delete-byid)
- [Get](#get)
    - [locations get byid](#locations-get-byid)
    - [locations get search](#locations-get-search)
- [Patch](#patch)
    - [locations patch byid](#locations-patch-byid)
- [Post](#post)
    - [locations post create](#locations-post-create)

## Delete

### locations delete byid

```bash
sam locations delete byid 3
> Successfully deleted the location with the ID "3".
```

## Get

### locations get byid

```bash
sam locations get byid 10
> id: 10
> name: South Maymie
> image: https://develop.snipeitapp.com/uploads/locations/1.jpg
> address: 87457 Brekke Squares Apt. 055
> address2: Suite 194
> city: South Eden
> state: DE
> country: TL
> zip: 49678-8178
> assigned_assets_count: 9
> assets_count: 98
> users_count: 0
> currency: VUV
> ldap_ou: null
> created_at.datetime: 2021-04-04 21:00:02
> created_at.formatted: Sun Apr 04, 2021 9:00PM
> updated_at.datetime: 2021-04-04 21:00:02
> updated_at.formatted: Sun Apr 04, 2021 9:00PM
> parent: null
> manager: null
> children: 
> available_actions.update: true
> available_actions.delete: false
```

### locations get search

```bash
sam locations get search "North" \
    --fields "id,name,state"
> id: 2
> name: Theodoretown
> state: NC
> 
> id: 5
> name: North Estella
> state: NE
```

## Patch

### locations patch byid

```bash
sam locations patch byid 3 \
    --name "New name"
> Successfully updated the location with the ID "3" named "New name".
```

## Post

### locations post byid

```bash
sam locations post create \
    --name "Awesome Location"
```

