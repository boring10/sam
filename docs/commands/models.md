# Models

- [Delete](#delete)
    - [models delete byid](#models-delete-byid)
- [Get](#get)
    - [models get byid](#models-get-byid)
    - [models get search](#models-get-search)
- [Patch](#patch)
    - [models patch byid](#models-patch-byid)
- [Post](#post)
    - [models post create](#models-post-create)

## Delete

```bash
sam models delete byid 9 
> Successfully deleted the model with the ID of "9".
```

## Get

### models get byid

### models get search

## Patch

### models patch byid

```bash
sam models patch byid 18 \
    --name "MacBook Air 2"
> Successfully updated the model with the ID "18" named "MacBook Air 2".
```

## Post

### models post create

```bash
sam models post create \
    --name "MacBook Air" \
    --manufacturer_id 1 \
    --category_id 1
> Successfully created a model with the ID "20" named "MacBook Air".
```
