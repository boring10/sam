# Categories

- [Delete](#delete)
    - [categories delete byid](#categories-delete-byid)
- [Get](#get)
    - [categories get byid](#categories-get-byid)
    - [categories get search](#categories-get-search)
- [Patch](#patch)
    - [categories patch byid](#categories-patch-byid)
- [Post](#post)
    - [categories post create](#categories-post-create)

## Delete

```bash
sam categories delete byid 21
> Successfully deleted the category with the ID of "21".
```

## Get

### categories get byid

```bash
sam categories get byid 4 \
    --fields "id,name,category_type"
> id: 4
> name: Mobile Phones
> category_type: Asset
```

### categories get search

```bash
sam categories get search "phones" \
    --limit 2 \
    --order "desc" \
    --fields "id,name,item_count"
> id: 7
> name: Conference Phones
> item_count: 0
> 
> id: 6
> name: VOIP Phones
> item_count: 7
```

## Patch

### categories patch byid

```bash
sam categories patch byid 7 \
    --category_type 'ASSET' \
    --checkin_email \
    --use_default_eula \
    --name "Conference - Phones"
> Successfully updated the category with the ID "7" named "Conference - Phones".
```

## Post

### categories post create

```bash
sam categories post create \
    --name "Chrome Device" \
    --category_type "asset" \
    --checkin_email \
    --use_default_eula
> Successfully created a category with the ID "21" named "Chrome Device".
```

