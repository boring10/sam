# Users

- [Delete](#delete)
    - [users delete byid](#users-delete-byid)
- [Get](#get)
    - [users get accessories byid](#users-get-accessories-byid)
    - [users get assets byid](#users-get-assets-byid)
    - [users get byid](#users-get-byid)
    - [users get licenses byid](#users-get-licenses-byid)
    - [users get search](#users-get-search)
- [Patch](#patch)
    - [users patch byid](#users-patch-byid)
- [Post](#post)
    - [users post create](#users-post-create)

## Delete

### users delete byid

```bash
sam users delete byid 71
> Successfully deleted the user with the ID of "71".
```

## Get

### users get accessories byid

```bash
sam users get accessories byid 58 \
    --fields "id,name,manufacturer.name,category.name"  
> id: 1
> name: USB Keyboard
> manufacturer.name: Apple
> category.name: Keyboards
> 
> id: 2
> name: Bluetooth Keyboard
> manufacturer.name: Apple
> category.name: Keyboards
```

### users get assets byid

```bash
sam users get assets byid 58 \
    --fields "id,asset_tag,serial,manufacturer.name,model.name,purchase_cost"
    --output "csv"
> id,asset_tag,manufacturer.name,model.name,purchase_cost,serial
> 1,411452679,Apple,Macbook Pro 13",1661.86,4e0f1a4f-40ee-34c5-86fa-c9d821d64397
> 2,1224885193,Apple,Macbook Pro 13",956.63,447d99af-6d76-3a80-b847-018e50ec523b
```

### users get byid

```bash
sam users get byid 58 \
    --fields "id,name,first_name,last_name,username,employee_num,jobtitle,phone"
> id: 58
> name: Sienna Lakin
> first_name: Sienna
> last_name: Lakin
> username: zackary.hermann
> employee_num: 27273
> jobtitle: Personal Service Worker
> phone: 242.991.1801 x03321
```

### users get licenses byid

```bash
sam users get licenses byid 58 \
    --fields "id,name,manufacturer.id,manufacturer.name,product_key,order_number,purchase_date.date,purchase_cost"
> id: 2
> name: Acrobat
> manufacturer.id: 9
> manufacturer.name: Adobe
> product_key: 571dfa7c-3545-3daf-9177-94b66c68caa3
> order_number: 20396647
> purchase_date.date: 2020-08-13
> purchase_cost: 29.99
```

### users get search

```bash
sam users get search "Dept of Silly Walks" \
    --fields "id,name,username,email,phone,jobtitle,department.name" \
    --output "csv"
> id,department.name,email,jobtitle,name,phone,username
> 1,Dept of Silly Walks,jadyn.green@example.org,Animal Trainer,Admin User,352-381-7853,admin
> 5,Dept of Silly Walks,runolfsson.marge@example.org,Kindergarten Teacher,Daniella Armstrong,(649) 535-2275 x628,hkuphal
> 9,Dept of Silly Walks,vernice.muller@example.net,Carpenter,Macie Berge,1-854-741-6324 x37985,cassandra.fritsch
> 18,Dept of Silly Walks,obarrows@example.net,Roofer,Anne Metz,(253) 597-3026 x91344,judge.lakin
> 21,Dept of Silly Walks,szboncak@example.net,Psychiatric Aide,Camylle Halvorson,(246) 916-2290 x693,joy.boehm
> 33,Dept of Silly Walks,wisozk.carolyn@example.net,Athletic Trainer,Bryon Ziemann,+12624943987,mills.easter
> 37,Dept of Silly Walks,watson87@example.net,Nutritionist,Jaydon McDermott,(783) 609-5722 x05876,mikayla.beer
> 47,Dept of Silly Walks,renner.janet@example.net,Fishery Worker,Vito Trantow,887.515.0517 x8991,rokon
> 48,Dept of Silly Walks,qromaguera@example.net,Farm Equipment Mechanic,Charley Muller,+15093468732,jacobson.branson
> 57,Dept of Silly Walks,ckoepp@example.org,Central Office Operator,Dixie Labadie,1-728-401-0030 x217,lbeahan
```

## Patch

### users patch byid

```bash
sam users patch byid 58 \
    --phone "242.991.1801 x03322"
> Successfully updated the user with the ID "58" with the name "Sienna Lakin" and username "zackary.hermann".
```

## Post

### users post create

```bash
sam users post create \
    --username "john.doe" \
    --first_name "John" \
    --last_name "Doe" \
    --password "WordPass123#" \
    --password_confirmation "WordPass123#" \
    --jobtitle "Manager"
    --employee_num "1234567890" \
    --phone "+1 555-555-5555" \
    --email "example@mail.com" \
    --address "Somewhere Street" \
    --city "Somehow" \
    --state "N/A" \
    --zip "123456" \
    --country "us" \
    --notes "Some notes" \
    --company_id 1 \
    --department_id 1 \
    --location_id 1 \
    --manager_id 3 \
    --permissions "assets.view:1" \
    --permissions "assets.create:-1" \
    --permissions "assets.edit:-1" \
    --permissions "assets.delete:-1" \
    --permissions "assets.checkout:0" \
    --permissions "assets.checkin:0" \
    --email_user \
    --activated \
> Successfully created a new user with the ID "71" with the name "John Doe" and username "john.doe".

```
