# Components

- [Delete](#delete)
    - [components delete byid](#components-delete-byid)
- [Get](#get)
    - [components get byid](#components-get-byid)
    - [components get checkedout byid](#components-get-checkedout-byid)
    - [components get search](#components-get-search)
- [Patch](#patch)
    - [components patch byid](#components-patch-byid)
- [Post](#post)
    - [components post checkin bypivotid](#components-post-checkin-bypivotid)
    - [components post checkout byid](#components-post-checkout-byid)
    - [components post create](#components-post-create)

## Delete

### components delete byid

```bash
sam components delete byid 6
> Successfully deleted the component with the ID of "6".
```

## Get

### components get byid

### components get search

## Patch

### components patch byid

```bash
sam components patch byid 7 \
    --order_number "abc13241235"
> Successfully updated the component with the ID "7" named "HP 11 G3/G4 Palmrest with Keyboard".
```

## Post

### components post create

```bash
sam components post create \
    --name "HP 11 G3/G4 Palmrest with Keyboard" \
    --qty 5 \
    --category_id 9
> Successfully created an component with the ID "7" named "HP 11 G3/G4 Palmrest with Keyboard" with a quantity of "5".
```
