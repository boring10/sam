# Companies

- [Delete](#delete)
    - [companies delete byid](#companies-delete-byid)
- [Get](#get)
    - [companies get byid](#companies-get-byid)
    - [companies get search](#companies-get-search)
- [Patch](#patch)
    - [companies patch byid](#companies-patch-byid)
- [Post](#post)
    - [companies post create](#companies-post-create)

## Delete

> 2021-03-25: Snipe-IT does not pass any information about the device back when deleting a company. The only information that is passed back is if it was successfull or if the company does not exist.

```bash
sam companies delete byid 11
> Successfully deleted the company with the ID of "11".
```

## Get

### companies get byid

```bash
sam companies get byid 11
> id: 11
> name: Test Company Name
> image: null
> created_at.datetime: 2021-03-25 09:56:15
> created_at.formatted: Thu Mar 25, 2021 9:56AM
> updated_at.datetime: 2021-03-25 09:56:15
> updated_at.formatted: Thu Mar 25, 2021 9:56AM
> assets_count: 0
> licenses_count: 0
> accessories_count: 0
> consumables_count: 0
> components_count: 0
> users_count: 0
> available_actions.update: true
> available_actions.delete: true
```

### companies get search

```bash
sam companies get search \
    --fields "id,name" \
    --limit 3 \
    --offset 2 \
    --sort id \
    --order asc \
    --output json
> {
>   '1': { id: '3', name: 'Kautzer Inc' },
>   '2': { id: '4', name: 'Nikolaus, Osinski and Pfannerstill' },
>   '3': { id: '5', name: 'Walker-Schoen' }
> }
```

## Patch

### companies patch byid

```bash
sam companies patch byid 11 \
    --name "Updated Company Name"
> Successfully updated the company with the ID "11" named "Updated Company Name".
```

## Post

### companies post create

```bash
sam companies post create \
    --name "Test Company Name"
> Successfully created a company with the ID "11" named "Test Company Name".
```
