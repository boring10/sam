# Directories and Files

When you first run SAM a directory will be created the the home/user directory (based on your operating system).

- **Linux/macOS:** `$HOME/.config/sam`
- **Windows:** `%USERPROFILE%\AppData\Local\sam` or `%HOMEPATH%\AppData\Local\sam` (both are to the same location)

Files that are created are:

- config.json
- profiles.json
- logs/[yyyymm].log *New log file is created every month.*

## config.json

This file is a more general configuration for SAM and may be utilized more in the future. Currently this file houses the checking for updates flag and last time it checked. See [Settings](../../commands/config/#settings) for available commands.

## profiles.json

This file contains the profile(s) for the current user. Many profiles can be stored in this file with the ability to switch between them. See [Profiles](../../commands/config/#profiles) for commands.

## [yyyymm].log

Currently a work in progress but I plan to make the logs better for reviewing purposes.
