# Build Locally

If you are developing locally for SAM, or need to build SAM so that it is compatible with your system architecture, then you can use the build scripts in the scripts directory of the project. For Linux/macOS use `build.sh`, for Windows `build.ps1`.

> ***NOTE: The scripts have only been tested lightly for my own setups so please use these with caution and, as always, be sure to read the script files before executing them on your system.***

The scripts are setup to build the x64 version unless your system does not run on a 64-bit OS (not including ARM).

```bash
# Clone the repository to your computer
git clone https://gitlab.com/boring10/sam.git
cd sam

# Linux/macOS
scripts/./build.sh

# Windows
# You will need to allow scripts to be run in order to do this with PowerShell.
scripts\build.ps1
```

A directory called `bin` will be created that contains the binary file(s) once the script has completed. If you are using Linux/macOS then the binary file(s) will already be executable for you.
