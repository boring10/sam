# FAQ

## Can I export my profiles

Yes, you can export by using the JSON output format. There is not a way to import the profiles but you are able to access the profiles and copy them manually (see [Directories and Files](resources/directories-and-files.md) for the location for your system).

```bash
sam config profiles list all --output json > /path/to/output/file.json
```

## Are the profiles encrypted

No, the profiles are not encrypted. All directories and files are saved in the current user's home folder and not system wide.

## Do I need to install Node.js to use this tool

No you do not need to install Node.js to use this tool. This tool is a fully bundled application that is portable.

## Is there any data collected with the application

No information is collected about you or your system unless you post that information with an issue.

Outside of the API calls to the instance of Snipe-IT in the active profile, the only other request to the Internet is made to check for updates. The update only happens once per 7 day period and you can disable this if you would like.

``` bash
sam config settings checkforupdates \
  --enabled false
```

## How can I test this application before using it with my instance of Snipe-IT

If you would like to test against a development API then I would recommend using <https://snipe-it.readme.io/reference#api-overview>. You can find the information you need there to create a SAM profile with the development API key.

