const yargs = require('yargs')

const commands = [
  'licenses',
  'licenses delete',
  'licenses delete byid',
  'licenses get',
  'licenses get byid',
  'licenses get search',
  'licenses patch',
  'licenses patch byid',
  'licenses post',
  'licenses post create',
]

describe('Command help for licenses', () => {
  commands.forEach((command) => {
    describe(`${command} --help`, () => {
      it('should return help output', async () => {
        // Initialize parser using the command module
        const parser = yargs.command(require('../../bin/sam')).help()
        // Run the command module with --help as argument
        const output = await new Promise((resolve) => {
          parser.parse(`${command} --help`, (err, argv, output) => {
            resolve(output)
          })
        })
        // Verify the output is correct
        expect(output).toEqual(expect.stringContaining(`sam ${command}`))
      })
    })
  })
})
