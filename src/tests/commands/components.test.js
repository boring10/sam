const yargs = require('yargs')

const commands = [
  'components',
  'components delete',
  'components delete byid',
  'components get',
  'components get assets',
  'components get assets byid',
  'components get byid',
  'components get search',
  'components patch',
  'components patch byid',
  'components post',
  'components post create',
]

describe('Command help for components', () => {
  commands.forEach((command) => {
    describe(`${command} --help`, () => {
      it('should return help output', async () => {
        // Initialize parser using the command module
        const parser = yargs.command(require('../../bin/sam')).help()
        // Run the command module with --help as argument
        const output = await new Promise((resolve) => {
          parser.parse(`${command} --help`, (err, argv, output) => {
            resolve(output)
          })
        })
        // Verify the output is correct
        expect(output).toEqual(expect.stringContaining(`sam ${command}`))
      })
    })
  })
})
