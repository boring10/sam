const yargs = require('yargs')

const commands = [
  'config',
  'config release',
  'config release latest',
  'config settings',
  'config settings checkforupdates',
  'config profiles create',
  'config profiles delete',
  'config profiles delete all',
  'config profiles delete byname',
  'config profiles list',
  'config profiles list active',
  'config profiles list all',
  'config profiles list byname',
  'config profiles update',
  'config profiles update byname',
]

describe('Command help for config', () => {
  commands.forEach((command) => {
    describe(`${command} --help`, () => {
      it('should return help output', async () => {
        // Initialize parser using the command module
        const parser = yargs.command(require('../../bin/sam')).help()
        // Run the command module with --help as argument
        const output = await new Promise((resolve) => {
          parser.parse(`${command} --help`, (err, argv, output) => {
            resolve(output)
          })
        })
        // Verify the output is correct
        expect(output).toEqual(expect.stringContaining(`sam ${command}`))
      })
    })
  })
})
