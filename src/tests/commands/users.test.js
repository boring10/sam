const yargs = require('yargs')

const commands = [
  'users',
  'users delete',
  'users delete byid',
  'users get',
  'users get byid',
  'users get accessories byid',
  'users get assets byid',
  'users get licenses byid',
  'users get search',
  'users patch',
  'users patch byid',
  'users post',
  'users post create',
]

describe('Command help for users', () => {
  commands.forEach((command) => {
    describe(`${command} --help`, () => {
      it('should return help output', async () => {
        // Initialize parser using the command module
        const parser = yargs.command(require('../../bin/sam')).help()
        // Run the command module with --help as argument
        const output = await new Promise((resolve) => {
          parser.parse(`${command} --help`, (err, argv, output) => {
            resolve(output)
          })
        })
        // Verify the output is correct
        expect(output).toEqual(expect.stringContaining(`sam ${command}`))
      })
    })
  })
})
