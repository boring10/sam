const yargs = require('yargs')

const commands = [
  'locations',
  'locations delete',
  'locations delete byid',
  'locations get',
  'locations get byid',
  'locations get search',
  'locations patch',
  'locations patch byid',
  'locations post',
  'locations post create',
]

describe('Command help for locations', () => {
  commands.forEach((command) => {
    describe(`${command} --help`, () => {
      it('should return help output', async () => {
        // Initialize parser using the command module
        const parser = yargs.command(require('../../bin/sam')).help()
        // Run the command module with --help as argument
        const output = await new Promise((resolve) => {
          parser.parse(`${command} --help`, (err, argv, output) => {
            resolve(output)
          })
        })
        // Verify the output is correct
        expect(output).toEqual(expect.stringContaining(`sam ${command}`))
      })
    })
  })
})
