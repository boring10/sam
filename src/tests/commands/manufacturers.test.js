const yargs = require('yargs')

const commands = [
  'manufacturers',
  'manufacturers delete',
  'manufacturers delete byid',
  'manufacturers get',
  'manufacturers get byid',
  'manufacturers get search',
  'manufacturers patch',
  'manufacturers patch byid',
  'manufacturers post',
  'manufacturers post create',
]

describe('Command help for manufacturers', () => {
  commands.forEach((command) => {
    describe(`${command} --help`, () => {
      it('should return help output', async () => {
        // Initialize parser using the command module
        const parser = yargs.command(require('../../bin/sam')).help()
        // Run the command module with --help as argument
        const output = await new Promise((resolve) => {
          parser.parse(`${command} --help`, (err, argv, output) => {
            resolve(output)
          })
        })
        // Verify the output is correct
        expect(output).toEqual(expect.stringContaining(`sam ${command}`))
      })
    })
  })
})
