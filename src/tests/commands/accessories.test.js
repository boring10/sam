const yargs = require('yargs')

const commands = [
  'accessories',
  'accessories delete',
  'accessories delete byid',
  'accessories get',
  'accessories get byid',
  'accessories get checkedout',
  'accessories get checkedout byid',
  'accessories get search',
  'accessories patch',
  'accessories patch byid',
  'accessories post',
  'accessories post checkin bypivotid',
  'accessories post checkout byid',
  'accessories post create',
]

describe('Command help for accessories', () => {
  commands.forEach((command) => {
    describe(`${command} --help`, () => {
      it('should return help output', async () => {
        // Initialize parser using the command module
        const parser = yargs.command(require('../../bin/sam')).help()
        // Run the command module with --help as argument
        const output = await new Promise((resolve) => {
          parser.parse(`${command} --help`, (err, argv, output) => {
            resolve(output)
          })
        })
        // Verify the output is correct
        expect(output).toEqual(expect.stringContaining(`sam ${command}`))
      })
    })
  })
})
