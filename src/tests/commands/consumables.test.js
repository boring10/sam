const yargs = require('yargs')

const commands = [
  'consumables',
  'consumables delete',
  'consumables delete byid',
  'consumables get',
  'consumables get byid',
  'consumables get search',
  'consumables patch',
  'consumables patch byid',
  'consumables post',
  'consumables post create',
]

describe('Command help for consumables', () => {
  commands.forEach((command) => {
    describe(`${command} --help`, () => {
      it('should return help output', async () => {
        // Initialize parser using the command module
        const parser = yargs.command(require('../../bin/sam')).help()
        // Run the command module with --help as argument
        const output = await new Promise((resolve) => {
          parser.parse(`${command} --help`, (err, argv, output) => {
            resolve(output)
          })
        })
        // Verify the output is correct
        expect(output).toEqual(expect.stringContaining(`sam ${command}`))
      })
    })
  })
})
