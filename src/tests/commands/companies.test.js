const yargs = require('yargs')

const commands = [
  'companies',
  'companies delete',
  'companies delete byid',
  'companies get',
  'companies get byid',
  'companies get search',
  'companies patch',
  'companies patch byid',
  'companies post',
  'companies post create',
]

describe('Command help for companies', () => {
  commands.forEach((command) => {
    describe(`${command} --help`, () => {
      it('should return help output', async () => {
        // Initialize parser using the command module
        const parser = yargs.command(require('../../bin/sam')).help()
        // Run the command module with --help as argument
        const output = await new Promise((resolve) => {
          parser.parse(`${command} --help`, (err, argv, output) => {
            resolve(output)
          })
        })
        // Verify the output is correct
        expect(output).toEqual(expect.stringContaining(`sam ${command}`))
      })
    })
  })
})
