const yargs = require('yargs')

const commands = [
  'models',
  'models delete',
  'models delete byid',
  'models get',
  'models get byid',
  'models get search',
  'models patch',
  'models patch byid',
  'models post',
  'models post create',
]

describe('Command help for models', () => {
  commands.forEach((command) => {
    describe(`${command} --help`, () => {
      it('should return help output', async () => {
        // Initialize parser using the command module
        const parser = yargs.command(require('../../bin/sam')).help()
        // Run the command module with --help as argument
        const output = await new Promise((resolve) => {
          parser.parse(`${command} --help`, (err, argv, output) => {
            resolve(output)
          })
        })
        // Verify the output is correct
        expect(output).toEqual(expect.stringContaining(`sam ${command}`))
      })
    })
  })
})
