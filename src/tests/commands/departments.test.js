const yargs = require('yargs')

const commands = [
  'departments',
  'departments delete',
  'departments delete byid',
  'departments get',
  'departments get byid',
  'departments get search',
  'departments patch',
  'departments patch byid',
  'departments post',
  'departments post create',
]

describe('Command help for departments', () => {
  commands.forEach((command) => {
    describe(`${command} --help`, () => {
      it('should return help output', async () => {
        // Initialize parser using the command module
        const parser = yargs.command(require('../../bin/sam')).help()
        // Run the command module with --help as argument
        const output = await new Promise((resolve) => {
          parser.parse(`${command} --help`, (err, argv, output) => {
            resolve(output)
          })
        })
        // Verify the output is correct
        expect(output).toEqual(expect.stringContaining(`sam ${command}`))
      })
    })
  })
})
