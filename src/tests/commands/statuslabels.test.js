const yargs = require('yargs')

const commands = [
  'statuslabels',
  'statuslabels delete',
  'statuslabels delete byid',
  'statuslabels get',
  'statuslabels get assetlist byid',
  'statuslabels get byid',
  'statuslabels get search',
  'statuslabels patch',
  'statuslabels patch byid',
  'statuslabels post',
  'statuslabels post create',
]

describe('Command help for statuslabels', () => {
  commands.forEach((command) => {
    describe(`${command} --help`, () => {
      it('should return help output', async () => {
        // Initialize parser using the command module
        const parser = yargs.command(require('../../bin/sam')).help()
        // Run the command module with --help as argument
        const output = await new Promise((resolve) => {
          parser.parse(`${command} --help`, (err, argv, output) => {
            resolve(output)
          })
        })
        // Verify the output is correct
        expect(output).toEqual(expect.stringContaining(`sam ${command}`))
      })
    })
  })
})
