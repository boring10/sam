/**
 * @namespace FormatArgs
 */
const alias = require('../libs/optionAliases')

/**
 * @memberof FormatArgs
 * @description Format the args to be used by the program.
 *
 * @param {Object} argv - Arguments object supplied by yargs.
 *
 * @returns {void}
 */
const format = (argv) => {
  for (let i = 0; i < argv._.length; i++) {
    if (i === 0) {
      argv.section = argv._[i]
      argv.commands = []
    } else if (i === 1) {
      argv.method = argv._[i]
    } else {
      argv.commands.push(argv._[i])
    }
  }

  delete argv.$0 // Deletes the root command since it is not needed.

  /**
   * Unable to delete the property "_" without Yargs throwing the following error.
   *
   * ```
   * TypeError: Cannot read property "0" of undefined"
   * ```
   *
   * Due to this the properties value is just set to an empty string.
   */
  argv._ = ''

  /**
   * Remove the option aliases since the full option name is used for API and
   * configuration purposes.
   */
  Object.keys(argv).forEach((item) => {
    if (alias(item)) {
      let x = alias(item)

      x.forEach((a) => {
        delete argv[a]
      })
    }
  })

  argv.options = {}
  let exclude = ['_', 'section', 'commands', 'method', 'options']
  Object.keys(argv).forEach((item) => {
    if (!exclude.includes(item)) {
      argv.options[item] = argv[item]
      delete argv[item]
    }
  })
}

module.exports = { format }
