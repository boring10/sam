/**
 * @namespace InitSetup
 */
const files = require('../libs/files')

/**
 * @memberof InitSetup
 * @description Initialize any directories and/or files.
 *
 * @returns {void}
 */
const initSetup = (argv) => {
  const fileLocations = files.fileLocations()

  files.createDir(fileLocations.dir)
  files.createDir(fileLocations.logDir)
  files.createFile(fileLocations.config)
  files.createFile(fileLocations.profiles)
  files.createFile(fileLocations.logs)

  return argv
}

module.exports = initSetup
