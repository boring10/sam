/**
 * @namespace GenerateConfig
 */
const path = require('path')
const { URL } = require('url')

/**
 * @memberof GenerateConfig
 * @description Create a configuration key that will be added to the argv variable
 * to allow for easier re-use.
 */
const generateConfig = (argv) => {
  const activeProfile = argv.activeProfile[Object.keys(argv.activeProfile)[0]]

  /**
   * Replace all PUT methods with the PATCH method
   */
  if (argv.method === 'put') {
    argv.method = 'patch'
  }

  let baseURL = new URL(activeProfile.url)
  baseURL.pathname = path.join(
    baseURL.pathname,
    'api',
    activeProfile.api_version,
    argv.section
  )

  argv.config = {
    method: argv.method,
    baseURL: baseURL.href,
    url: '',
    headers: {
      authorization: `Bearer ${activeProfile.api_key}`,
      accept: 'application/json',
      'content-type': 'application/json',
    },
    data: argv.options,
  }

  return argv
}

module.exports = generateConfig
