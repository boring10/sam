/**
 * @namespace ActiveProfile
 */
const fs = require('fs')

let profiles = '{}'
if (fs.existsSync(require('../libs/files').fileLocations().profiles)) {
  profiles = require(require('../libs/files').fileLocations().profiles)
}

/**
 * @memberof ActiveProfile
 * @description Add the active profile to the argv variable if an active profile exists.
 */
const activeProfile = (argv) => {
  const profile = {}

  profile['activeProfile'] = {}

  let activeProfile = false

  /**
   * Don't use a user profile when performing tests, primarily integration tests.
   */
  if (process.env.NODE_ENV.toLowerCase() === 'test') {
    const _TEST_MODE_PROFILE_ = (profile.activeProfile._TEST_MODE_PROFILE_ = {})

    _TEST_MODE_PROFILE_.active = true
    _TEST_MODE_PROFILE_.url = process.env.SAM_TEST_URL
    _TEST_MODE_PROFILE_.api_key = process.env.SAM_TEST_API_KEY
    _TEST_MODE_PROFILE_.api_version = process.env.SAM_TEST_API_VERSION
    _TEST_MODE_PROFILE_.api_throttle = process.env.SAM_TEST_API_THROTTLE
    _TEST_MODE_PROFILE_.auto_asset_tag = process.env.SAM_TEST_AUTO_ASSET_TAG

    return profile
  }

  Object.keys(profiles).forEach((key) => {
    if (profiles[key].active) {
      activeProfile = true

      profile.activeProfile[key] = {}
      Object.keys(profiles[key]).forEach((item) => {
        profile.activeProfile[key][item] = profiles[key][item]
      })
    }
  })

  if (
    !activeProfile &&
    argv.section !== 'config' &&
    argv.method !== 'profiles'
  ) {
    argv.error = `No active profile exists.

Create a new profile with the following command:
  sam config profiles create

Update an existing profile to be the active profile:
  sam config profiles update --name <name> --active true
or
  sam config profiles update --name <name> --active
      `
    return argv
  }

  return profile
}

module.exports = activeProfile
