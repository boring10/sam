/**
 * @namespace CheckForUpdates
 */
const axios = require('axios')
const log = require('../libs/log')
const files = require('../libs/files')
const writeFile = require('../libs/writeFile')
const fileLocations = files.fileLocations()
const config = require(fileLocations.config)

/**
 * @memberof CheckForUpdates
 * @description Determine if checking for updates is enabled and check for any updates.
 *
 * @returns {void}
 */
const checkForUpdates = async () => {
  const date = new Date().toISOString().split('T')[0]
  const current = require('../../package.json').version

  if (
    config.check_for_updates.enabled &&
    process.env.GITLAB_ENABLE_UPDATE_CHECK === '1'
  ) {
    if (getDateDiff(date) >= 7) {
      try {
        const data = await axios({
          method: 'GET',
          baseURL: `${process.env.GITLAB_PROJECT_API}/releases`,
          headers: {
            accept: 'application/json',
            'content-type': 'application/json',
          },
        }).then((response) => {
          return response.data[0]
        })

        const latest = data.name.replace(/v/, '')

        if (latest !== current) {
          console.log('----------------------')
          console.log('Update available:')
          console.log(`Current: v${current}`)
          console.log(`Latest: v${latest}`)
          console.log('----------------------')
          console.log('')
        }
      } catch (error) {
        if (!error.response) {
          log.error('Unable to check for updates at this time.')
        } else {
          log.error(error.response.data)
        }
      }

      config.check_for_updates.last_update_check = date

      writeFile.config(config)
    }
  }
}

/**
 * @memberof CheckForUpdates
 * @private
 *
 * https://stackoverflow.com/a/45594038/12066662
 *
 * @description Get the number of days since that last time an update check was performed.
 *
 * @param {string} dateStr Date string formatted in YYYY-MM-DD
 *
 * @returns {number} Number of days since the last update check.
 */
function getDateDiff(dateStr) {
  let currentDate = new Date(dateStr)
  let lastCheck = new Date(config.check_for_updates.last_update_check)

  /**
   * Ensure that the date is valid, if not, default to the current day and update
   * the config file.
   */
  if (isNaN(lastCheck)) {
    lastCheck = new Date()

    config.check_for_updates.last_update_check = lastCheck
      .toISOString()
      .split('T')[0]

    writeFile.config(config)
  }

  let timeDiff = Math.abs(lastCheck.getTime() - currentDate.getTime())
  let dayDiff = Math.ceil(timeDiff / (1000 * 3600 * 24))

  return parseInt(dayDiff)
}

module.exports = checkForUpdates
