/**
 * @namespace AppendToConfigUrl
 */
const path = require('path')
const { URL } = require('url')

/**
 * @memberof AppendToConfigUrl
 * @description Adds the ID to the config for the url.
 *
 * @returns {Object}
 */
const id = (argv) => {
  argv.config.url = `${argv.config.data.id}`
  delete argv.config.data.id

  return argv
}

/**
 * @memberof AppendToConfigUrl
 * @description Adds the asset tag to the config for the url.
 *
 * @returns {object}
 */
const assetTag = (argv) => {
  let baseUrl = new URL(argv.config.baseURL)
  baseUrl.pathname = path.join(baseUrl.pathname, 'bytag')

  argv.config.baseURL = baseUrl.href
  argv.config.url = `${argv.config.data.asset_tag}`
  delete argv.config.data.asset_tag

  return argv
}

/**
 * @memberof AppendToConfigUrl
 * @description Adds the serial to the config for the url.
 *
 * @returns {object}
 */
const serial = (argv) => {
  let baseUrl = new URL(argv.config.baseURL)
  baseUrl.pathname = path.join(baseUrl.pathname, 'byserial')

  argv.config.baseURL = baseUrl.href
  argv.config.url = `${argv.config.data.serial}`
  delete argv.config.data.serial

  return argv
}

/**
 * @memberof AppendToConfigUrl
 * @description Adds the ID to the config for the url before the last command.
 *
 * @returns {Object}
 */
const idBeforeLastCommand = (argv) => {
  let baseUrl = new URL(argv.config.baseURL)
  baseUrl.pathname = path.join(
    baseUrl.pathname,
    argv.config.data.id.toString(),
    argv.commands[0]
  )

  argv.config.baseURL = baseUrl.href
  delete argv.config.data.id

  return argv
}

/**
 * @memberof AppendToConfigUrl
 * @description Append commands in order to the URL.
 *
 * @returns {Object}
 */
const inOrder = (argv) => {
  let baseUrl = new URL(argv.config.baseURL)
  argv.commands.forEach((command) => {
    baseUrl.pathname = path.join(baseUrl.pathname, command)
  })

  argv.config.baseURL = baseUrl.href
  return argv
}

/**
 * @memberof AppendToConfigUrl
 * @description Append all except the last command.
 */
const excludeLastCommand = (argv) => {
  let baseUrl = new URL(argv.config.baseURL)

  for (let i = 0; i < argv.commands.length - 1; i++) {
    baseUrl.pathname = path.join(baseUrl.pathname, argv.commands[i])
  }

  argv.config.baseURL = baseUrl.href

  return argv
}

module.exports = {
  id,
  assetTag,
  serial,
  idBeforeLastCommand,
  inOrder,
  excludeLastCommand,
}
