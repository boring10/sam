const formatArgs = require('./formatArgs')

describe.skip('formatArgs', () => {
  let argvObj = {}

  describe('formatArgs.format()', () => {
    afterEach(() => {
      argvObj = {}
    })

    it('should be an object', () => {
      argvObj._ = ['hardware', 'get', 'byid']
      argvObj.$0 = 'sam'

      let formattedObj = formatArgs.format(argvObj)

      expect(typeof formattedObj).toBe('object')
    })

    it('should return a nested object with an array of commands', () => {
      argvObj = {
        _: ['hardware', 'get', 'byid'],
        $0: 'sam',
        id: 4,
      }

      let formattedObj = formatArgs.format(argvObj)

      expect(formattedObj).toEqual(
        expect.objectContaining({
          section: expect.any(String),
          method: expect.any(String),
          commands: expect.arrayContaining([expect.any(String)]),
          options: expect.objectContaining({
            id: expect.any(Number),
          }),
        })
      )
    })

    it('should return a nested object with an empty array for commands', () => {
      argvObj = {
        _: ['profiles', 'create'],
        $0: 'sam',
        option1: '123456',
        option2: 'http://localhost',
        profilename: 'test',
      }

      let formattedObj = formatArgs.format(argvObj)

      expect(formattedObj).toEqual(
        expect.objectContaining({
          section: expect.any(String),
          method: expect.any(String),
          commands: expect.arrayContaining([]),
          options: expect.objectContaining({
            option1: expect.any(String),
            option2: expect.any(String),
            profilename: expect.any(String),
          }),
        })
      )
    })
  })
})
