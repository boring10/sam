/**
 * @namespace StopProcess
 */
const log = require('../libs/log')

/**
 * @memberof StopProcess
 * @description Stop the execution of the program and log the error message.
 *
 * @returns {void}
 */
const stopProcess = (args) => {
  if (args.error) {
    log.error(args.error)

    process.exit(1)
  }
}

module.exports = stopProcess
