/**
 * @namespace ReplaceHtmlCodes
 */

/**
 * @memberof ReplaceHtmlCodes
 * @description Replace HTML codes with the actual characters.
 *
 * @param {string} str String to perform a regex replacement.
 *
 * @returns {string}
 */
const replaceHtmlCodes = (str) => {
  const htmlCodes = []

  htmlCodes.push(['&quot;', '"'])
  htmlCodes.push(['&#039;', "'"])
  htmlCodes.push(['&lt;', '<'])
  htmlCodes.push(['&gt;', '>'])

  if (str !== null && typeof str !== 'number' && str !== '') {
    htmlCodes.forEach((code) => {
      let regex = new RegExp(code[0], 'g')

      str = str.toString().replace(regex, code[1])
    })
  }

  return str
}

module.exports = replaceHtmlCodes
