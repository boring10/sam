const fs = require('fs')
const path = require('path')

require('dotenv').config({ path: path.join(__dirname, '../.env') })
const log = require('./log')

describe('logs', () => {
  describe('logs.dateTime()', () => {
    it('should return a string', () => {
      expect(typeof log.dateTime()).toBe('string')
    })

    it('should be a length of 20', () => {
      expect(log.dateTime().length).toBe(30)
    })

    it('should contain "T"', () => {
      expect(log.dateTime().includes('T')).toBe(true)
    })

    it('should contain "Z"', () => {
      expect(log.dateTime().includes('Z')).toBe(true)
    })

    it('should contain "UTC"', () => {
      expect(log.dateTime().includes('UTC')).toBe(true)
    })
  })

  describe('log.appendToFile()', () => {
    beforeEach(() => {
      fs.appendFileSync = jest.fn()
    })

    afterEach(() => {
      jest.clearAllMocks()
    })

    it('should NOT call fs.appendFileSync() if no argument', () => {
      log.appendToFile('')

      expect(fs.appendFileSync).not.toHaveBeenCalled()
    })

    it('should call fs.appendFileSync once', () => {
      log.appendToFile('some logging information.')

      expect(fs.appendFileSync).toHaveBeenCalled()
    })
  })
})
