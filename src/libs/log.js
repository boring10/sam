/**
 * @namespace Log
 */
const fs = require('fs')
const { logs } = require('../libs/files').fileLocations()

/**
 * @memberof Log
 * @description Format of the date and time that will be used for logging purposes.
 *
 * @returns {string} YYYY-MM-DDTHH:MM:SSZ
 */
const dateTime = () => {
  const date = new Date()
  const UTC = (date.getTimezoneOffset() / 60) * -1

  if (UTC > 0) {
    UTC = `+${UTC}`
  }

  return `${date.toISOString()} UTC${UTC}`
}

/**
 * @memberof Log
 * @description Append the log information to the log file.
 *
 * @returns {void}
 */
const appendToFile = (str) => {
  if (str) {
    fs.appendFileSync(logs, `${dateTime()} ${str}\n`)
  }
}

/**
 * @memberof Log
 * @description Format a response log entry for the log file. Also outpus the response to the console.
 *
 * @param {string} str Text to be logged.
 * @param {boolean} verbose Whether the response is output to the console as well.
 *
 * @returns {void}
 */
const response = (str, verbose = true) => {
  if (verbose) {
    console.log(str)
  }

  appendToFile(`[RESPONSE]: ${str}`)
}

/**
 * @memberof Log
 * @description Flag the log entry as an error.
 *
 * @todo This function needs to be vetted to ensure that it is working as expected.
 *
 * @returns {string}
 */
const error = (messages) => {
  if (typeof messages === 'object') {
    Object.keys(messages).forEach((message) => {
      if (!Array.isArray(message)) {
        console.log(`${message} - ${messages[message]}`)
        appendToFile(`[ERROR]: ${message} - ${messages[message]}`)
      } else {
        let array = messages[message]

        array.forEach((str) => {
          console.log(`${message} - ${messages[message]}`)
          appendToFile(`[ERROR]: ${message} - ${str}`)
        })
      }
    })
  } else {
    console.log(messages)
    appendToFile(`[ERROR]: ${messages}`)
  }
}

module.exports = { dateTime, appendToFile, response, error }
