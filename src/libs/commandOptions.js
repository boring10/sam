/**
 * @namespace CommandOptions
 */

/**
 * @class
 * @description Retrieves the option information for the command.
 */
class CommandOptions {
  /**
   * @private
   * @description Accepts an array of commands.
   *
   * @param {Object} commands Array of command line arguments.
   * @param {string} commands.section Root command that was entered.
   * @param {string} commands.method HTTP request that was entered.
   * @param {string} commands.command1 Subcommand that was entered.
   */
  constructor(commands) {
    this.section = commands[0]
    this.method = commands[1]
    this.command1 = commands[2]
    this.command2 = commands[3]
  }

  /**
   * @private
   * @description Command option to return an object for.
   *
   * @param {string} optionName Name of the desired option.
   *
   * @returns {Object}
   */
  option(optionName) {
    return require(`../commands/options/${optionName}`)(this)
  }
}

module.exports = CommandOptions
