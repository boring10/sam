const fs = require('fs')
const path = require('path')

require('dotenv').config({ path: path.join(__dirname, '../.env') })
const files = require('./files')

describe('files', () => {
  describe('files.fileLocations()', () => {
    let filesObj = files.fileLocations()

    it('should be an object', () => {
      expect(typeof filesObj).toBe('object')
    })

    it('should have properties', () => {
      expect(filesObj).toEqual(
        expect.objectContaining({
          dir: expect.any(String),
          logDir: expect.any(String),
          profiles: expect.any(String),
          logs: expect.any(String),
        })
      )
    })
  })

  describe('files.createDir()', () => {
    let createDir = files.createDir
    let dir = 'sam'

    beforeEach(() => {
      fs.existsSync = jest.fn()
      fs.mkdirSync = jest.fn()
    })

    afterAll(() => {
      jest.clearAllMocks()
    })

    it('should create a directory', () => {
      fs.existsSync.mockReturnValue(false)

      createDir(dir)
      expect(fs.mkdirSync).toHaveBeenCalled()
    })

    it('should NOT create a directory', () => {
      fs.existsSync.mockReturnValue(true)

      createDir(dir)
      expect(fs.mkdirSync).not.toHaveBeenCalled()
    })
  })

  describe('files.createFile()', () => {
    let createFile = files.createFile
    let file

    describe('profiles.json', () => {
      beforeEach(() => {
        fs.existsSync = jest.fn()
        fs.writeFileSync = jest.fn()
        file = 'profiles.json'
      })

      afterAll(() => {
        jest.clearAllMocks()
      })

      it('should create a file called "profiles.json"', () => {
        fs.existsSync.mockReturnValue(false)

        createFile(file)
        expect(fs.writeFileSync).toHaveBeenCalled()
      })

      it('should NOT create a file called "profiles.json"', () => {
        fs.existsSync.mockReturnValue(true)

        createFile(file)
        expect(fs.writeFileSync).not.toHaveBeenCalled()
      })
    })

    describe('sam.log', () => {
      beforeEach(() => {
        fs.existsSync = jest.fn()
        fs.writeFileSync = jest.fn()
        file = 'sam.log'
      })

      afterAll(() => {
        jest.clearAllMocks()
      })

      it('should create a file called "sam.log"', () => {
        fs.existsSync.mockReturnValue(false)

        createFile(file)
        expect(fs.writeFileSync).toHaveBeenCalled()
      })

      it('should NOT create a file called "sam.log"', () => {
        fs.existsSync.mockReturnValue(true)

        createFile(file)
        expect(fs.writeFileSync).not.toHaveBeenCalled()
      })
    })
  })
})
