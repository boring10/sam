/**
 * @namespace Files
 */
const fs = require('fs')
const os = require('os')
const homedir = os.homedir()
const path = require('path')

/**
 * @memberof Files
 * @description Directory structure for files that are used by the application.
 *
 * @property {string} dir - The full path to the configuration directory.
 * @property {string} logDir - The full path to the logs directory.
 * @property {string} profiles - The full path to the profiles.json file.
 * @property {string} logs - The full path to the current log.
 */
const fileLocations = () => {
  const date = new Date()
  const year = date.getFullYear()
  const month = ('0' + (date.getMonth() + 1)).slice(-2)

  let dir
  if (process.env.USE_HOME_DIR) {
    if (os.platform() !== 'win32') {
      dir = path.resolve(homedir, process.env.SAM_CONFIG_DIR)
    } else {
      dir = path.resolve(homedir, process.env.SAM_APPDATA_DIR)
    }
  } else {
    dir = path.resolve(process.env.SAM_ROOT_DIR)
  }
  const logDir = path.join(dir, process.env.SAM_LOG_DIR)
  const config = path.join(dir, process.env.SAM_CONFIG_FILE)
  const profiles = path.join(dir, process.env.SAM_PROFILES_FILE)
  const logs = path.join(logDir, `${year}${month}.log`)

  return { dir, logDir, config, profiles, logs }
}

/**
 * @memberof Files
 * @description Creates a new directory if it does not already exist.
 *
 * @param {string} dir The full path to the directory.
 *
 * @returns {void}
 */
const createDir = (dir) => {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir)
  }
}

/**
 * @memberof Files
 * @description Create a new file if it does not already exits.
 *
 * @param {string} file The full path to the file.
 *
 * @returns {void}
 */
const createFile = (file) => {
  /**
   * @private
   * @description Default content for the file to be created.
   *
   * @param {string} fileName The full path to the file.
   *
   * @returns {string} Default content of the file.
   */
  function fileContents(fileName) {
    if (fileName.includes('profiles')) {
      return '{}'
    }

    if (fileName.includes('config.json')) {
      return JSON.stringify({
        check_for_updates: {
          enabled: true,
          last_update_check: '',
        },
      })
    }

    return ''
  }

  if (!fs.existsSync(file)) {
    fs.writeFileSync(file, fileContents(file))
  }
}

module.exports = { fileLocations, createDir, createFile }
