/**)
 * @namespace OutputDisplay
 */
const fs = require('fs')

const parseObjectProperties = require('./parseObjectProperties')
const replaceHtmlCodes = require('./replaceHtmlCodes')

/**
 * @memberof OutputDisplay
 *
 * @param {Object} data Array or object that will be parsed.
 * @param {string} outputFormatType Formatting of the output.
 * @param {string} fields String of fields that are separated by a comma.
 *
 * @returns {void}
 */
const outputDisplay = (
  data,
  outputFormatType = 'default',
  fields = false,
  export_to_file = ''
) => {
  if (outputFormatType === 'default') {
    return defaultOutput(data, fields)
  }

  if (outputFormatType === 'json') {
    return jsonOutput(data, fields)
  }

  if (outputFormatType === 'csv') {
    return csvOutput(data, fields, export_to_file)
  }
}

/**
 * @memberof OutputDisplay
 * @private
 */
function defaultOutput(data, fields) {
  /**
   * @private
   */
  function outputFn(obj, fields) {
    let propKey = Object.keys(obj)[0]

    if (fields) {
      if (fields.split(',').includes(propKey)) {
        return `${propKey}: ${obj[propKey]}\n`
      }

      return ''
    } else {
      return `${propKey}: ${obj[propKey]}\n`
    }
  }

  let output = ''

  if (data.rows) {
    data.rows.forEach((row) => {
      parseObjectProperties(row).forEach((item) => {
        if (!output) {
          output = outputFn(item, fields)
        } else {
          let tmpItem = item[Object.getOwnPropertyNames(item)[0]]

          if (typeof tmpItem === 'string') {
            item[Object.getOwnPropertyNames(item)[0]] = tmpItem
              .replace(/\n/g, ' ')
              .replace(/\r/g, '')
          }

          output += outputFn(item, fields)
        }
      })

      output += '\n'
    })
  } else {
    parseObjectProperties(data).forEach((item) => {
      if (!output) {
        output = outputFn(item, fields)
      } else {
        let tmpItem = item[Object.getOwnPropertyNames(item)[0]]

        if (typeof tmpItem === 'string') {
          item[Object.getOwnPropertyNames(item)[0]] = tmpItem
            .replace(/\n/g, ' ')
            .replace(/\r/g, '')
        }

        output += outputFn(item, fields)
      }
    })
  }

  return output.trim()
}

/**
 * @memberof OutputDisplay
 * @private
 */
function jsonOutput(data, fields) {
  if (data.rows) {
    let parentArray = []

    data.rows.forEach((row) => {
      let str = defaultOutput(row, fields)
      let array = str.split(/\n/)
      let obj = {}

      array.forEach((item) => {
        let key = item.split(':')[0]
        let value = item.split(': ')[1]

        obj[key] = value
      })

      parentArray.push(obj)
    })

    return parentArray
  } else {
    let str = defaultOutput(data, fields)
    let array = str.split(/\n/)
    let obj = {}

    array.forEach((item) => {
      let key = item.split(':')[0]
      let value = item.split(': ')[1]

      obj[key] = value
    })

    return obj
  }
}

/**
 * @memberof OutputDisplay
 * @private
 */
function csvOutput(data, fields, export_to_file) {
  let csv = []
  let header = []

  if (!data.rows) {
    appendHeader(data)

    header = moveIdToFront(header)
    csv.push(header)

    appendRow(data)
  } else {
    data.rows.forEach((row) => {
      appendHeader(row)
    })

    header = moveIdToFront(header)
    csv.push(header)

    data.rows.forEach((row) => {
      appendRow(row)
    })
  }

  function moveIdToFront(array) {
    if (!array.includes('id')) {
      return array
    }

    array.sort()
    array = array.filter((item) => item !== 'id')
    array.splice(0, 0, 'id')

    return array
  }

  function appendHeader(row) {
    let objKeys = parseObjectProperties(row)

    objKeys.forEach((item) => {
      let objKey = Object.keys(item)[0]

      if (!fields || fields.split(',').includes(`${objKey}`)) {
        if (!header.includes(`${objKey}`)) {
          header.push(objKey)
        }
      }
    })
  }

  function appendRow(row) {
    let item = parseObjectProperties(row)
    let array = Array(header.length - 1)

    item.forEach((obj) => {
      Object.keys(obj).forEach((key) => {
        if (header.includes(key)) {
          let index = header.indexOf(key)

          if (obj[key] === null || obj[key] === undefined) {
            array.splice(index, 1, '')
          } else {
            if (typeof obj[key] === 'string') {
              let str = obj[key].replace(/\n/g, ' ').replace(/\r/g, '')

              if (str.includes(',')) {
                array.splice(index, 1, `"${str}"`)
              } else {
                array.splice(index, 1, `${str}`)
              }
            } else {
              array.splice(index, 1, `${obj[key]}`)
            }

            // if (typeof obj[key] === 'string' && obj[key].includes(',')) {
            //   array.splice(index, 1, `"${obj[key]}"`)
            // } else {
            //   array.splice(index, 1, `${obj[key]}`)
            // }
          }
        }
      })
    })
    csv.push([array])
  }

  flattenArray = []

  csv.forEach((arrayRow) => {
    flattenArray.push([arrayRow.join(',')])
  })

  flattenArray.forEach((flatten) => {
    let stream
    if (export_to_file) {
      fs.writeFileSync(export_to_file, '')

      stream = fs.createWriteStream(export_to_file, { flags: 'a' })
    }

    flatten.forEach((row) => {
      if (export_to_file) {
        stream.write(`${row}\n`)
      } else {
        console.log(row)
      }
    })
  })

  return ''
}

module.exports = outputDisplay
