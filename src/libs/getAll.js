const axios = require('axios')
const outputDisplay = require('./outputDisplay')

/**
 * @memberof GetAll
 * @description
 * Get all of the results for the query.
 *
 * @param {Object} config - Configuration from argv.
 * @param {Array | boolean} fields - Specified fields, if any.
 *
 * @returns {void}
 */
async function getAll(config, fields) {
  const limit = 500

  config.data.limit = limit
  config.data.offset = 0

  let resData = await axios(config).then((x) => x.data)
  const total = resData.total
  let numOfRows = resData.rows.length

  let data = resData.rows
  console.log(`Retrieved: ${numOfRows} of ${total}`)

  const runMore = async () => {
    if (data.length < total) {
      const offset =
        config.data.offset + limit < total ? config.data.offset + limit : limit

      config.data.offset = offset
      const resData = await axios(config).then((x) => x.data)
      data = data.concat(resData.rows)
      console.log(`Retrieved: ${offset + resData.rows.length} of ${total}`)

      if (offset !== total) await runMore()
    }
  }

  await runMore()

  outputDisplay(
    { rows: data },
    config.data.output,
    fields,
    config.data.export_to_file
  )
}

module.exports = getAll
