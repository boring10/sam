const axios = require('axios')

const log = require('./log')
const outputDisplay = require('./outputDisplay')

exports.hardwareGetByTag = async (argv, captureReturn) => {
  const fields = argv.config.data.fields || false

  try {
    const response = await axios(argv.config)
    const data = response.data

    if (data.status === 'error') {
      log.error(data.messages)
      return
    }

    if (captureReturn) {
      return outputDisplay(data, 'json', false)
    }

    console.log(outputDisplay(data, argv.config.data.output, fields))
  } catch (error) {
    log.error(error.response)
  }
}

exports.hardwarePatchById = async (argv) => {
  try {
    let response = await axios(argv.config)
    let data = response.data
    const payload = data.payload

    if (data.status === 'success') {
      const assetInfo = (function () {
        let str = `(asset tag: ${payload.asset_tag}`

        if (payload.name) {
          return `${str}, name: ${payload.name})`
        } else {
          return `${str})`
        }
      })()

      log.response(
        `Successfully updated the asset with the ID "${payload.id}" ${assetInfo}`
      )
    } else {
      console.log(`${data.messages} (ID: ${argv.config.url})`)
    }
  } catch (err) {
    console.log(err)
  }
}
