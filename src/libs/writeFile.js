/**
 * @namespace WriteFile
 */
const fs = require('fs')
const fileLocations = require('./files').fileLocations()
const files = require('./files')

/**
 * @memberof WriteFile
 * @description Overwrites the profiles that are in the JSON file.
 *
 * @returns {void}
 */
const profiles = (newProfiles) => {
  if (!fs.existsSync(fileLocations.profiles)) {
    files.createDir(fileLocations.dir)
    files.createFile(fileLocations.profiles)
  }

  fs.writeFileSync(fileLocations.profiles, JSON.stringify(newProfiles))
}

/**
 * @memberof WriteFile
 * @description Overwrites the config settings that are in a JSON file.
 *
 * @returns {void}
 */
const config = (newConfig) => {
  if (!fs.existsSync(fileLocations.config)) {
    files.createDir(fileLocations.dir)
    files.createFile(fileLocations.config)
  }

  fs.writeFileSync(fileLocations.config, JSON.stringify(newConfig))
}

module.exports = { profiles, config }
