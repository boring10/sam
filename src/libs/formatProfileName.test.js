const formatProfileName = require('./formatProfileName')

describe('formatProfileName()', () => {
  let profileName

  it('should be a string', () => {
    profileName = 'Test'

    expect(typeof formatProfileName(profileName)).toBe('string')
  })

  it('should NOT include spaces', () => {
    profileName = 'Test Test'

    expect(formatProfileName(profileName).includes(' ')).toBe(false)
  })

  it('should NOT include capital letters', () => {
    profileName = 'Test tEst'

    expect(formatProfileName(profileName)).toEqual('testtest')
  })
})
