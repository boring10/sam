/**
 * @namespace CommandSection
 */

/**
 * @memberof CommandSection
 * @description Returns the root command (section).
 *
 * @param {Object} commands Array of commands.
 *
 * @returns {string} Root command that was entered.
 */
const commandSection = (commands) => {
  return commands[0]
}

module.exports = commandSection
