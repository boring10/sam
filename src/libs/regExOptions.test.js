const regExOptions = require('./regExOptions')

describe('regexOptions', () => {
  describe('Method: macAddress', () => {
    it('should be a string', () => {
      expect(typeof regExOptions.macAddress('aabbcc112233')).toBe('string')
    })
  })
})
