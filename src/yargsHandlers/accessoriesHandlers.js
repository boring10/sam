/**
 * @namespace AccessoriesHandlers
 */
const axios = require('axios')

const appendToConfigUrl = require('../middleware/appendToConfigUrl')
const log = require('../libs/log')
const outputDisplay = require('../libs/outputDisplay')
const getAll = require('../libs/getAll')
const deepClone = require('../libs/deepClone')

/**
 * @memberof AccessoriesHandlers
 * @description
 * Delete a specific accessory by the ID.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function deleteByid(argv) {
  const fields = argv.config.data.fields || false
  const adjustedArgv = deepClone(argv)
  adjustedArgv.config.method = 'get'

  let getData = await getByid(adjustedArgv, true)

  /**
   * Stop if there was no asset found.
   */
  if (!getData) return

  try {
    const response = await axios(argv.config)
    const data = response.data

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully deleted the accessory with the ID of "${response.config.url}".`
    )

    if (fields) {
      console.log(outputDisplay(getData, argv.config.data.output, fields))
    }
  } catch (error) {
    log.error(error.response.data)
  }
}

/**
 * @memberof AccessoriesHandlers
 * @description
 * Get the details of a specific accessory by the ID.
 *
 * @param {object} argv
 * @param {boolean} [captureReturn] - Returns the output instead of displaying to console.
 *
 * @returns {void | object}
 */
async function getByid(argv, captureReturn) {
  const fields = argv.config.data.fields || false

  try {
    const response = await axios(argv.config)
    const data = response.data

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    /**
     * Always returns output in JSON format.
     */
    if (captureReturn) {
      return outputDisplay(data, 'json', false)
    }

    console.log(outputDisplay(data, argv.config.data.output, fields))
  } catch (error) {
    log.error(error.response)
  }
}

/**
 * @memberof AccessoriesHandlers
 * @description
 * Get the users that the accessory is checked out to by the accessory ID.
 *
 * @param {object} argv
 * @param {boolean} [captureReturn] - Returns the output instead of displaying to console.
 *
 * @returns {void | object}
 */
async function getCheckedoutByid(argv, captureReturn) {
  const fields = argv.config.data.fields || false

  if (!argv.options.all) {
    try {
      const response = await axios(argv.config)
      const data = response.data

      if (data.status === 'error') {
        log.error(data.messages)

        return
      }

      /**
       * Always returns output in JSON format.
       */
      if (captureReturn) {
        return outputDisplay(data, 'json', false)
      }

      console.log(outputDisplay(data, argv.config.data.output, fields))
    } catch (error) {
      log.error(error.response.data)
    }
  } else {
    getAll(argv.config, fields)
  }
}

/**
 * @memberof AccessoriesHandlers
 * @description
 * Search for the accessories that you would like to retrieve.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function getSearch(argv) {
  const fields = argv.config.data.fields || false

  if (!argv.options.all) {
    try {
      const response = await axios(argv.config)
      const data = response.data

      if (data.status === 'error') {
        log.error(data.messages)

        return
      }

      console.log(outputDisplay(data, argv.config.data.output, fields))
    } catch (error) {
      log.error(error.response.data)
    }
  } else {
    getAll(argv.config, fields)
  }
}

/**
 * @memberof AccessoriesHandlers
 * @description
 * Update an existing accessory by the ID.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function patchByid(argv) {
  const fields = argv.config.data.fields || false

  try {
    const response = await axios(argv.config)
    const data = response.data
    const payload = data.payload

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully updated the accessory with the ID "${payload.id}" named "${payload.name}" with a quantity of "${payload.qty}".`
    )

    if (fields) {
      argv.config.method = 'get'
      await getByid(argv)
    }
  } catch (err) {
    log.error(err)
  }
}

// TODO: Work on gathering more detail about the checkin
/**
 * @memberof AccessoriesHandlers
 * @description
 * Check in an accessory using the assigne pivot ID. This is different from the
 * accessory ID and the user ID. The ID has the fieldname "assigned_pivot_id".
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function postCheckinBypivotid(argv) {
  const fields = argv.config.data.fields || false

  try {
    const response = await axios(argv.config)
    const data = response.data

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    console.log(data)
    log.response(`Successfully checked in the accessory.`)

    if (fields) {
      argv.config.method = 'get'
      await getByid(argv)
    }
  } catch (err) {
    log.error(err)
  }
}

module.exports = {
  deleteByid,
  getByid,
  getCheckedoutByid,
  getSearch,
  patchByid,
  postCheckinBypivotid,
  // postCheckoutByid,
  // postCreate,
}
