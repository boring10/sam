/**
 * @namespace HardwareHandlers
 */
const axios = require('axios')
const axiosRetry = require('axios-retry')

const appendToConfigUrl = require('../middleware/appendToConfigUrl')
const log = require('../libs/log')
const outputDisplay = require('../libs/outputDisplay')
const getAll = require('../libs/getAll')
const deepClone = require('../libs/deepClone')

axiosRetry(axios, {
  retries: 20,
  retryDelay: (retryCount) => {
    console.log(`Retry attempt: ${retryCount}`)
    return retryCount * 2000
  },
  retryCondition: (error) => {
    return error.response.status === 429
  },
  shouldResetTimeout: true,
})

/**
 * @memberof HardwareHandlers
 * @description
 * Delete an asset using the asset ID.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function deleteByid(argv) {
  const fields = argv.config.data.fields || false
  const adjustedArgv = deepClone(argv)
  adjustedArgv.config.method = 'get'

  let getData = await getByid(adjustedArgv, true)

  /**
   * Stop if there was no asset found.
   */
  if (!getData) return

  /**
   * If the asset has already been deleted then notify the user and do not continue
   */
  if (getData.hasOwnProperty('deleted_at.formatted')) {
    log.response(
      `The asset was deleted successfully on ${getData['deleted_at.formatted']}.`
    )

    if (fields) {
      console.log(outputDisplay(getData, argv.config.data.output, fields))
    }

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    deleteByid(argv)
  } catch (error) {
    log.error(error.response.data)
  }
}

/**
 * @memberof HardwareHandlers
 * @description
 * Delete an asset using the asset asset tag.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function deleteBytag(argv) {
  const adjustedArgv = deepClone(argv)
  adjustedArgv.config.method = 'get'
  adjustedArgv.config.url = argv.config.data.asset_tag.toString()
  adjustedArgv.config.baseURL = `${adjustedArgv.config.baseURL}/bytag`

  delete argv.config.data.asset_tag

  let getData = await getBytag(adjustedArgv, true)

  if (!getData) return

  argv.config.url = getData.id

  deleteByid(argv)
}

/**
 * @memberof HardwareHandlers
 * @description
 * Get an asset using the asset ID.
 *
 * @param {object} argv
 * @param {boolean} [captureReturn] - Returns the output instead of displaying to console.
 *
 * @returns {void | object}
 */
async function getByid(argv, captureReturn) {
  const fields = argv.config.data.fields || false

  try {
    const response = await axios(argv.config)
    const data = response.data

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    /**
     * Always returns output in a JSON format
     */
    if (captureReturn) {
      return outputDisplay(data, 'json', false)
    }

    console.log(outputDisplay(data, argv.config.data.output, fields))
  } catch (error) {
    log.error(error.response)
  }
}

/**
 * @memberof HardwareHandlers
 * @description
 * Get an asset using the asset serial number.
 *
 * @param {object} argv
 * @param {boolean} [captureReturn] - Returns the output instead of displaying to console.
 * @param {}
 *
 * @returns {void | object}
 */
async function getByserial(argv, captureReturn) {
  const fields = argv.config.data.fields || false

  try {
    const response = await axios(argv.config)
    const data = response.data

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    if (captureReturn) {
      return outputDisplay(data, 'json', false)
    }

    console.log(outputDisplay(data, argv.config.data.output, fields))
  } catch (error) {
    log.error(error.response)
  }
}

/**
 * @memberof HardwareHandlers
 * @description
 * Get an asset using the asset tag.
 *
 * @param {object} argv
 * @param {boolean} [captureReturn] - Returns the output instead of displaying to console.
 *
 * @returns {void | object}
 */
async function getBytag(argv, captureReturn) {
  const fields = argv.config.data.fields || false

  try {
    const response = await axios(argv.config)
    const data = response.data

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    if (captureReturn) {
      return outputDisplay(data, 'json', false)
    }

    console.log(outputDisplay(data, argv.config.data.output, fields))
  } catch (error) {
    log.error(error.response)
  }
}

/**
 * @memberof HardwareHandlers
 * @description
 * Get the details of the audits that are due.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function getAuditDue(argv) {
  const fields = argv.config.data.fields || false

  if (!argv.options.all) {
    try {
      const response = await axios(argv.config)
      const data = response.data

      if (data.status === 'error') {
        log.error(data.messages)

        return
      }

      console.log(outputDisplay(data, argv.config.data.output, fields))
    } catch (error) {
      log.error(error.response.data)
    }
  } else {
    getAll(argv.config, fields)
  }
}

/**
 * @memberof HardwareHandlers
 * @description
 * Get the details of the audits that are overdue.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function getAuditOverdue(argv) {
  const fields = argv.config.data.fields || false

  if (!argv.options.all) {
    try {
      const response = await axios(argv.config)
      const data = response.data

      if (data.status === 'error') {
        log.error(data.messages)

        return
      }

      console.log(outputDisplay(data, argv.config.data.output, fields))
    } catch (error) {
      log.error(error.response.data)
    }
  } else {
    getAll(argv.config, fields)
  }
}

/**
 * @memberof HardwareHandlers
 * @description
 * Get the details of the licenses checked out to an asset by the asset ID.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function getLicensesByid(argv) {
  const fields = argv.config.data.fields || false

  if (!argv.options.all) {
    try {
      const response = await axios(argv.config)
      const data = response.data

      if (data.status === 'error') {
        log.error(data.messages)

        return
      }

      console.log(outputDisplay(data, argv.config.data.output, fields))
    } catch (error) {
      log.error(error.response.data)
    }
  } else {
    getAll(argv.config, fields)
  }
}

/**
 * @memberof HardwareHandlers
 * @description
 * Get the details of the licenses checked out to an asset by the asset tag.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function getLicensesBytag(argv) {
  const adjustedArgv = deepClone(argv)
  adjustedArgv.config.method = 'get'
  adjustedArgv.config.url = argv.config.data.asset_tag.toString()
  adjustedArgv.config.baseURL = `${adjustedArgv.config.baseURL}/bytag`

  delete argv.config.data.asset_tag

  const getData = await getBytag(adjustedArgv, true)

  if (!getData) return

  argv.config.data.id = getData.id
  argv = appendToConfigUrl.idBeforeLastCommand(argv)

  getLicensesByid(argv)
}

/**
 * @memberof HardwareHandlers
 * @description
 * Get the details of the licenses checked out to an asset by the serial number.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function getLicensesByserial(argv) {
  const adjustedArgv = deepClone(argv)
  adjustedArgv.config.method = 'get'
  adjustedArgv.config.url = argv.config.data.asset_tag.toString()
  adjustedArgv.config.baseURL = `${adjustedArgv.config.baseURL}/byserial`

  delete argv.config.data.serial_num

  let getData = await getByserial(adjustedArgv, true)

  if (!getData) return

  if (!testSerialResults(adjustedArgv, getData)) {
    return
  }

  argv.config.data.id = getData[0].id
  argv = appendToConfigUrl.idBeforeLastCommand(argv)

  getLicensesByid(argv)
}

/**
 * @memberof HardwareHandlers
 * @description
 * Search for the assets taht you would like to retrieve.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function getSearch(argv) {
  const fields = argv.config.data.fields || false

  if (!argv.options.all) {
    try {
      const response = await axios(argv.config)
      const data = response.data

      if (data.status === 'error') {
        log.error(data.messages)

        return
      }

      console.log(
        outputDisplay(
          data,
          argv.config.data.output,
          fields,
          argv.config.data.export_to_file
        )
      )
    } catch (error) {
      log.error(error.response)
    }
  } else {
    getAll(argv.config, fields)
  }
}

/**
 * @memberof HardwareHandlers
 * @description
 * Patch an asset using the asset ID.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function patchByid(argv) {
  const fields = argv.config.data.fields || false

  try {
    let response = await axios(argv.config)
    let data = response.data
    const payload = data.payload

    if (data.status === 'success') {
      const assetInfo = (function () {
        let str = `(asset tag: ${payload.asset_tag}`

        if (payload.name) {
          return `${str}, name: ${payload.name})`
        } else {
          return `${str})`
        }
      })()

      log.response(
        `Successfully updated the asset with the ID "${payload.id}" ${assetInfo}`
      )

      if (fields) {
        argv.config.method = 'get'
        await getByid(argv)
      }
    } else {
      console.log(`${data.messages} (ID: ${argv.config.url})`)
    }
  } catch (err) {
    console.log(err)
  }
}

/**
 * @memberof HardwareHandlers
 * @description
 * Patch an asset using the serial. It will only continue if a unique result
 * that matches exactly is returned (not case sensative).
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function patchByserial(argv) {
  const adjustedArgv = deepClone(argv)
  adjustedArgv.config.method = 'get'
  adjustedArgv.config.url = argv.config.data.serial_num.toString()
  adjustedArgv.config.baseURL = `${adjustedArgv.config.baseURL}/byserial`

  delete argv.config.data.serial_num

  let getData = await getByserial(adjustedArgv, true)

  if (!getData) return

  if (!testSerialResults(adjustedArgv, getData)) {
    return
  }

  argv.config.data.id = getData[0].id
  argv = appendToConfigUrl.id(argv)

  patchByid(argv)
}

/**
 * @memberof HardwareHandlers
 * @description
 * Patch an asset using the asset tag.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function patchBytag(argv) {
  const adjustedArgv = deepClone(argv)
  adjustedArgv.config.method = 'get'
  adjustedArgv.config.url = argv.config.data.asset_tag.toString()
  adjustedArgv.config.baseURL = `${adjustedArgv.config.baseURL}/bytag`

  delete argv.config.data.asset_tag

  const getData = await getBytag(adjustedArgv, true)

  if (!getData) return

  argv.config.data.id = getData.id
  argv = appendToConfigUrl.id(argv)

  patchByid(argv)
}

/**
 * @memberof HardwareHandlers
 * @description
 * Checkin an asset by the asset ID.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function postCheckinByid(argv) {
  argv = appendToConfigUrl.idBeforeLastCommand(argv)
  const fields = argv.config.data.fields || false

  try {
    const response = await axios(argv.config)
    const data = response.data
    const payload = data.payload

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully checked in the asset with the asset tag "${payload.asset}".`
    )

    if (fields) {
      let baseURL = argv.config.baseURL
      argv.config.method = 'get'

      // Remove the last "/" and everything after it.
      argv.config.baseURL = baseURL.substr(0, baseURL.lastIndexOf('/'))
      await getByid(argv)
    }
  } catch (err) {
    log.error(err)
  }
}

/**
 * @memberof HardwareHandlers
 * @description
 * Checkin an asset by the asset tag.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function postCheckinBytag(argv) {
  const adjustedArgv = deepClone(argv)
  adjustedArgv.config.method = 'get'
  adjustedArgv.config.url = argv.config.data.asset_tag.toString()
  adjustedArgv.config.baseURL = `${adjustedArgv.config.baseURL}/bytag`

  delete argv.config.data.asset_tag

  const getData = await getBytag(adjustedArgv, true)

  if (!getData) return

  argv.config.data.id = getData.id

  postCheckinByid(argv)
}

/**
 * @memberof HardwareHandlers
 * @description
 * Checkout an asset by the asset ID.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function postCheckoutByid(argv) {
  argv = appendToConfigUrl.idBeforeLastCommand(argv)
  const fields = argv.config.data.fields || false

  try {
    const response = await axios(argv.config)
    const data = response.data
    const payload = data.payload

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully checked out the asset with the asset tag "${payload.asset}".`
    )

    if (fields) {
      let baseURL = argv.config.baseURL
      argv.config.method = 'get'

      // Remove the last "/" and everything after it.
      argv.config.baseURL = baseURL.substr(0, baseURL.lastIndexOf('/'))
      await getByid(argv)
    }
  } catch (err) {
    log.error(err)
  }
}

/**
 * @memberof HardwareHandlers
 * @description
 * Checkout an asset by the asset tag.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function postCheckoutBytag(argv) {
  const adjustedArgv = deepClone(argv)
  adjustedArgv.config.method = 'get'
  adjustedArgv.config.url = argv.config.data.asset_tag.toString()
  adjustedArgv.config.baseURL = `${adjustedArgv.config.baseURL}/bytag`

  delete argv.config.data.asset_tag

  const getData = await getBytag(adjustedArgv, true)

  if (!getData) return

  argv.config.data.id = getData.id

  postCheckinByid(argv)
}

/**
 * @memberof HardwareHandlers
 * @description
 * Create a new asset.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function postCreate(argv) {
  const fields = argv.config.data.fields || false

  let auto_asset_tag =
    argv.activeProfile[Object.keys(argv.activeProfile)[0]].auto_asset_tag

  if (!auto_asset_tag && !argv.options.asset_tag) {
    log.error(
      `According to the active profile the asset tag option is required.`
    )

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data
    const payload = data.payload

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully created an asset with the ID "${payload.id}" and asset tag "${payload.asset_tag}".`
    )

    if (fields) {
      const adjustedArgv = deepClone(argv)
      adjustedArgv.config.method = 'get'
      adjustedArgv.config.url = `${payload.id}`
      adjustedArgv.config.data = {}

      const getData = await getByid(adjustedArgv, true)

      console.log(outputDisplay(getData, argv.config.data.output, fields))
    }
  } catch (err) {
    log.error(err)
  }
}

/**
 * @memberof HardwareHandlers
 * @private
 * @description
 * Check if for no results or multiple results for a single serial number have
 * been returned.
 *
 * @param {object} argv
 * @param {object} data
 *
 * @returns {boolean}
 */
function testSerialResults(argv, data) {
  if (!Array.isArray(data) || data.length === 0) {
    log.error(
      `No results were found for the serial number "${argv.config.url}".`
    )

    return 0
  }

  if (!data.length > 1) {
    log.error(
      `More than one result was returned for the serial number "${argv.config.url}".`
    )

    return 0
  }

  if (!data[0].serial.toLowerCase() === argv.config.url.toLowerCase()) {
    log.error(`The serial number "${argv.config.url}" does not match exactly.`)

    return 0
  }

  return 1
}

module.exports = {
  deleteByid,
  deleteBytag,
  getByid,
  getByserial,
  getBytag,
  getAuditDue,
  getAuditOverdue,
  getLicensesByid,
  getLicensesByserial,
  getLicensesBytag,
  getSearch,
  patchByid,
  patchByserial,
  patchBytag,
  postCheckinByid,
  postCheckinBytag,
  postCheckoutByid,
  postCheckoutBytag,
  postCreate,
}
