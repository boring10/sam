/**
 * @namespace HardwareHandlers
 */
const axios = require('axios')

const log = require('../libs/log')
const outputDisplay = require('../libs/outputDisplay')
const getAll = require('../libs/getAll')

/**
 * @memberof HardwareHandlers
 * @description
 * Search for the assets taht you would like to retrieve.
 *
 * @param {object} argv
 *
 * @returns {void}
 */
async function getActivity(argv) {
  const fields = argv.config.data.fields || false

  if (!argv.options.all) {
    try {
      const response = await axios(argv.config)
      const data = response.data

      if (data.status === 'error') {
        log.error(data.messages)

        return
      }

      console.log(data.rows)
      // console.log(
      //   outputDisplay(
      //     data,
      //     argv.config.data.output,
      //     fields,
      //     argv.config.data.export_to_file
      //   )
      // )
    } catch (error) {
      log.error(error.response)
    }
  } else {
    getAll(argv.config, fields)
  }
}

module.exports = {
  getActivity,
}
