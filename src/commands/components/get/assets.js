exports.command = 'assets <command>'
exports.description = 'Get the assets a component has been checked out to.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('assets')
}
