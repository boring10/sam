const commandSection = require('../../libs/commandSection')
const method = 'post'

exports.command = method
exports.description = `HTTP ${method.toUpperCase()} method.`
exports.builder = function (yargs) {
  const section = commandSection(yargs.getContext().commands)

  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir(`../${section}/${method}`)
}
