const CommandOptions = require('../../../libs/commandOptions')
const log = require('../../../libs/log')
const config = require(require('../../../libs/files').fileLocations().config)
const writeFile = require('../../../libs/writeFile')

exports.command = 'checkforupdates'
exports.description =
  'Enable or disable the check for updates. When enabled, this is performed once per day.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('enabled', commandOptions.option('enabled'))
    .strictOptions((enabled = true))
}
exports.handler = function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  config.check_for_updates.enabled = argv.options.enabled

  writeFile.config(config)

  log.response(
    `The configuration setting "check_for_updates" has been set to ${argv.options.enabled}`
  )
}
