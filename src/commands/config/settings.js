exports.command = 'settings <command>'
exports.description = 'Manage the settings for SAM.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./settings')
}
