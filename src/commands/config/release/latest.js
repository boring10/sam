const axios = require('axios')
const log = require('../../../libs/log')

exports.command = 'latest'
exports.description = 'Release notes for the latest release.'
exports.builder = function (yargs) {
  return yargs.strictOptions((enabled = true))
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    const data = await axios({
      method: 'GET',
      baseURL: `${process.env.GITLAB_PROJECT_API}/releases`,
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
      },
    }).then((response) => {
      return response.data[0]
    })

    log.response(data.description.trim())
  } catch (error) {
    if (error.response) {
      log.error(error.response.message)
    } else {
      log.error(error)
    }
  }
}
