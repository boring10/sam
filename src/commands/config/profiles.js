exports.command = 'profiles <command>'
exports.description = 'Manage your API profiles for Snipe-IT.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./profiles')
}
