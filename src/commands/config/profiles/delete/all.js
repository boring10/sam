const CommandOptions = require('../../../../libs/commandOptions')
const log = require('../../../../libs/log')
const writeFile = require('../../../../libs/writeFile')

exports.command = 'all'
exports.description = 'Delete all of the profiles.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs.option('confirm', commandOptions.option('confirm'))
}
exports.handler = function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  let confirm = argv.options.confirm
  if (!confirm) {
    let prompt = require('prompt-sync')({ sigint: true })
    let response

    do {
      response = prompt('Delete all profiles? (Y/n): ').toLowerCase()
    } while (response !== 'y' && response !== 'n')

    if (response === 'y') {
      confirm = true
    }
  }

  if (confirm) {
    writeFile.profiles({})
    log.response('All profiles have been deleted.')
  } else {
    log.response('No profiles have been deleted.')
  }
}
