const CommandOptions = require('../../../libs/commandOptions')
const formatProfileName = require('../../../libs/formatProfileName')
const log = require('../../../libs/log')
const profileMatch = require('../../../middleware/profileMatch')
const writeFile = require('../../../libs/writeFile')

exports.command = 'create'
exports.description = 'Creae a new profile to access your instance of Snipe-IT.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('name', commandOptions.option('name'))
    .option('active', commandOptions.option('active'))
    .option('api_key', commandOptions.option('api_key'))
    .option('api_throttle', commandOptions.option('api_throttle'))
    .option('api_version', commandOptions.option('api_version'))
    .option('auto_asset_tag', commandOptions.option('auto_asset_tag'))
    .option('url', commandOptions.option('url'))
    .strict((enabled = true))
    .middleware([profileMatch])
}
exports.handler = function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  let name = formatProfileName(argv.options.name)
  delete argv.options.name

  const profiles = require(require('../../../libs/files').fileLocations()
    .profiles)

  if (argv.options.active) {
    Object.keys(profiles).forEach((key) => {
      profiles[key].active = false
    })
  }

  profiles[name] = argv.options

  writeFile.profiles(profiles)

  if (argv.options.active) {
    log.response(
      `The profile "${name}" has been created. This profile is now the currently active profile`
    )
  } else {
    log.response(`The profile "${name}" has been created.`)
  }
}
