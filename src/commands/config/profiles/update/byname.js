const CommandOptions = require('../../../../libs/commandOptions')
const formatProfileName = require('../../../../libs/formatProfileName')
const log = require('../../../../libs/log')
const profileMatch = require('../../../../middleware/profileMatch')
const profiles = require(require('../../../../libs/files').fileLocations()
  .profiles)
const writeFile = require('../../../../libs/writeFile')

exports.command = 'byname <name>'
exports.description = 'Update a Snipe-IT profile.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('active', commandOptions.option('active'))
    .option('api_key', commandOptions.option('api_key'))
    .option('api_throttle', commandOptions.option('api_throttle'))
    .option('api_version', commandOptions.option('api_version'))
    .option('auto_asset_tag', commandOptions.option('auto_asset_tag'))
    .option('new_name', commandOptions.option('new_name'))
    .option('url', commandOptions.option('url'))
    .strict((enabled = true))
    .middleware([profileMatch])
}
exports.handler = function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  let name = formatProfileName(argv.options.name)
  delete argv.options.name

  if (argv.options.active) {
    Object.keys(profiles).forEach((key) => {
      if (key === name) {
        profiles[key].active = true
      } else {
        profiles[key].active = false
      }
    })
  }

  if (argv.options.new_name) {
    let new_name = formatProfileName(argv.options.new_name)
    delete argv.options.new_name

    profiles[new_name] = profiles[name]
    delete profiles[name]

    log.response(
      `The profile name has been updated from "${name}" to "${new_name}".`
    )

    name = new_name
  }

  Object.keys(argv.options).forEach((key) => {
    profiles[name][key] = argv.options[key]
  })

  writeFile.profiles(profiles)

  log.response(`The profile "${name}" has been updated.`)
}
