exports.command = 'update <command>'
exports.description = 'Update existing profiles.'
exports.builder = function (yargs) {
  return yargs.demandCommand(1).commandDir('./update')
}
