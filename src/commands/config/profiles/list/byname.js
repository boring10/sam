const profiles = require(require('../../../../libs/files').fileLocations()
  .profiles)
const CommandOptions = require('../../../../libs/commandOptions')
const formatProfileName = require('../../../../libs/formatProfileName')
const profileMatch = require('../../../../middleware/profileMatch')
const log = require('../../../../libs/log')

exports.command = 'byname <name>'
exports.description = 'List the profile that matches the name.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('output', commandOptions.option('output'))
    .strict((enabled = true))
    .middleware([profileMatch])
}
exports.handler = function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  const name = formatProfileName(argv.options.name)
  const profile = argv.profileMatch[name]

  if (argv.options.output === 'json') {
    console.log(profile)

    return
  }

  Object.keys(profile).forEach((key) => {
    log.response(`${key}: ${profile[key]}`)
  })
}
