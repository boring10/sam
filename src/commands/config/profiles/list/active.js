const CommandOptions = require('../../../../libs/commandOptions')
const log = require('../../../../libs/log')

exports.command = 'active'
exports.description = 'List the active profile.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('output', commandOptions.option('output'))
    .strictOptions((enabled = true))
}
exports.handler = function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  const profiles = argv.activeProfile

  if (argv.options.output === 'json') {
    console.log(profiles)

    return
  }

  Object.keys(profiles).forEach((profile) => {
    log.response(profile)
    Object.keys(profiles[profile]).forEach((key) => {
      console.log(`  ${key}: ${profiles[profile][key]}`)
    })
  })
}
