const CommandOptions = require('../../../../libs/commandOptions')
const profiles = require(require('../../../../libs/files').fileLocations()
  .profiles)
const log = require('../../../../libs/log')

exports.command = 'all'
exports.description = 'List all profiles.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('output', commandOptions.option('output'))
    .strictOptions((enabled = true))
}
exports.handler = function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  if (argv.options.output === 'json') {
    console.log(profiles)

    return
  }

  Object.keys(profiles).forEach((profile) => {
    log.response(profile)

    Object.keys(profiles[profile]).forEach((key) => {
      if (key === 'api_key') {
        console.log(
          `  ${key}: ${profiles[profile][key].substr(0, 10)}[truncated]`
        )
      } else {
        console.log(`  ${key}: ${profiles[profile][key]}`)
      }
    })
    console.log('')
  })
}
