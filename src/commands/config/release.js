exports.command = 'release <command>'
exports.description = 'Release information.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./release')
}
