exports.command = 'hardware <method>'
exports.description = 'Manage your assets.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods')
    .commandDir('hardware')
}
