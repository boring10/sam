exports.command = 'users <method>'
exports.description = 'Manage your users.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods')
    .commandDir('users')
}
