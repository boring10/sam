exports.command = 'maintenances <method>'
exports.description = 'Manage your asset maintenances.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods', {
      // exclude: function (path) {
      //   if (/patch\.js$/.test(path)) {
      //     return true
      //   } else {
      //     return false
      //   }
      // },
    })
    .commandDir('maintenances')
}
