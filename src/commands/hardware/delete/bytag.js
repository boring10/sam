const CommandOptions = require('../../../libs/commandOptions')
const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const hardwareHandlers = require('../../../yargsHandlers/hardwareHandlers')

exports.command = 'bytag <asset_tag>'
exports.description = 'Delete a specific asset by the asset tag.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('fields', commandOptions.option('fields'))
    .option('output', commandOptions.option('output'))
    .strict((enabled = true))
    .middleware([generateConfig, appendToConfigUrl.id])
}
exports.handler = async function (argv) {
  hardwareHandlers.deleteBytag(argv)
}
