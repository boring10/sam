const CommandOptions = require('../../../../libs/commandOptions')
const generateConfig = require('../../../../middleware/generateConfig')
const hardwareHandlers = require('../../../../yargsHandlers/hardwareHandlers')

exports.command = 'bytag <asset_tag>'
exports.description = 'Checkout an asset by the asset tag.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('assigned_asset', commandOptions.option('assigned_asset'))
    .option('assigned_location', commandOptions.option('assigned_location'))
    .option('assigned_user', commandOptions.option('assigned_user'))
    .option('checkout_at', commandOptions.option('checkout_at'))
    .option('checkout_to_type', commandOptions.option('checkout_to_type'))
    .option('expected_checkin', commandOptions.option('expected_checkin'))
    .option('fields', commandOptions.option('fields'))
    .option('name', commandOptions.option('name'))
    .option('notes', commandOptions.option('notes'))
    .strict((enabled = true))
    .middleware([generateConfig])
}
exports.handler = async function (argv) {
  hardwareHandlers.postCheckoutBytag(argv)
}
