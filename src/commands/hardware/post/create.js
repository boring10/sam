const CommandOptions = require('../../../libs/commandOptions')
const generateConfig = require('../../../middleware/generateConfig')
const hardwareHandlers = require('../../../yargsHandlers/hardwareHandlers')

exports.command = 'create'
exports.description = 'Create a new asset.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('<custom_fieldname>', commandOptions.option('custom_fieldname'))
    .option('asset_tag', commandOptions.option('asset_tag'))
    .option('company_id', commandOptions.option('company_id'))
    .option('fields', commandOptions.option('fields'))
    .option('model_id', commandOptions.option('model_id'))
    .option('name', commandOptions.option('name'))
    .option('notes', commandOptions.option('notes'))
    .option('order_number', commandOptions.option('order_number'))
    .option('output', commandOptions.option('output'))
    .option('purchase_cost', commandOptions.option('purchase_cost'))
    .option('purchase_date', commandOptions.option('purchase_date'))
    .option('requestable', commandOptions.option('requestable'))
    .option('rtd_location_id', commandOptions.option('rtd_location_id'))
    .option('serial', commandOptions.option('serial'))
    .option('status_id', commandOptions.option('status_id'))
    .option('supplier_id', commandOptions.option('supplier_id'))
    .option('warranty_months', commandOptions.option('warranty_months'))
    .middleware([generateConfig])
}
exports.handler = async function (argv) {
  hardwareHandlers.postCreate(argv)
}
