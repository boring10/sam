exports.command = 'checkout <command>'
exports.description = 'Check an asset out.'
exports.builder = function (yargs) {
  return yargs
    .strict((enabled = true))
    .demandCommand(1)
    .commandDir('./checkout')
}
