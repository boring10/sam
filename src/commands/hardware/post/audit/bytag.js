const axios = require('axios')

const CommandOptions = require('../../../../libs/commandOptions')
const appendToConfigUrl = require('../../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../../middleware/generateConfig')
const log = require('../../../../libs/log')

exports.command = 'bytag <asset_tag>'
exports.description = 'Audit an asset by the asset ID.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('location_id', commandOptions.option('location_id'))
    .option('next_audit_date', commandOptions.option('next_audit_date'))
    .option('notes', commandOptions.option('notes'))
    .option('update_location', commandOptions.option('update_location'))
    .strict((enabled = true))
    .middleware([generateConfig, appendToConfigUrl.excludeLastCommand])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data
    const payload = data.payload

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully audited the asset with the asset tag "${payload.asset_tag}" (next audit: ${payload.next_audit_date.formatted}).`
    )
  } catch (err) {
    log.error(err)
  }
}
