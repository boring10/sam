exports.command = 'checkin <command>'
exports.description = 'Check an asset in.'
exports.builder = function (yargs) {
  return yargs
    .strict((enabled = true))
    .demandCommand(1)
    .commandDir('./checkin')
}
