const CommandOptions = require('../../../libs/commandOptions')
const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const hardwareHandlers = require('../../../yargsHandlers/hardwareHandlers')

exports.command = 'byserial <serial>'
exports.description = 'Get the details of a specific asset by the serial.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('fields', commandOptions.option('fields'))
    .option('output', commandOptions.option('output'))
    .strict((enabled = true))
    .middleware([generateConfig, appendToConfigUrl.serial])
}
exports.handler = async function (argv) {
  hardwareHandlers.getByserial(argv)
}
