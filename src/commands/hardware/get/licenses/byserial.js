const CommandOptions = require('../../../../libs/commandOptions')
const generateConfig = require('../../../../middleware/generateConfig')
const hardwareHandlers = require('../../../../yargsHandlers/hardwareHandlers')

exports.command = 'byserial <serial_num>'
exports.description =
  'Get the details of the licenses checked out to an asset by the serial number.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('all', commandOptions.option('all'))
    .option('export_to_file', commandOptions.option('export_to_file'))
    .option('fields', commandOptions.option('fields'))
    .option('limit', commandOptions.option('limit'))
    .option('offset', commandOptions.option('offset'))
    .option('order', commandOptions.option('order'))
    .option('output', commandOptions.option('output'))
    .option('sort', commandOptions.option('sort'))
    .strict((enabled = true))
    .middleware([generateConfig])
}
exports.handler = async function (argv) {
  hardwareHandlers.getLicensesByserial(argv)
}
