exports.command = 'licenses <command>'
exports.description = 'Get the licenses checked out to an asset.'
exports.builder = function (yargs) {
  return yargs
    .strict((enabled = true))
    .demandCommand(1)
    .commandDir('./licenses')
}
