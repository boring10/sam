exports.command = 'audit <command>'
exports.description =
  'This API call uses the "Audit Threshold" in "Admin Settings > Notifications" to determine whether an asset is due for auditing.'
exports.builder = function (yargs) {
  return yargs
    .strict((enabled = true))
    .demandCommand(1)
    .commandDir('./audit')
}
