const CommandOptions = require('../../../libs/commandOptions')
const generateConfig = require('../../../middleware/generateConfig')
const hardwareHandlers = require('../../../yargsHandlers/hardwareHandlers')

exports.command = 'search [search]'
exports.description = 'Search for the assets that you would like to retrieve.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return (
    yargs
      .option('all', commandOptions.option('all'))
      .option('<custom_fieldname>', commandOptions.option('custom_fieldname'))
      .option('category_id', commandOptions.option('category_id'))
      .option('company_id', commandOptions.option('company_id'))
      .option('export_to_file', commandOptions.option('export_to_file'))
      .option('fields', commandOptions.option('fields'))
      .option('limit', commandOptions.option('limit'))
      .option('location_id', commandOptions.option('location_id'))
      .option('manufacturer_id', commandOptions.option('manufacturer_id'))
      .option('model_id', commandOptions.option('model_id'))
      .option('offset', commandOptions.option('offset'))
      .option('order', commandOptions.option('order'))
      .option('output', commandOptions.option('output'))
      .option('sort', commandOptions.option('sort'))
      .option('status', commandOptions.option('status'))
      // .strict((enabled = true))
      .middleware([generateConfig])
  )
}
exports.handler = async function (argv) {
  hardwareHandlers.getSearch(argv)
}
