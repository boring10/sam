exports.command = 'depreciations <method>'
exports.description = 'Manage your depreciations.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods')
    .commandDir('depreciations')
}
