const axios = require('axios')

const CommandOptions = require('../../../libs/commandOptions')
const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')

exports.command = 'byid <id>'
exports.description =
  'Partially update a category, passing only the fields you want to modify.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('category_type', commandOptions.option('category_type'))
    .option('checkin_email', commandOptions.option('checkin_email'))
    .option('eula_text', commandOptions.option('eula_text'))
    .option('name', commandOptions.option('name'))
    .option('require_acceptance', commandOptions.option('require_acceptance'))
    .option('use_default_eula', commandOptions.option('use_default_eula'))
    .coerce('category_type', (opt) => opt.toLowerCase())
    .middleware([generateConfig, appendToConfigUrl.id])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    let response = await axios(argv.config)
    let data = response.data
    const payload = data.payload

    log.response(
      `Successfully updated the category with the ID "${payload.id}" named "${payload.name}".`
    )
  } catch (err) {
    console.log(err)
  }
}
