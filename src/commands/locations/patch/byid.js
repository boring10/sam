const axios = require('axios')

const CommandOptions = require('../../../libs/commandOptions')
const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')

exports.command = 'byid <id>'
exports.description = 'Update the location by the ID.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return (
    yargs
      .option('address', commandOptions.option('address'))
      .option('address2', commandOptions.option('address2'))
      .option('country', commandOptions.option('country'))
      .option('currency', commandOptions.option('currency'))
      // .option('ldap_ou', commandOptions.option('ldap_ou'))
      .option('manager_id', commandOptions.option('manager_id'))
      .option('name', commandOptions.option('name'))
      .option('parent_id', commandOptions.option('parent_id'))
      .option('state', commandOptions.option('state'))
      .option('zip', commandOptions.option('zip'))
      .coerce('country', (opt) => opt.toUpperCase())
      .middleware([generateConfig, appendToConfigUrl.id])
  )
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  if (argv.config.data.country) {
    argv.config.data.country = argv.config.data.country.toUpperCase()
  }

  try {
    const response = await axios(argv.config)
    const data = response.data
    const payload = data.payload

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully update the location with the ID "${payload.id}" named "${payload.name}".`
    )
  } catch (err) {
    log.error(err)
  }
}
