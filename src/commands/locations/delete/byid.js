const axios = require('axios')

const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')

exports.command = 'byid <id>'
exports.description = 'Delete the location by the ID.'
exports.builder = function (yargs) {
  return yargs.middleware([generateConfig, appendToConfigUrl.id])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    /**
     * The response does not return anything about the location that was deleted
     * so the ID (url in this case) that was entered is used instead of the payload.
     */
    log.response(
      `Successfully deleted the location with the ID "${argv.config.url}".`
    )
  } catch (err) {
    log.error(err)
  }
}
