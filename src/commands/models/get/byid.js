const axios = require('axios')

const CommandOptions = require('../../../libs/commandOptions')
const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')
const outputDisplay = require('../../../libs/outputDisplay')

exports.command = 'byid <id>'
exports.description = 'Get the details of a specific models by the ID.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('fields', commandOptions.option('fields'))
    .option('output', commandOptions.option('output'))
    .strict((enabled = true))
    .middleware([generateConfig, appendToConfigUrl.id])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  const fields = argv.config.data.fields || false

  try {
    const response = await axios(argv.config)
    const data = response.data

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    console.log(outputDisplay(data, argv.config.data.output, fields))
  } catch (error) {
    log.error(error.response)
  }
}
