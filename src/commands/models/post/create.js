const axios = require('axios')

const CommandOptions = require('../../../libs/commandOptions')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')

exports.command = 'create'
exports.description = 'Create a new model.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return (
    yargs
      .options('category_id', commandOptions.option('category_id'))
      .options('eol', commandOptions.option('eol'))
      .options('fieldset_id', commandOptions.option('fieldset_id'))
      .options('manufacturer_id', commandOptions.option('manufacturer_id'))
      .options('model_number', commandOptions.option('model_number'))
      .options('name', commandOptions.option('name'))
      .options('notes', commandOptions.option('notes'))
      // .options('requestable', commandOptions.option('requestable'))
      .middleware([generateConfig])
  )
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data
    const payload = data.payload

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully create a new model with the ID "${payload.id}" with the name "${payload.name}".`
    )
  } catch (err) {
    log.error(err)
  }
}
