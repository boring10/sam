const axios = require('axios')

const CommandOptions = require('../../../libs/commandOptions')
const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')

exports.command = 'byid <id>'
exports.description = 'Update an existing user.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return (
    yargs
      .options('activated', commandOptions.option('activated'))
      .options('address', commandOptions.option('address'))
      .options('city', commandOptions.option('city'))
      .options('company_id', commandOptions.option('company_id'))
      .options('country', commandOptions.option('country'))
      .options('department_id', commandOptions.option('department_id'))
      .options('email', commandOptions.option('email'))
      .options('email_user', commandOptions.option('email_user'))
      .options('employee_num', commandOptions.option('employee_num'))
      .options('first_name', commandOptions.option('first_name'))
      // .options('groups', commandOptions.option('groups'))
      .options('jobtitle', commandOptions.option('jobtitle'))
      .options('last_name', commandOptions.option('last_name'))
      .options('location_id', commandOptions.option('location_id'))
      .options('manager_id', commandOptions.option('manager_id'))
      .options('notes', commandOptions.option('notes'))
      .options('password', commandOptions.option('password'))
      .options(
        'password_confirmation',
        commandOptions.option('password_confirmation')
      )
      .options('permissions', commandOptions.option('permissions'))
      .options('phone', commandOptions.option('phone'))
      .options('state', commandOptions.option('state'))
      .options(
        'two_factor_enrolled',
        commandOptions.option('two_factor_enrolled')
      )
      .options('two_factor_optin', commandOptions.option('two_factor_optin'))
      .options('username', commandOptions.option('username'))
      // .options('website', commandOptions.option('website')) // 2021-04-18: Does not appear to be available at this time.
      .options('zip', commandOptions.option('zip'))
      .middleware([generateConfig, appendToConfigUrl.id])
  )
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data
    const payload = data.payload

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully updated the user with the ID "${
        payload.id
      }" with the name "${payload.name.trim()}" and username "${
        payload.username
      }".`
    )
  } catch (err) {
    log.error(err)
  }
}
