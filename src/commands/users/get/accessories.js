exports.command = 'accessories [command]'
exports.description = 'Get the accessories checked out to a user.'
exports.builder = function (yargs) {
  return yargs
    .strict((enabled = true))
    .demandCommand(1)
    .commandDir('./accessories')
}
