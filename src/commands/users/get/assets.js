exports.command = 'assets <command>'
exports.description = 'Get the assets checked out to the user.'
exports.builder = function (yargs) {
  return yargs
    .strict((enabled = true))
    .demandCommand(1)
    .commandDir('./assets')
}
