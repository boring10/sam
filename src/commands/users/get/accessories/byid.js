const axios = require('axios')

const CommandOptions = require('../../../../libs/commandOptions')
const appendToConfigUrl = require('../../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../../middleware/generateConfig')
const log = require('../../../../libs/log')
const outputDisplay = require('../../../../libs/outputDisplay')
const getAll = require('../../../../libs/getAll')

exports.command = 'byid <id>'
exports.description =
  'Get the details of the accessories checked out to a user by the user ID.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('all', commandOptions.option('all'))
    .option('export_to_file', commandOptions.option('export_to_file'))
    .option('fields', commandOptions.option('fields'))
    .option('limit', commandOptions.option('limit'))
    .option('offset', commandOptions.option('offset'))
    .option('order', commandOptions.option('order'))
    .option('output', commandOptions.option('output'))
    .option('sort', commandOptions.option('sort'))
    .strict((enabled = true))
    .middleware([generateConfig, appendToConfigUrl.idBeforeLastCommand])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  const fields = argv.config.data.fields || false

  if (!argv.options.all) {
    try {
      const response = await axios(argv.config)
      const data = response.data

      if (data.status === 'error') {
        log.error(data.messages)

        return
      }

      console.log(outputDisplay(data, argv.config.data.output, fields))
    } catch (error) {
      log.error(error.response.data)
    }
  } else {
    getAll(argv.config, fields)
  }
}
