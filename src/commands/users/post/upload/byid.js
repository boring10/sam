const axios = require('axios')
const FormData = require('form-data')
const fs = require('fs/promises')

const CommandOptions = require('../../../../libs/commandOptions')
const appendToConfigUrl = require('../../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../../middleware/generateConfig')
const log = require('../../../../libs/log')

exports.command = 'byid <id>'
exports.description = 'Upload a file to a user.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('file', {
      description: 'File to upload',
      type: 'string',
    })
    .middleware([generateConfig, appendToConfigUrl.idBeforeLastCommand])
}
exports.handler = async function (argv) {
  const file = await fs.readFile(argv.config.data.file)

  const form = new FormData()
  form.append('file', file, 'test.jpg')
  argv.config.headers['content-type'] = form.getHeaders()['content-type']
  delete argv.config.headers.accept

  const response = await axios(argv.config)
  const data = response.data

  console.log(data)
}
