exports.command = 'departments <method>'
exports.description = 'Manage your departments.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods')
    .commandDir('departments')
}
