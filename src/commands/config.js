exports.command = 'config <command>'
exports.description = 'Manage the application configuration settings.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./config')
}
