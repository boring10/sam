const axios = require('axios')

const CommandOptions = require('../../../libs/commandOptions')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')

exports.command = 'create'
exports.description = 'Create a new maintenance.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('asset_id', commandOptions.option('asset_id'))
    .option(
      'asset_maintenance_type',
      commandOptions.option('asset_maintenance_type')
    )
    .option('completion_date', commandOptions.option('completion_date'))
    .option('cost', commandOptions.option('cost'))
    .option('is_warranty', commandOptions.option('is_warranty'))
    .option('notes', commandOptions.option('notes'))
    .option('start_date', commandOptions.option('start_date'))
    .option('supplier_id', commandOptions.option('supplier_id'))
    .option('title', commandOptions.option('title'))
    .middleware([generateConfig])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data
    const payload = data.payload

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully created the maintenance "${payload.title}" with the ID "${payload.id}" for the asset with the ID of "${payload.asset_id}".`
    )
  } catch (err) {
    log.error(err)
  }
}
