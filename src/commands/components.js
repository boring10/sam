exports.command = 'components <method>'
exports.description = 'Manage your components.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods')
    .commandDir('components')
}
