exports.command = 'checkin <command>'
exports.description = 'Checkin a consumable from a user.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('checkin')
}
