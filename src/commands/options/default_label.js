/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for default_label.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const default_label = (commands) => {
  const { section, method } = commands

  if (section === 'statuslabels') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    statuslabels: {
      patch: {
        byid: {
          alias: alias('default_label'),
          description:
            'Update whether it should be bubbled up to the top of the list of available statuses.',
          type: 'boolean',
        },
      },
      post: {
        create: {
          alias: alias('default_label'),
          description:
            'Determine whether it should be bubbled up to the top of the list of available statuses.',
          default: false,
          type: 'boolean',
        },
      },
    },
  }
}

module.exports = default_label
