/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for show_in_nav.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const show_in_nav = (commands) => {
  const { section, method } = commands

  if (section === 'statuslabels') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    statuslabels: {
      patch: {
        byid: {
          alias: alias('show_in_nav'),
          description:
            'Update whether the status label should show in the left-side nav of the web GUI.',
          type: 'boolean',
        },
      },
      post: {
        create: {
          alias: alias('show_in_nav'),
          description:
            'Determine whether the status label should show in the left-side nav of the web GUI.',
          default: true,
          type: 'boolean',
        },
      },
    },
  }
}

module.exports = show_in_nav
