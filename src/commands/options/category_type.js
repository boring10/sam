/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for category_type.
 *
 * @param {object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const category_type = (commands) => {
  const { section, method } = commands

  if (section === 'categories') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {object}
 */
function optionsObj() {
  let choices = ['asset', 'accessory', 'consumable', 'component', 'license']

  return {
    categories: {
      patch: {
        byid: {
          alias: alias('category_type'),
          description: 'Type of category.',
          choices,
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('category_type'),
          description: 'Type of category.',
          required: true,
          choices,
          type: 'string',
        },
      },
    },
  }
}

module.exports = category_type
