/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for api_version.
 *
 * @param {object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const api_version = (commands) => {
  const { section, method } = commands

  if (section === 'config') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {object}
 */
function optionsObj() {
  let choices = ['v1']

  return {
    config: {
      profiles: {
        create: {
          alias: alias('api_version'),
          description: 'The API version for this profile to use.',
          default: 'v1',
          choices,
          type: 'string',
        },
        update: {
          alias: alias('api_version'),
          description: 'The API version for this profile to use.',
          choices,
          type: 'string',
        },
      },
    },
  }
}

module.exports = api_version
