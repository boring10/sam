/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for active.
 *
 * @param {object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const active = (commands) => {
  const { section, method } = commands

  if (section === 'config') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {object}
 */
function optionsObj() {
  return {
    config: {
      profiles: {
        create: {
          alias: alias('active'),
          description:
            'States whether to make this profile the active profile.',
          default: false,
          type: 'boolean',
        },
        update: {
          alias: alias('active'),
          description:
            'Update the active status of the profile. If enabled, all other profiles will have their active status set to false.',
          type: 'boolean',
        },
      },
    },
  }
}

module.exports = active
