/**
 * @namespace OptionsTest
 */
const booleanFalse = ['boolean', Boolean, false]
const booleanTrue = ['boolean', Boolean, true]
const description = ['string', String]
const typeArray = ['array', Array]
const typeBoolean = ['string', String, 'boolean']
const typeNum = ['string', String, 'number']
const typeStr = ['string', String, 'string']
const typeFunc = ['string', Function]
const defaultStr = function (str) {
  return ['string', String, str]
}
const defaultNumber = function (number) {
  return ['number', Number, number]
}

/**
 * @private
 * @typedef Object
 * @description Object that will be tested upon for each command option.
 *
 * The key of "_depth" must be used for each nested object beginning with the
 * method. This key may only contain a number of "1" or "2" to indicate whether
 * the object is nested any further or not.
 */
const obj = {
  /* Action Type */
  action_type: {
    reports: {
      get: {
        activity: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Activated */
  activated: {
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          default: booleanFalse,
          type: typeBoolean,
        },
      },
    },
  },

  /* Active */
  active: {
    config: {
      profiles: {
        _depth: 2,
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          default: booleanFalse,
          type: typeBoolean,
        },
        update: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
  },

  /* Address */
  address: {
    locations: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Address2 */
  address2: {
    locations: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* All */
  all: {
    accessories: {
      get: {
        checkedout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeBoolean,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
    categories: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
    companies: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
    components: {
      get: {
        assets: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeBoolean,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
    consumables: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
    departments: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
    hardware: {
      get: {
        audit: {
          _depth: 2,
          due: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeBoolean,
          },
          overdue: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeBoolean,
          },
        },
        licenses: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeBoolean,
          },
          byserial: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeBoolean,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeBoolean,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
    licenses: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
    locations: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
    maintenances: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
    manufacturers: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
    models: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
    reports: {
      get: {
        activity: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
    statuslabels: {
      get: {
        assetlist: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeBoolean,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
    users: {
      get: {
        accessories: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeBoolean,
          },
        },
        assets: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeBoolean,
          },
        },
        licenses: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeBoolean,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
  },

  /* API Key */
  api_key: {
    config: {
      profiles: {
        _depth: 2,
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          demandOption: booleanTrue,
          type: typeStr,
        },
        update: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* API Throttle */
  api_throttle: {
    config: {
      profiles: {
        _depth: 2,
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultNumber(120),
          type: typeNum,
        },
        update: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
  },

  /* API Version */
  api_version: {
    config: {
      profiles: {
        _depth: 2,
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('v1'),
          choices: typeArray,
          type: typeStr,
        },
        update: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          type: typeStr,
        },
      },
    },
  },

  /* Asset ID */
  asset_id: {
    maintenances: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeNum,
        },
      },
    },
  },

  /* Asset Maintenance Type */
  asset_maintenance_type: {
    maintenances: {
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          choices: typeArray,
          coerce: typeFunc,
          type: typeStr,
        },
      },
    },
  },

  /* Asset Tag */
  asset_tag: {
    hardware: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Assigned Asset */
  assigned_asset: {
    hardware: {
      post: {
        checkout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
        },
      },
    },
  },

  /* Assigned Location */
  assigned_location: {
    hardware: {
      post: {
        checkout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
        },
      },
    },
  },

  /* Assigned To */
  assigned_to: {
    accessories: {
      post: {
        checkout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            required: booleanTrue,
            type: typeNum,
          },
        },
      },
    },
    consumables: {
      post: {
        checkout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            required: booleanTrue,
            type: typeNum,
          },
        },
      },
    },
  },

  /* Assigned User */
  assigned_user: {
    hardware: {
      post: {
        checkout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
        },
      },
    },
  },

  /* Auto Asset Tag */
  auto_asset_tag: {
    config: {
      profiles: {
        _depth: 2,
        create: {
          _depth: 1,
          description,
          default: booleanTrue,
          type: typeBoolean,
        },
        update: {
          _depth: 1,
          description,
          type: typeBoolean,
        },
      },
    },
  },

  /* Category ID */
  category_id: {
    accessories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeNum,
        },
      },
    },
    components: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeNum,
        },
      },
    },
    consumables: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeNum,
        },
      },
    },
    hardware: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeNum,
        },
      },
    },
    models: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeNum,
        },
      },
    },
  },

  /* Category Type */
  category_type: {
    categories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          choices: typeArray,
          type: typeStr,
        },
      },
    },
  },

  /* Checkin Email */
  checkin_email: {
    categories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          default: booleanFalse,
          type: typeBoolean,
        },
      },
    },
  },

  /* Checkout At */
  checkout_at: {
    hardware: {
      post: {
        checkout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
      },
    },
  },

  /* Checkout to Type */
  checkout_to_type: {
    hardware: {
      post: {
        checkout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            choices: typeArray,
            default: defaultStr('user'),
            type: typeStr,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            choices: typeArray,
            default: defaultStr('user'),
            type: typeStr,
          },
        },
      },
    },
  },

  /* City */
  city: {
    locations: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Color */
  color: {
    statuslabels: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Company ID */
  company_id: {
    accessories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    components: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    consumables: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    departments: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    hardware: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
  },

  /* Completion Date */
  completion_date: {
    maintenances: {
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Confirm */
  confirm: {
    config: {
      profiles: {
        _depth: 2,
        delete: {
          _depth: 2,
          all: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeBoolean,
          },
          byname: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeBoolean,
          },
        },
      },
    },
  },

  /* Cost */
  cost: {
    maintenances: {
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Country */
  country: {
    locations: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          coerce: typeFunc,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          coerce: typeFunc,
          type: typeStr,
        },
      },
    },
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          coerce: typeFunc,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          coerce: typeFunc,
          type: typeStr,
        },
      },
    },
  },

  /* Currency */
  currency: {
    locations: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Custom Fieldname */
  custom_fieldname: {
    hardware: {
      get: {
        search: {
          _depth: 1,
          description,
          type: typeStr,
        },
      },
      patch: {
        byid: {
          _depth: 1,
          description,
          type: typeStr,
        },
        byserial: {
          _depth: 1,
          description,
          type: typeStr,
        },
        bytag: {
          _depth: 1,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Default Label */
  default_label: {
    statuslabels: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          default: booleanFalse,
          type: typeBoolean,
        },
      },
    },
  },

  /* Department ID */
  department_id: {
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
  },

  /* Email */
  email: {
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Email User */
  email_user: {
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          default: booleanFalse,
          type: typeBoolean,
        },
      },
    },
  },

  /* Employee Num */
  employee_num: {
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Enabled */
  enabled: {
    config: {
      settings: {
        _depth: 2,
        checkforupdates: {
          _depth: 1,
          alias: typeArray,
          description,
          default: booleanTrue,
          type: typeBoolean,
        },
      },
    },
  },

  /* EOL */
  eol: {
    models: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
  },

  /* EULA Text */
  eula_text: {
    categories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Expand */
  expand: {
    accessories: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: booleanTrue,
          type: typeBoolean,
        },
      },
    },
    consumables: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: booleanTrue,
          type: typeBoolean,
        },
      },
    },
    licenses: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: booleanTrue,
          type: typeBoolean,
        },
      },
    },
    users: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: booleanTrue,
          type: typeBoolean,
        },
      },
    },
  },

  /* Expected Checking */
  expected_checkin: {
    hardware: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        checkout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
      },
    },
  },

  /* Expiration Date */
  expiration_date: {
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* export_to_file */
  export_to_file: {
    accessories: {
      get: {
        checkedout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    categories: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    companies: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    components: {
      get: {
        assets: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    consumables: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    departments: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    hardware: {
      get: {
        audit: {
          _depth: 2,
          due: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
          overdue: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        licenses: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
          byserial: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    licenses: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    locations: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    maintenances: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    models: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    reports: {
      get: {
        activity: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    statuslabels: {
      get: {
        assetlist: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    users: {
      get: {
        accessories: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        assets: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        licenses: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Fields */
  fields: {
    accessories: {
      delete: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        checkedout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    categories: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    companies: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    components: {
      get: {
        assets: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    consumables: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    departments: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    hardware: {
      delete: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        licenses: {
          _depth: 2,
          byid: {
            alias: typeArray,
            _depth: 1,
            description,
            type: typeStr,
          },
          byserial: {
            alias: typeArray,
            _depth: 1,
            description,
            type: typeStr,
          },
          bytag: {
            alias: typeArray,
            _depth: 1,
            description,
            type: typeStr,
          },
        },
        audit: {
          _depth: 2,
          due: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
          overdue: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    licenses: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    locations: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    maintenances: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    manufacturers: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    models: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    reports: {
      get: {
        activity: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    statuslabels: {
      get: {
        assetlist: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    users: {
      get: {
        accessories: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        assets: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        licenses: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Fieldset ID */
  fieldset_id: {
    models: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
  },

  /* First Name */
  first_name: {
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
  },

  /* Is Warranty */
  is_warranty: {
    maintenances: {
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          default: booleanFalse,
          type: typeBoolean,
        },
      },
    },
  },

  /* Item ID */
  item_id: {
    reports: {
      get: {
        activity: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Item No */
  item_no: {
    consumables: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Item Type */
  item_type: {
    reports: {
      get: {
        activity: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Jobtitle */
  jobtitle: {
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Last Name */
  last_name: {
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* LDAP OU */
  ldap_ou: {
    locations: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* License Email */
  license_email: {
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* License Name */
  license_name: {
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Limit */
  limit: {
    accessories: {
      get: {
        checkedout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultNumber(50),
            type: typeNum,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultNumber(50),
          type: typeNum,
        },
      },
    },
    categories: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultNumber(50),
          type: typeNum,
        },
      },
    },
    companies: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultNumber(50),
          type: typeNum,
        },
      },
    },
    components: {
      get: {
        assets: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultNumber(50),
            type: typeNum,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultNumber(50),
          type: typeNum,
        },
      },
    },
    consumables: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultNumber(50),
          type: typeNum,
        },
      },
    },
    departments: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultNumber(50),
          type: typeNum,
        },
      },
    },
    hardware: {
      get: {
        audit: {
          _depth: 2,
          due: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultNumber(50),
            type: typeNum,
          },
          overdue: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultNumber(50),
            type: typeNum,
          },
        },
        licenses: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultNumber(50),
            type: typeNum,
          },
          byserial: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultNumber(50),
            type: typeNum,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultNumber(50),
            type: typeNum,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultNumber(50),
          type: typeNum,
        },
      },
    },
    licenses: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultNumber(50),
          type: typeNum,
        },
      },
    },
    locations: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultNumber(50),
          type: typeNum,
        },
      },
    },
    maintenances: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultNumber(50),
          type: typeNum,
        },
      },
    },
    manufacturers: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultNumber(50),
          type: typeNum,
        },
      },
    },
    models: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultNumber(50),
          type: typeNum,
        },
      },
    },
    reports: {
      get: {
        activity: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultNumber(50),
          type: typeNum,
        },
      },
    },
    statuslabels: {
      get: {
        assetlist: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultNumber(50),
            type: typeNum,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultNumber(50),
          type: typeNum,
        },
      },
    },
    users: {
      get: {
        accessories: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultNumber(50),
            type: typeNum,
          },
        },
        assets: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultNumber(50),
            type: typeNum,
          },
        },
        licenses: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultNumber(50),
            type: typeNum,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultNumber(50),
          type: typeNum,
        },
      },
    },
  },

  /* Location ID */
  location_id: {
    accessories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    components: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    consumables: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    departments: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    hardware: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        audit: {
          _depth: 2,
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
        },
        checkin: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
        },
      },
    },
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
  },

  /* Maintained */
  maintained: {
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
  },

  /* Manager ID */
  manager_id: {
    departments: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    locations: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
  },

  /* Manufacturer ID */
  manufacturer_id: {
    accessories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    consumables: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    hardware: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeNum,
        },
      },
    },
    models: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeNum,
        },
      },
    },
  },

  /* Min Amt */
  min_amt: {
    accessories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          hidden: booleanTrue,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          hidden: booleanTrue,
          type: typeNum,
        },
      },
    },
    components: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    consumables: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
  },

  /* Model ID */
  model_id: {
    hardware: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeNum,
        },
      },
    },
  },

  /* Model Number */
  model_number: {
    accessories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    consumables: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    models: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Name */
  name: {
    accessories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
    categories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
    companies: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
    components: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
    consumables: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
    departments: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
    hardware: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        checkin: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        checkout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
    locations: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
    manufacturers: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
    models: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
    statuslabels: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
    config: {
      profiles: {
        _depth: 2,
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
        update: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
  },

  /* New Name */
  new_name: {
    config: {
      profiles: {
        _depth: 2,
        update: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Next Audit Date */
  next_audit_date: {
    hardware: {
      post: {
        audit: {
          _depth: 2,
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
      },
    },
  },

  /* Note */
  note: {
    accessories: {
      post: {
        checkout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
      },
    },
    consumables: {
      post: {
        checkout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
      },
    },
  },

  /* Notes */
  notes: {
    hardware: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        audit: {
          _depth: 2,
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        checkin: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        checkout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeStr,
          },
        },
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    models: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    maintenances: {
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    statuslabels: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Offset */
  offset: {
    accessories: {
      get: {
        checkedout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    categories: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    companies: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    components: {
      get: {
        assets: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    consumables: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    departments: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    hardware: {
      get: {
        audit: {
          _depth: 2,
          due: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
          overdue: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
        },
        licenses: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
          byserial: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    licenses: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    locations: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    maintenances: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    manufacturers: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    models: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    reports: {
      get: {
        activity: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    statuslabels: {
      get: {
        assetlist: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    users: {
      get: {
        accessories: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
        },
        assets: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
        },
        licenses: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeNum,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
  },

  /* Order */
  order: {
    accessories: {
      get: {
        checkedout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            choices: typeArray,
            default: defaultStr('asc'),
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          default: defaultStr('asc'),
          type: typeStr,
        },
      },
    },
    categories: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          default: defaultStr('asc'),
          type: typeStr,
        },
      },
    },
    companies: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          default: defaultStr('asc'),
          type: typeStr,
        },
      },
    },
    components: {
      get: {
        assets: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            choices: typeArray,
            default: defaultStr('asc'),
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          default: defaultStr('asc'),
          type: typeStr,
        },
      },
    },
    consumables: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          default: defaultStr('asc'),
          type: typeStr,
        },
      },
    },
    departments: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          default: defaultStr('asc'),
          type: typeStr,
        },
      },
    },
    hardware: {
      get: {
        audit: {
          _depth: 2,
          due: {
            _depth: 1,
            alias: typeArray,
            description,
            choices: typeArray,
            default: defaultStr('asc'),
            type: typeStr,
          },
          overdue: {
            _depth: 1,
            alias: typeArray,
            description,
            choices: typeArray,
            default: defaultStr('asc'),
            type: typeStr,
          },
        },
        licenses: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            choices: typeArray,
            default: defaultStr('asc'),
            type: typeStr,
          },
          byserial: {
            _depth: 1,
            alias: typeArray,
            description,
            choices: typeArray,
            default: defaultStr('asc'),
            type: typeStr,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            choices: typeArray,
            default: defaultStr('asc'),
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          default: defaultStr('asc'),
          type: typeStr,
        },
      },
    },
    licenses: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          default: defaultStr('asc'),
          type: typeStr,
        },
      },
    },
    locations: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          default: defaultStr('asc'),
          type: typeStr,
        },
      },
    },
    maintenances: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          default: defaultStr('asc'),
          type: typeStr,
        },
      },
    },
    manufacturers: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          default: defaultStr('asc'),
          type: typeStr,
        },
      },
    },
    models: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          default: defaultStr('asc'),
          type: typeStr,
        },
      },
    },
    statuslabels: {
      get: {
        assetlist: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            choices: typeArray,
            default: defaultStr('asc'),
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          default: defaultStr('asc'),
          type: typeStr,
        },
      },
    },
    users: {
      get: {
        accessories: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            choices: typeArray,
            default: defaultStr('asc'),
            type: typeStr,
          },
        },
        assets: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            choices: typeArray,
            default: defaultStr('asc'),
            type: typeStr,
          },
        },
        licenses: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            choices: typeArray,
            default: defaultStr('asc'),
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          default: defaultStr('asc'),
          type: typeStr,
        },
      },
    },
  },

  /* Order Number */
  order_number: {
    accessories: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    components: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    consumables: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    hardware: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    licenses: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Output */
  output: {
    accessories: {
      delete: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
        checkedout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('default'),
            choices: typeArray,
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
    },
    categories: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
    },
    companies: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
    },
    components: {
      get: {
        assets: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('default'),
            choices: typeArray,
            type: typeStr,
          },
        },
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
    },
    consumables: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
    },
    departments: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
    },
    hardware: {
      delete: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
      get: {
        audit: {
          _depth: 2,
          due: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('default'),
            choices: typeArray,
            type: typeStr,
          },
          overdue: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('default'),
            choices: typeArray,
            type: typeStr,
          },
        },
        licenses: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('default'),
            choices: typeArray,
            type: typeStr,
          },
          byserial: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('default'),
            choices: typeArray,
            type: typeStr,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('default'),
            choices: typeArray,
            type: typeStr,
          },
        },
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
    },
    licenses: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
    },
    locations: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
    },
    maintenances: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
    },
    manufacturers: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
    },
    models: {
      get: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
    },
    reports: {
      get: {
        activity: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
    },
    statuslabels: {
      get: {
        assetlist: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('default'),
            choices: typeArray,
            type: typeStr,
          },
        },
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
    },
    users: {
      get: {
        accessories: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('default'),
            choices: typeArray,
            type: typeStr,
          },
        },
        assets: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('default'),
            choices: typeArray,
            type: typeStr,
          },
        },
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
        licenses: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('default'),
            choices: typeArray,
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('default'),
          choices: typeArray,
          type: typeStr,
        },
      },
    },
    config: {
      profiles: {
        _depth: 2,
        list: {
          _depth: 2,
          active: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('default'),
            choices: typeArray,
            type: typeStr,
          },
          all: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('default'),
            choices: typeArray,
            type: typeStr,
          },
          byname: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('default'),
            choices: typeArray,
            type: typeStr,
          },
        },
      },
    },
  },

  /* Parent ID */
  parent_id: {
    locations: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
  },

  /* Password */
  password: {
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
  },

  /* Password Confirmation */
  password_confirmation: {
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
  },

  /* Permissions */
  permissions: {
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Phone */
  phone: {
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Purchase Cost */
  purchase_cost: {
    accessories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    components: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    consumables: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    hardware: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Purchase Date */
  purchase_date: {
    accessories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    components: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    consumables: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    hardware: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Purchase Order */
  purchase_order: {
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Qty */
  qty: {
    accessories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeNum,
        },
      },
    },
    components: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeNum,
        },
      },
    },
    consumables: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeNum,
        },
      },
    },
  },

  /* Reassignable */
  reassignable: {
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
  },

  /* Requestable */
  requestable: {
    consumables: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
    hardware: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
    // models: {
    //   patch: {
    //     byid: {
    //       _depth: 1,
    //       alias: typeArray,
    //       description,
    //       type: typeBoolean,
    //     },
    //   },
    //   post: {
    //     create: {
    //       _depth: 1,
    //       alias: typeArray,
    //       description,
    //       type: typeBoolean,
    //     },
    //   },
    // },
  },

  /* Require Acceptance */
  require_acceptance: {
    categories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          default: booleanFalse,
          type: typeBoolean,
        },
      },
    },
  },

  /* RTD Location ID */
  rtd_location_id: {
    hardware: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
  },

  /* Seats */
  seats: {
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeNum,
        },
      },
    },
  },

  /* Serial */
  serial: {
    components: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    hardware: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Show In Nav */
  show_in_nav: {
    statuslabels: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          default: booleanTrue,
          type: typeBoolean,
        },
      },
    },
  },

  /* Sort */
  sort: {
    accessories: {
      get: {
        checkedout: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('id'),
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('id'),
          type: typeStr,
        },
      },
    },
    categories: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('id'),
          type: typeStr,
        },
      },
    },
    companies: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('id'),
          type: typeStr,
        },
      },
    },
    components: {
      get: {
        assets: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('id'),
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('id'),
          type: typeStr,
        },
      },
    },
    consumables: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('id'),
          type: typeStr,
        },
      },
    },
    departments: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('id'),
          type: typeStr,
        },
      },
    },
    hardware: {
      get: {
        audit: {
          _depth: 2,
          due: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('id'),
            type: typeStr,
          },
          overdue: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('id'),
            type: typeStr,
          },
        },
        licenses: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('id'),
            type: typeStr,
          },
          byserial: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('id'),
            type: typeStr,
          },
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('id'),
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('id'),
          type: typeStr,
        },
      },
    },
    licenses: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('id'),
          type: typeStr,
        },
      },
    },
    locations: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('id'),
          type: typeStr,
        },
      },
    },
    maintenances: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('id'),
          type: typeStr,
        },
      },
    },
    manufacturers: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('id'),
          type: typeStr,
        },
      },
    },
    models: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('id'),
          type: typeStr,
        },
      },
    },
    statuslabels: {
      get: {
        assetlist: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('id'),
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('id'),
          type: typeStr,
        },
      },
    },
    users: {
      get: {
        accessories: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('id'),
            type: typeStr,
          },
        },
        assets: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('id'),
            type: typeStr,
          },
        },
        licenses: {
          _depth: 2,
          byid: {
            _depth: 1,
            alias: typeArray,
            description,
            default: defaultStr('id'),
            type: typeStr,
          },
        },
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          default: defaultStr('id'),
          type: typeStr,
        },
      },
    },
  },

  /* Start Date */
  start_date: {
    maintenances: {
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
  },

  /* State */
  state: {
    locations: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Status */
  status: {
    hardware: {
      get: {
        search: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          type: typeStr,
        },
      },
    },
  },

  /* Status ID */
  status_id: {
    hardware: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeNum,
        },
      },
    },
  },

  /* Supplier ID */
  supplier_id: {
    accessories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    hardware: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
    maintenances: {
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeNum,
        },
      },
    },
  },

  /* Support Email */
  support_email: {
    manufacturers: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Support Phone */
  support_phone: {
    manufacturers: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Support URL */
  support_url: {
    manufacturers: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Target ID */
  target_id: {
    reports: {
      get: {
        activity: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Target Type */
  target_type: {
    reports: {
      get: {
        activity: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Termination Date */
  termination_date: {
    licenses: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Title */
  title: {
    maintenances: {
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
  },

  /* Two Factor Enrolled */
  two_factor_enrolled: {
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          default: booleanFalse,
          type: typeBoolean,
        },
      },
    },
  },

  /* Two Factor Optin */
  two_factor_optin: {
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          default: booleanFalse,
          type: typeBoolean,
        },
      },
    },
  },

  /* Type */
  type: {
    statuslabels: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          choices: typeArray,
          default: defaultStr('deployable'),
          type: typeStr,
        },
      },
    },
  },

  /* Update Location */
  update_location: {
    hardware: {
      post: {
        audit: {
          _depth: 2,
          bytag: {
            _depth: 1,
            alias: typeArray,
            description,
            type: typeBoolean,
          },
        },
      },
    },
  },

  /* URL */
  url: {
    manufacturers: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    config: {
      profiles: {
        _depth: 2,
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          demandOption: booleanTrue,
          type: typeStr,
        },
        update: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Use Default EULA */
  use_default_eula: {
    categories: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeBoolean,
        },
      },
    },
  },

  /* Username */
  username: {
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          required: booleanTrue,
          type: typeStr,
        },
      },
    },
  },

  /* Warranty Months */
  warranty_months: {
    hardware: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
        byserial: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
        bytag: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeNum,
        },
      },
    },
  },

  /* Website */
  website: {
    users: {
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },

  /* Zip */
  zip: {
    locations: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
    users: {
      patch: {
        byid: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
      post: {
        create: {
          _depth: 1,
          alias: typeArray,
          description,
          type: typeStr,
        },
      },
    },
  },
}

const options = Object.keys(obj)

options.forEach((option) => {
  describe(`${option}()`, () => {
    let sections = Object.keys(obj[option])

    sections.forEach((section) => {
      describe(`section: ${section}`, () => {
        let commands = {}

        beforeEach(() => {
          commands.section = section
        })

        let methods = Object.keys(obj[option][section])

        methods.forEach((method) => {
          let currentObj = obj[option][section][method]

          describe(`method: ${method}`, () => {
            if (currentObj._depth === 1) {
              testSuite({ section, method }, option)
            } else {
              Object.keys(currentObj).forEach((item) => {
                if (item !== '_depth') {
                  depthIteration(
                    currentObj[item],
                    { section, method, command1: item },
                    option
                  )
                }
              })
            }
          })
        })
      })
    })
  })
})

/**
 * @memberof OptionsTest
 * @private
 * @description Iterates through the object creating nested describe objects for each
 * case.
 *
 * @returns {void}
 */
function depthIteration(currentObj, commands, option, lastCommandKeyUsed) {
  if (lastCommandKeyUsed) {
    let keys = Object.keys(commands)
    let index = keys.indexOf(lastCommandKeyUsed)

    if (index < keys.length - 1) {
      let count = keys.length - 1 - index

      do {
        let pop = keys.pop()

        delete commands[pop]
        count--
      } while (count > 0)
    }
  }

  let lastCommandKey
  let commandKeys = Object.keys(commands)

  if (currentObj._depth === 1) {
    lastCommandKey = commandKeys[commandKeys.length - 1]

    describe(`${lastCommandKey}: ${commands[lastCommandKey]}`, () => {
      testSuite(commands, option)
    })
  } else {
    lastCommandKey = commandKeys[commandKeys.length - 1]
    let objArray = Object.keys(currentObj)

    objArray.forEach((item) => {
      if (item !== '_depth') {
        describe(`${lastCommandKey}: ${commands[lastCommandKey]}`, () => {
          let newKey = `command${commandKeys.length - 1}`
          commands[newKey] = item

          depthIteration(currentObj[item], commands, option, newKey)
        })
      }
    })
  }
}

/**
 * @memberof OptionsTest
 * @private
 * @description Actual test suite for all of the command options.
 *
 * @returns {void}
 */
function testSuite(commands, requiredOpt) {
  let opt = require(`./${requiredOpt}`)
  let activeCmd = opt(commands)
  let props = Object.keys(activeCmd)
  let objMethods = obj[requiredOpt]

  for (let key in commands) {
    objMethods = objMethods[commands[key]]
  }

  it('should be an object', () => {
    expect(typeof activeCmd).toBe('object')
  })

  props.forEach((key) => {
    let tempArray = objMethods[key]

    it(`should contain a "${key}" as a ${tempArray[0]}`, () => {
      expect(activeCmd[key]).toEqual(expect.any(tempArray[1]))
    })

    if (key === 'type') {
      it(`should contain a "${key}" property that has a value of "${tempArray[2]}"`, () => {
        expect(activeCmd[key]).toEqual(`${tempArray[2]}`)
      })
    }

    if (key === 'default' || key === 'demandOption' || key === 'required') {
      it(`should contain a "${key}" property that has a value of "${tempArray[2]}"`, () => {
        if (typeof tempArray[2] === 'boolean') {
          expect(activeCmd[key]).toEqual(Boolean(tempArray[2]))
        } else {
          expect(activeCmd[key].toString()).toEqual(`${tempArray[2]}`)
        }
      })
    }
  })

  it(`should only contain properties: ${props
    .join()
    .replace(/,/g, ', ')}`, () => {
    let objProperties = Object.keys(activeCmd)

    props.forEach((property) => {
      let index = objProperties.indexOf(property)
      objProperties.splice(index, 1)
    })

    expect(objProperties.length).toBe(0)
  })
}
