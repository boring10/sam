/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for warranty_months.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const warranty_months = (commands) => {
  const { section, method } = commands

  if (section === 'hardware') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    hardware: {
      patch: {
        byid: {
          alias: alias('warranty_months'),
          description: 'Update the number of months for the hardware warranty.',
          type: 'number',
        },
        bytag: {
          alias: alias('warranty_months'),
          description: 'Update the number of months for the hardware warranty.',
          type: 'number',
        },
        byserial: {
          alias: alias('warranty_months'),
          description: 'Update the number of months for the hardware warranty.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('warranty_months'),
          description: 'Number of months for the hardware warranty.',
          type: 'number',
        },
      },
    },
  }
}

module.exports = warranty_months
