/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for ldap_ou.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const ldap_ou = (commands) => {
  const { section, method } = commands

  if (section === 'locations') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    locations: {
      patch: {
        byid: {
          alias: alias('ldap_ou'),
          description: 'Update the ldap organizational unit.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('ldap_ou'),
          description: 'The ldap organizational unit.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = ldap_ou
