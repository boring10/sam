/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for asset_id.
 *
 * @param {object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const asset_id = (commands) => {
  const { section, method } = commands

  if (section === 'maintenances') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {object}
 */
function optionsObj() {
  return {
    maintenances: {
      get: {
        search: {
          alias: alias('asset_id'),
          description:
            "Asset ID of the asset you'd like to return maintenances for.",
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('asset_id'),
          description: 'Unique asset ID for the asset.',
          required: true,
          type: 'number',
        },
      },
    },
  }
}

module.exports = asset_id
