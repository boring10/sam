/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for parent_id.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const parent_id = (commands) => {
  const { section, method } = commands

  if (section === 'locations') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    locations: {
      patch: {
        byid: {
          alias: alias('parent_id'),
          description:
            'Update the ID of the location that will be the parent of the new location.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('parent_id'),
          description:
            'The ID of the location that will be the parent of the new location.',
          type: 'number',
        },
      },
    },
  }
}

module.exports = parent_id
