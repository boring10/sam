/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for address2.
 *
 * @param {object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const address2 = (commands) => {
  const { section, method } = commands

  if (section === 'locations') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {object}
 */
function optionsObj() {
  return {
    locations: {
      patch: {
        byid: {
          alias: alias('address2'),
          description: 'Update the apartment, condo, floor number, etc.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('address2'),
          description: 'Apartment, condo, floor number, etc.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = address2
