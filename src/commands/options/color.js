/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for color.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const color = (commands) => {
  const { section, method } = commands

  if (section === 'statuslabels') {
    const { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  let obj = {
    alias: alias('color'),
    description:
      'Update the hex code showing what color the status label should be on the pie chart in the dashboard.',
    type: 'string',
  }

  return {
    statuslabels: {
      patch: {
        byid: obj,
      },
      post: {
        create: obj,
      },
    },
  }
}

module.exports = color
