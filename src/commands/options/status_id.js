/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for status_id.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const status_id = (commands) => {
    const { section, method } = commands

    if (section === 'hardware') {
        let { command1 } = commands

        return optionsObj()[section][method][command1]
    }

    return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
    return {
        hardware: {
            patch: {
                byid: {
                    alias: alias('status_id'),
                    description: 'Update the ID of the corresponding status label.',
                    type: 'number',
                },
                bytag: {
                    alias: alias('status_id'),
                    description: 'Update the ID of the corresponding status label.',
                    type: 'number',
                },
                byserial: {
                    alias: alias('status_id'),
                    description: 'Update the ID of the corresponding status label.',
                    type: 'number',
                },
            },
            post: {
                create: {
                    alias: alias('status_id'),
                    description: 'The ID of the corresponding status label.',
                    required: true,
                    type: 'number',
                },
                checkin: {
                    bytag: {
                        alias: alias('status_id'),
                        description: 'The ID of the corresponding status label.',
                        type: 'number',
                    },
                },
            },
        },
    }
}

module.exports = status_id
