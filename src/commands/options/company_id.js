/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for company_id.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const company_id = (commands) => {
  const { section, method } = commands

  if (
    section === 'accessories' ||
    section === 'components' ||
    section === 'consumables' ||
    section === 'departments' ||
    section === 'hardware' ||
    section === 'licenses' ||
    section === 'users'
  ) {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    accessories: {
      patch: {
        byid: {
          alias: alias('company_id'),
          description:
            'Update the ID of the company the accessory is assigned to.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('company_id'),
          description: 'ID of the company the accessory is assigned to.',
          type: 'number',
        },
      },
    },
    components: {
      patch: {
        byid: {
          alias: alias('company_id'),
          description:
            'Update the ID of the company the component is assigned to.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('company_id'),
          description: 'ID of the company the component is assigned to.',
          type: 'number',
        },
      },
    },
    consumables: {
      get: {
        search: {
          alias: alias('company_id'),
          description: 'Company ID to filter by.',
          type: 'number',
        },
      },
      patch: {
        byid: {
          alias: alias('company_id'),
          description:
            'Update ID of the company the consumable is assigned to.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('company_id'),
          description: 'ID of the company the consumable is assigned to.',
          type: 'number',
        },
      },
    },
    departments: {
      patch: {
        byid: {
          alias: alias('company_id'),
          description:
            'Update the ID of the company the department is assigned to.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('company_id'),
          description: 'ID of the company the department is assigned to.',
          type: 'number',
        },
      },
    },
    hardware: {
      get: {
        search: {
          alias: alias('company_id'),
          description:
            'Optionally restrict the hardware results to a company ID.',
          type: 'number',
        },
      },
      patch: {
        byid: {
          alias: alias('company_id'),
          description:
            'Update the ID of the company to associate the hardware with.',
          type: 'number',
        },
        bytag: {
          alias: alias('company_id'),
          description:
            'Update the ID of the company to associate the hardware with.',
          type: 'number',
        },
        byserial: {
          alias: alias('company_id'),
          description:
            'Update the ID of the company to associate the hardware with.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('company_id'),
          description: 'The ID of the company to associate the hardware with.',
          type: 'number',
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          alias: alias('company_id'),
          description:
            'Update the ID of the company to associate the licenses with.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('company_id'),
          description: 'The ID of the company to associate the licenses with.',
          type: 'number',
        },
      },
    },
    users: {
      patch: {
        byid: {
          alias: alias('company_id'),
          description:
            'Update the ID of the company to associate the user with.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('company_id'),
          description: 'The ID of the company to associate the user with.',
          type: 'number',
        },
      },
    },
  }
}

module.exports = company_id
