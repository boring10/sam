/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for checkin_email.
 *
 * @param {object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const checkin_email = (commands) => {
  const { section, method } = commands

  if (section === 'categories') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {object}
 */
function optionsObj() {
  return {
    categories: {
      patch: {
        byid: {
          alias: alias('checkin_email'),
          description:
            'Should the user be emailed the EULA and/or an acceptance confirmation email when this item is checked in/out.',
          type: 'boolean',
        },
      },
      post: {
        create: {
          alias: alias('checkin_email'),
          description:
            'Should the user be emailed the EULA and/or an acceptance confirmation email when this item is checked in/out.',
          default: false,
          type: 'boolean',
        },
      },
    },
  }
}

module.exports = checkin_email
