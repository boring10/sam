/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for offset.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const offset = (commands) => {
  const { section, method } = commands

  if (
    section === 'accessories' ||
    section === 'categories' ||
    section === 'companies' ||
    section === 'components' ||
    section === 'consumables' ||
    section === 'departments' ||
    section === 'hardware' ||
    section === 'licenses' ||
    section === 'locations' ||
    section === 'maintenances' ||
    section === 'manufacturers' ||
    section === 'models' ||
    section === 'reports' ||
    section === 'statuslabels' ||
    section === 'users'
  ) {
    let { command1 } = commands

    if (
      command1 === 'accessories' ||
      command1 === 'assetlist' ||
      command1 === 'assets' ||
      command1 === 'audit' ||
      command1 === 'checkedout' ||
      command1 === 'licenses'
    ) {
      let { command2 } = commands

      return optionsObj()[section][method][command1][command2]
    }

    return optionsObj()[section][method][command1]
  }

  return optionsObj()
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  let obj = {
    alias: alias('offset'),
    description: 'Offset to use.',
    type: 'number',
  }

  return {
    accessories: {
      get: {
        checkedout: {
          byid: obj,
        },
        search: obj,
      },
    },
    categories: {
      get: {
        search: obj,
      },
    },
    companies: {
      get: {
        search: obj,
      },
    },
    components: {
      get: {
        assets: {
          byid: obj,
        },
        search: obj,
      },
    },
    consumables: {
      get: {
        search: obj,
      },
    },
    departments: {
      get: {
        search: obj,
      },
    },
    hardware: {
      get: {
        audit: {
          due: obj,
          overdue: obj,
        },
        licenses: {
          byid: obj,
          byserial: obj,
          bytag: obj,
        },
        search: obj,
      },
    },
    licenses: {
      get: {
        search: obj,
      },
    },
    locations: {
      get: {
        search: obj,
      },
    },
    maintenances: {
      get: {
        search: obj,
      },
    },
    manufacturers: {
      get: {
        search: obj,
      },
    },
    models: {
      get: {
        search: obj,
      },
    },
    reports: {
      get: {
        activity: obj,
      },
    },
    statuslabels: {
      get: {
        assetlist: {
          byid: obj,
        },
        search: obj,
      },
    },
    users: {
      get: {
        accessories: {
          byid: obj,
        },
        assets: {
          byid: obj,
        },
        licenses: {
          byid: obj,
        },
        search: obj,
      },
    },
  }
}

module.exports = offset
