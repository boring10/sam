/**
 * @namespace CommandOptions
 */

/**
 * @memberof CommandOptions
 * @description Command option for auto_asset_tag.
 *
 * @param {object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const auto_asset_tag = (commands) => {
  const { section, method } = commands

  if (section === 'config') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {object}
 */
function optionsObj() {
  return {
    config: {
      profiles: {
        create: {
          description:
            'Set to true if you have enabled auto-incrementing asset IDs',
          default: true,
          type: 'boolean',
        },
        update: {
          description:
            'Set to true if you have enabled auto-incrementing asset IDs',
          type: 'boolean',
        },
      },
    },
  }
}

module.exports = auto_asset_tag
