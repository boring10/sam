/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for fields.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const fields = (commands) => {
  const { section, method } = commands

  if (
    section === 'accessories' ||
    section === 'categories' ||
    section === 'companies' ||
    section === 'components' ||
    section === 'consumables' ||
    section === 'departments' ||
    section === 'hardware' ||
    section === 'licenses' ||
    section === 'locations' ||
    section === 'maintenances' ||
    section === 'manufacturers' ||
    section === 'models' ||
    section === 'reports' ||
    section === 'statuslabels' ||
    section === 'users'
  ) {
    const { command1 } = commands

    if (
      command1 === 'accessories' ||
      command1 === 'assetlist' ||
      command1 === 'assets' ||
      command1 === 'checkedout' ||
      command1 === 'checkin' ||
      command1 === 'checkout' ||
      command1 === 'licenses' ||
      command1 === 'audit'
    ) {
      const { command2 } = commands

      return optionsObj()[section][method][command1][command2]
    }

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  let obj = {
    alias: alias('fields'),
    description:
      'Specify the fields that will be returned. By default, all fields will be returned.',
    type: 'string',
  }

  let objAlt = {
    alias: alias('fields'),
    description: 'Specify the fields that will be returned.',
    type: 'string',
  }

  return {
    accessories: {
      delete: {
        byid: objAlt,
      },
      get: {
        byid: obj,
        checkedout: {
          byid: obj,
        },
        search: obj,
      },
    },
    categories: {
      get: {
        byid: obj,
        search: obj,
      },
    },
    companies: {
      get: {
        byid: obj,
        search: obj,
      },
    },
    components: {
      get: {
        assets: {
          byid: obj,
        },
        byid: obj,
        search: obj,
      },
    },
    consumables: {
      get: {
        byid: obj,
        search: obj,
      },
    },
    departments: {
      get: {
        byid: obj,
        search: obj,
      },
    },
    hardware: {
      delete: {
        byid: objAlt,
        bytag: objAlt,
      },
      get: {
        byid: obj,
        bytag: obj,
        byserial: obj,
        audit: {
          due: obj,
          overdue: obj,
        },
        licenses: {
          byid: obj,
          bytag: obj,
          byserial: obj,
        },
        search: obj,
      },
      patch: {
        byid: objAlt,
        bytag: objAlt,
        byserial: objAlt,
      },
      post: {
        checkin: {
          byid: objAlt,
          bytag: objAlt,
        },
        checkout: {
          byid: objAlt,
          bytag: objAlt,
        },
        create: objAlt,
      },
    },
    licenses: {
      get: {
        byid: obj,
        search: obj,
      },
    },
    locations: {
      get: {
        byid: obj,
        search: obj,
      },
    },
    maintenances: {
      get: {
        byid: obj,
        search: obj,
      },
    },
    manufacturers: {
      get: {
        byid: obj,
        search: obj,
      },
    },
    models: {
      get: {
        byid: obj,
        search: obj,
      },
    },
    reports: {
      get: {
        activity: obj,
      },
    },
    statuslabels: {
      get: {
        assetlist: {
          byid: obj,
        },
        byid: obj,
        search: obj,
      },
    },
    users: {
      get: {
        accessories: {
          byid: obj,
        },
        assets: {
          byid: obj,
        },
        byid: obj,
        search: obj,
        licenses: {
          byid: obj,
        },
      },
    },
  }
}

module.exports = fields
