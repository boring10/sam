/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for serial.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const serial = (commands) => {
  const { section, method } = commands

  if (
    section === 'components' ||
    section === 'hardware' ||
    section === 'licenses'
  ) {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    components: {
      patch: {
        byid: {
          alias: alias('serial'),
          description: 'Update the serial number of the component.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('serial'),
          description: 'The serial number of the component.',
          type: 'string',
        },
      },
    },
    hardware: {
      patch: {
        byid: {
          alias: alias('serial'),
          description: 'Update the serial number of the hardware.',
          type: 'string',
        },
        bytag: {
          alias: alias('serial'),
          description: 'Update the serial number of the hardware.',
          type: 'string',
        },
        byserial: {
          alias: alias('serial'),
          description: 'Update the serial number of the hardware.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('serial'),
          description: 'The serial number of the hardware.',
          type: 'string',
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          alias: alias('serial'),
          description:
            'Update the serial number (product key) of the licenses.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('serial'),
          description: 'Serial number (product key) of the licenses.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = serial
