/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for item_no.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const item_no = (commands) => {
  const { section, method } = commands

  if (section === 'consumables') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    consumables: {
      patch: {
        byid: {
          alias: alias('item_no'),
          description: 'Update the item number of the component.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('item_no'),
          description: 'The item number of the component.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = item_no
