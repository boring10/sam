/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for expected_checkin.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const expected_checkin = (commands) => {
  const { section, method } = commands

  if (section === 'hardware') {
    let { command1 } = commands

    if (command1 === 'checkout') {
      let { command2 } = commands

      return optionsObj()[section][method][command1][command2]
    }

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    hardware: {
      patch: {
        byid: {
          alias: alias('expected_checkin'),
          description:
            'Update the date that the asset is expected to be checked back in.',
          type: 'string',
        },
        bytag: {
          alias: alias('expected_checkin'),
          description:
            'Update the date that the asset is expected to be checked back in.',
          type: 'string',
        },
        byserial: {
          alias: alias('expected_checkin'),
          description:
            'Update the date that the asset is expected to be checked back in.',
          type: 'string',
        },
      },
      post: {
        checkout: {
          byid: {
            alias: alias('expected_checkin'),
            description:
              'Date that the asset is expected to be checked back in.',
            type: 'string',
          },
          bytag: {
            alias: alias('expected_checkin'),
            description:
              'Date that the asset is expected to be checked back in.',
            type: 'string',
          },
        },
      },
    },
  }
}

module.exports = expected_checkin
