/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for qty.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const qty = (commands) => {
  const { section, method } = commands

  if (
    section === 'accessories' ||
    section === 'components' ||
    section === 'consumables'
  ) {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    accessories: {
      patch: {
        byid: {
          alias: alias('qty'),
          description: 'Update the quantity of the accessory you have.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('qty'),
          description: 'Quantity of the accessory you have.',
          required: true,
          type: 'number',
        },
      },
    },
    components: {
      patch: {
        byid: {
          alias: alias('qty'),
          description: 'Update the quantity of the component you have.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('qty'),
          description: 'Quantity of the component you have.',
          required: true,
          type: 'number',
        },
      },
    },
    consumables: {
      patch: {
        byid: {
          alias: alias('qty'),
          description: 'Update the quantity of the consumable you have.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('qty'),
          description: 'Quantity of the consumable you have.',
          required: true,
          type: 'number',
        },
      },
    },
  }
}

module.exports = qty
