/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for update_location.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const update_location = (commands) => {
  const { section, method } = commands

  if (section === 'hardware') {
    let { command1 } = commands

    if (command1 === 'audit') {
      let { command2 } = commands

      return optionsObj()[section][method][command1][command2]
    }

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    hardware: {
      post: {
        audit: {
          bytag: {
            alias: alias('update_location'),
            description: 'Whether to update the location or not.',
            type: 'boolean',
          },
        },
      },
    },
  }
}

module.exports = update_location
