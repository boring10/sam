/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for rtd_location_id.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const rtd_location_id = (commands) => {
  const { section, method } = commands

  if (
    section === 'accessories' ||
    section === 'components' ||
    section === 'consumables' ||
    section === 'departments' ||
    section === 'hardware' ||
    section === 'users'
  ) {
    let { command1 } = commands

    if (command1 === 'checkin' || command1 === 'audit') {
      let { command2 } = commands

      return optionsObj()[section][method][command1][command2]
    }

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    accessories: {
      patch: {
        byid: {
          alias: alias('location_id'),
          description:
            'Update the ID of the location the accessory is assigned to.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('location_id'),
          description: 'ID of the location the accessory is assigned to.',
          type: 'number',
        },
      },
    },
    components: {
      patch: {
        byid: {
          alias: alias('location_id'),
          description:
            'Update the ID of the location the component is assigned to.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('location_id'),
          description: 'ID of the location the component is assigned to.',
          type: 'number',
        },
      },
    },
    consumables: {
      patch: {
        byid: {
          alias: alias('location_id'),
          description:
            'Update the ID of the location the consumable is assigned to.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('location_id'),
          description: 'ID of the location the consumable is assigned to.',
          type: 'number',
        },
      },
    },
    departments: {
      patch: {
        byid: {
          alias: alias('location_id'),
          description:
            'Update the ID of the location the departments is assigned to.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('location_id'),
          description: 'ID of the location the accessory is assigned to.',
          type: 'number',
        },
      },
    },
    hardware: {
      get: {
        search: {
          alias: alias('location_id'),
          description:
            'Optionally restrict the hardware results to this location ID.',
          type: 'number',
        },
      },
      patch: {
        byserial: {
          alias: alias('location_id'),
          description: 'The location ID of the asset.',
          type: 'number',
        },
      },
      post: {
        audit: {
          bytag: {
            alias: alias('location_id'),
            description: 'The location ID of the asset.',
            type: 'number',
          },
        },
        checkin: {
          byid: {
            alias: alias('location_id'),
            description: 'The location ID of the asset.',
            type: 'number',
          },
          bytag: {
            alias: alias('location_id'),
            description: 'The location ID of the asset.',
            type: 'number',
          },
        },
      },
    },
    users: {
      patch: {
        byid: {
          alias: alias('location_id'),
          description: 'Update the location ID of the user.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('location_id'),
          description: 'The location ID of the user.',
          type: 'number',
        },
      },
    },
  }
}

module.exports = rtd_location_id
