/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for eula_text.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const eula_text = (commands) => {
  const { section, method } = commands

  if (section === 'categories') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    categories: {
      patch: {
        byid: {
          alias: alias('eula_text'),
          description:
            'Customize your EULA for the specific type of item. If there is only one EULA for all of your items then set the "use_default_eula" to true.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('eula_text'),
          description:
            'Customize your EULA for the specific type of item. If there is only one EULA for all of your items then set the "use_default_eula" to true.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = eula_text
