/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for enabled.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const enabled = (commands) => {
  const { section, method } = commands

  if (section === 'config' && method === 'settings') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }
  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    config: {
      settings: {
        checkforupdates: {
          alias: alias('enabled'),
          description:
            'States whether to make this profile the enabled profile. If this is the only profile then it will be enabled by default.',
          default: true,
          type: 'boolean',
        },
      },
    },
  }
}

module.exports = enabled
