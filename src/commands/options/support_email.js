/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for support_email.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const support_email = (commands) => {
  const { section, method } = commands

  if (section === 'manufacturers') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    manufacturers: {
      patch: {
        byid: {
          alias: alias('support_email'),
          description: 'Update the support email for the manufacturer.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('support_email'),
          description: 'Support email for the manufacturer.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = support_email
