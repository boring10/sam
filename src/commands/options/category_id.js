/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for category_id.
 *
 * @param {object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const category_id = (commands) => {
  const { section, method } = commands

  if (
    section === 'accessories' ||
    section === 'components' ||
    section === 'consumables' ||
    section === 'hardware' ||
    section === 'licenses' ||
    section === 'models'
  ) {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {object}
 */
function optionsObj() {
  return {
    accessories: {
      patch: {
        byid: {
          alias: alias('category_id'),
          description:
            'Update the ID of the category the accessory belongs to.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('category_id'),
          description: 'ID of the category the accessory belongs to.',
          required: true,
          type: 'number',
        },
      },
    },
    components: {
      patch: {
        byid: {
          alias: alias('category_id'),
          description:
            'Update the ID of the category the component belongs to.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('category_id'),
          description: 'ID of the category the component belongs to.',
          required: true,
          type: 'number',
        },
      },
    },
    consumables: {
      get: {
        search: {
          alias: alias('category_id'),
          description: 'Category ID to filter by.',
          type: 'number',
        },
      },
      patch: {
        byid: {
          alias: alias('category_id'),
          description: 'Update ID of the category the consumable belongs to.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('category_id'),
          description: 'ID of the category the consumable belongs to.',
          required: true,
          type: 'number',
        },
      },
    },
    hardware: {
      get: {
        search: {
          alias: alias('category_id'),
          description:
            'Optionally restrict the hardware results to this category ID.',
          type: 'number',
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          alias: alias('category_id'),
          description: 'Update the ID of the category the license belongs to.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('category_id'),
          description: 'ID of the category the license belongs to.',
          required: true,
          type: 'number',
        },
      },
    },
    models: {
      patch: {
        byid: {
          alias: alias('category_id'),
          description: 'Update the ID of the category the model belongs to.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('category_id'),
          description: 'ID of the category the model belongs to.',
          required: true,
          type: 'number',
        },
      },
    },
  }
}

module.exports = category_id
