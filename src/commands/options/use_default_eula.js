/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for use_default_eula.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const use_default_eula = (commands) => {
  const { section, method } = commands

  if (section === 'categories') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    categories: {
      patch: {
        byid: {
          alias: alias('use_default_eula'),
          description: 'Use the primary default EULA instead.',
          type: 'boolean',
        },
      },
      post: {
        create: {
          alias: alias('use_default_eula'),
          description: 'Use the primary default EULA instead.',
          type: 'boolean',
        },
      },
    },
  }
}

module.exports = use_default_eula
