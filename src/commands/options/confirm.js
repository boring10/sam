/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for confirm.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const confirm = (commands) => {
  const { section, method } = commands

  if (section === 'config' && method === 'profiles') {
    const { command1 } = commands

    if (command1 === 'delete') {
      const { command2 } = commands

      return optionsObj()[section][method][command1][command2]
    }

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    config: {
      profiles: {
        delete: {
          all: {
            alias: alias('confirm'),
            description:
              'Confirm that you would like to delete ALL of the profiles without being prompted.',
            type: 'boolean',
          },
          byname: {
            alias: alias('confirm'),
            description:
              'Confirm that you would like to delete the profile without being prompted.',
            type: 'boolean',
          },
        },
      },
    },
  }
}

module.exports = confirm
