/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for item_type.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const item_type = (commands) => {
  const { section, method } = commands

  if (section === 'reports') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    reports: {
      get: {
        activity: {
          alias: alias('item_type'),
          description:
            "The type of item you're searching against. App\\Models\\Asset, etc. Required when passing item_id.",
          type: 'string',
        },
      },
    },
  }
}

module.exports = item_type
