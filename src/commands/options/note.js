/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for notes.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const notes = (commands) => {
  const { section, method } = commands

  if (section === 'accessories' || section === 'consumables') {
    let { command1 } = commands

    if (command1 === 'checkout') {
      let { command2 } = commands

      return optionsObj()[section][method][command1][command2]
    }

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    accessories: {
      post: {
        checkout: {
          byid: {
            alias: alias('note'),
            description: 'Note about the accessory.',
            type: 'string',
          },
        },
      },
    },
    consumables: {
      post: {
        checkout: {
          byid: {
            alias: alias('note'),
            description: 'Note about the consumable.',
            type: 'string',
          },
        },
      },
    },
  }
}

module.exports = notes
