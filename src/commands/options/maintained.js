/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for maintained.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const maintained = (commands) => {
  const { section, method } = commands

  if (section === 'licenses') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    licenses: {
      patch: {
        byid: {
          alias: alias('maintained'),
          description: 'Update the maintained status of the license.',
          type: 'boolean',
        },
      },
      post: {
        create: {
          alias: alias('maintained'),
          description: 'Maintained status of the license.',
          type: 'boolean',
        },
      },
    },
  }
}

module.exports = maintained
