/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for completion_date.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const completion_date = (commands) => {
  const { section, method } = commands

  if (section === 'maintenances') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    maintenances: {
      post: {
        create: {
          alias: alias('completion_date'),
          description: 'Date of maintenance completion.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = completion_date
