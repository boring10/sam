/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for email.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const email = (commands) => {
  const { section, method } = commands

  if (section === 'users') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    users: {
      patch: {
        byid: {
          alias: alias('email'),
          description: 'Update the email for the user.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('email'),
          description: 'The email for the user.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = email
