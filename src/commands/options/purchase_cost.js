/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for purchase_cost.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const purchase_cost = (commands) => {
  const { section, method } = commands

  if (
    section === 'accessories' ||
    section === 'components' ||
    section === 'consumables' ||
    section === 'hardware' ||
    section === 'licenses'
  ) {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    accessories: {
      patch: {
        byid: {
          alias: alias('purchase_cost'),
          description: 'Update the cost of item being purchased.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('purchase_cost'),
          description: 'Cost of item being purchased.',
          type: 'string',
        },
      },
    },
    components: {
      patch: {
        byid: {
          alias: alias('purchase_cost'),
          description: 'Update the cost of item being purchased.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('purchase_cost'),
          description: 'Cost of item being purchased.',
          type: 'string',
        },
      },
    },
    consumables: {
      patch: {
        byid: {
          alias: alias('purchase_cost'),
          description: 'Update the cost of item being purchased.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('purchase_cost'),
          description: 'Cost of item being purchased.',
          type: 'string',
        },
      },
    },
    hardware: {
      patch: {
        byid: {
          alias: alias('purchase_cost'),
          description:
            'Update the purchase cost of the hardware, without the currency symbol.',
          type: 'string',
        },
        bytag: {
          alias: alias('purchase_cost'),
          description:
            'Update the purchase cost of the hardware, without the currency symbol.',
          type: 'string',
        },
        byserial: {
          alias: alias('purchase_cost'),
          description:
            'Update the purchase cost of the hardware, without the currency symbol.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('purchase_cost'),
          description:
            'The purchase cost of the hardware, without the currency symbol.',
          type: 'string',
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          alias: alias('purchase_cost'),
          description: 'Update the cost of the license.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('purchase_cost'),
          description: 'Cost of the license.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = purchase_cost
