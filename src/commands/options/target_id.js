/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for target_id.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const target_id = (commands) => {
  const { section, method } = commands

  if (section === 'reports') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    reports: {
      get: {
        activity: {
          alias: alias('target_id'),
          description:
            "The ID of the target you're querying against. Required if passing target_type.",
          type: 'string',
        },
      },
    },
  }
}

module.exports = target_id
