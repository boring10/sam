/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for status.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const status = (commands) => {
  const { section, method } = commands

  if (section === 'hardware') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    hardware: {
      get: {
        search: {
          alias: alias('status'),
          description: 'Optionally restrict the results to a status type.',
          choices: ['archived', 'deleted', 'deployed', 'requestable', 'rtd'],
          type: 'string',
        },
      },
    },
  }
}

module.exports = status
