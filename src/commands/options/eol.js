/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for eol.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const eol = (commands) => {
  const { section, method } = commands

  if (section === 'models') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    models: {
      patch: {
        byid: {
          alias: alias('eol'),
          description:
            "Update the number of months until this model's assets are considered EOL.",
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('eol'),
          description:
            "Number of months until this model's assets are considered EOL.",
          type: 'number',
        },
      },
    },
  }
}

module.exports = eol
