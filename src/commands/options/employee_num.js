/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for employee_num.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const employee_num = (commands) => {
  const { section, method } = commands

  if (section === 'users') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    users: {
      patch: {
        byid: {
          alias: alias('employee_num'),
          description: 'Update the employee number of the user.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('employee_num'),
          description: 'The employee number of the user.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = employee_num
