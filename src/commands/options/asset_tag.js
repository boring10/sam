/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for asset_tag.
 *
 * @param {object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const asset_tag = (commands) => {
  const { section, method } = commands

  if (section === 'hardware') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {object}
 */
function optionsObj() {
  return {
    hardware: {
      patch: {
        byid: {
          alias: alias('asset_tag'),
          description: 'Update the unique asset tag for the asset.',
          type: 'string',
        },
        bytag: {
          alias: alias('asset_tag'),
          description: 'Update the unique asset tag for the asset.',
          type: 'string',
        },
        byserial: {
          alias: alias('asset_tag'),
          description: 'Update the unique asset tag for the asset.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('asset_tag'),
          description: 'Unique asset tag for the asset.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = asset_tag
