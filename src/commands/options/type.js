/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for type.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const type = (commands) => {
  const { section, method } = commands

  if (section === 'statuslabels') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    statuslabels: {
      patch: {
        byid: {
          description:
            'Update the type of status designation for the status label.',
          choices: ['deployable', 'pending', 'undeployable', 'archived'],
          type: 'string',
        },
      },
      post: {
        create: {
          description: 'The type of status designation for the status label.',
          choices: ['deployable', 'pending', 'undeployable', 'archived'],
          default: 'deployable',
          type: 'string',
        },
      },
    },
  }
}

module.exports = type
