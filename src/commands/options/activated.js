/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for activated.
 *
 * @param {object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const activated = (commands) => {
  const { section, method } = commands

  if (section === 'users') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {object}
 */
function optionsObj() {
  return {
    users: {
      patch: {
        byid: {
          alias: alias('activated'),
          description: 'Whether the user is able to login or not.',
          type: 'boolean',
        },
      },
      post: {
        create: {
          alias: alias('activated'),
          description: 'Whether the user is able to login or not.',
          default: false,
          type: 'boolean',
        },
      },
    },
  }
}

module.exports = activated
