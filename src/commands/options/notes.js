/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for notes.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const notes = (commands) => {
  const { section, method } = commands

  if (
    section === 'hardware' ||
    section === 'licenses' ||
    section === 'models' ||
    section === 'maintenances' ||
    section === 'statuslabels' ||
    section === 'users'
  ) {
    let { command1 } = commands

    if (
      command1 === 'checkin' ||
      command1 === 'checkout' ||
      command1 === 'audit'
    ) {
      let { command2 } = commands

      return optionsObj()[section][method][command1][command2]
    }

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    hardware: {
      patch: {
        byid: {
          alias: alias('notes'),
          description: 'Update the notes for the hardware.',
          type: 'string',
        },
        bytag: {
          alias: alias('notes'),
          description: 'Update the notes for the hardware.',
          type: 'string',
        },
        byserial: {
          alias: alias('notes'),
          description: 'Update the notes for the hardware.',
          type: 'string',
        },
      },
      post: {
        audit: {
          bytag: {
            alias: alias('notes'),
            description: 'Notes about the audit.',
            type: 'string',
          },
        },
        checkin: {
          byid: {
            alias: alias('notes'),
            description: 'Notes about the asset checkin.',
            type: 'string',
          },
          bytag: {
            alias: alias('notes'),
            description: 'Notes about the asset checkin.',
            type: 'string',
          },
        },
        checkout: {
          byid: {
            alias: alias('notes'),
            description: 'Notes about the asset checkout.',
            type: 'string',
          },
          bytag: {
            alias: alias('notes'),
            description: 'Notes about the asset checkout.',
            type: 'string',
          },
        },
        create: {
          alias: alias('notes'),
          description: 'Notes for the hardware.',
          type: 'string',
        },
      },
    },
    licenses: {
      patch: {
        byid: {
          alias: alias('notes'),
          description: 'Update the notes about the license.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('notes'),
          description: 'Notes about the license.',
          type: 'string',
        },
      },
    },
    models: {
      patch: {
        byid: {
          alias: alias('notes'),
          description: 'Update the notes about the model.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('notes'),
          description: 'Notes about the model.',
          type: 'string',
        },
      },
    },
    maintenances: {
      post: {
        create: {
          alias: alias('notes'),
          description: 'Notes about the model.',
          type: 'string',
        },
      },
    },
    statuslabels: {
      patch: {
        byid: {
          alias: alias('notes'),
          description: 'Update the notes about the status label.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('notes'),
          description: 'Notes about the status label.',
          type: 'string',
        },
      },
    },
    users: {
      patch: {
        byid: {
          alias: alias('notes'),
          description: 'Update the notes about the user.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('notes'),
          description: 'Notes about the user.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = notes
