/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for model_id.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const model_id = (commands) => {
  const { section, method } = commands

  if (section === 'hardware') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    hardware: {
      get: {
        search: {
          alias: alias('model_id'),
          description:
            'Optionally restrict the hardware results to this model ID.',
          type: 'number',
        },
      },
      patch: {
        byid: {
          alias: alias('model_id'),
          description: 'Update the ID of the associated hardware model ID.',
          type: 'number',
        },
        bytag: {
          alias: alias('model_id'),
          description: 'Update the ID of the associated hardware model ID.',
          type: 'number',
        },
        byserial: {
          alias: alias('model_id'),
          description: 'Update the ID of the associated hardware model ID.',
          type: 'number',
        },
      },
      post: {
        create: {
          alias: alias('model_id'),
          description: 'The ID of the associated hardware model ID.',
          required: true,
          type: 'number',
        },
      },
    },
  }
}

module.exports = model_id
