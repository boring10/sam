/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for api_key.
 *
 * @param {object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const api_key = (commands) => {
  const { section, method } = commands

  if (section === 'config') {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {object}
 */
function optionsObj() {
  return {
    config: {
      profiles: {
        create: {
          alias: alias('api_key'),
          description:
            'The API key to authenticate with your instance of Snipe-IT.',
          demandOption: true,
          type: 'string',
        },
        update: {
          alias: alias('api_key'),
          description:
            'The API key to authenticate with your instance of Snipe-IT.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = api_key
