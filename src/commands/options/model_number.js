/**
 * @namespace CommandOptions
 */
const alias = require('../../libs/optionAliases')

/**
 * @memberof CommandOptions
 * @description Command option for model_number.
 *
 * @param {Object} commands Object of commands that were entered.
 * @param {string} commands.section The root command that was entered.
 * @param {string} commands.method HTTP method request that was entered.
 *
 * @returns {object}
 */
const model_number = (commands) => {
  const { section, method } = commands

  if (
    section === 'accessories' ||
    section === 'consumables' ||
    section === 'models'
  ) {
    let { command1 } = commands

    return optionsObj()[section][method][command1]
  }

  return optionsObj()[section][method]
}

/**
 * @private
 * @description Options that are available for the specified section and method.
 *
 * @returns {Object}
 */
function optionsObj() {
  return {
    accessories: {
      patch: {
        byid: {
          alias: alias('model_number'),
          description: 'Update the model number of accessory.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('model_number'),
          description: 'Model number of accessory.',
          type: 'string',
        },
      },
    },
    consumables: {
      patch: {
        byid: {
          alias: alias('model_number'),
          description: 'Update the model number of consumable.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('model_number'),
          description: 'Model number of consumable.',
          type: 'string',
        },
      },
    },
    models: {
      patch: {
        byid: {
          alias: alias('model_number'),
          description: 'Update the model number for the model.',
          type: 'string',
        },
      },
      post: {
        create: {
          alias: alias('model_number'),
          description: 'Model number for the model.',
          type: 'string',
        },
      },
    },
  }
}

module.exports = model_number
