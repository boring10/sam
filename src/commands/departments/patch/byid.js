const axios = require('axios')

const CommandOptions = require('../../../libs/commandOptions')
const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')

exports.command = 'byid <id>'
exports.description = 'Update a department.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('company_id', commandOptions.option('company_id'))
    .option('location_id', commandOptions.option('location_id'))
    .option('manager_id', commandOptions.option('manager_id'))
    .option('name', commandOptions.option('name'))
    .middleware([generateConfig, appendToConfigUrl.id])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data
    const payload = data.payload

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully updated the department with the ID "${payload.id}" named "${payload.name}".`
    )
  } catch (err) {
    log.error(err)
  }
}
