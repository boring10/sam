exports.command = 'locations <method>'
exports.description = 'Manage your locations.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods')
    .commandDir('locations')
}
