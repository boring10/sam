const csv = require('csv-parser')
const { spawn } = require('child_process')
const fs = require('fs')

exports.command = 'csv <filename>'
exports.description = 'Select the CSV file that will be used.'
exports.builder = function (yargs) {
  return yargs
}
exports.handler = async function (argv) {
  let activeProfile = Object.keys(argv.activeProfile)[0]
  let apiThrottle = argv.activeProfile[activeProfile].api_throttle
  let results = []
  const filename = argv.options.filename
  delete argv.options.filename

  fs.createReadStream(filename)
    .pipe(csv())
    .on('data', (data) => {
      // Date: 2021-04-15
      // Parse and remove leading/trailing white space due to the Snipe-IT API
      // for some values.
      //
      // It is common that the last row may be blank and it can be removed when
      // there is not value to help reduce errors. It appears to have a blank row
      // when the column "Purchase Cost" is hidden, otherwise, the total will be
      // displayed in the last row.
      let obj = {}

      let emptyValue = true
      Object.keys(data).forEach((key) => {
        obj[key.trim()] = data[key].trim()

        if (obj[key] !== '') {
          emptyValue = false
        }
      })

      if (!emptyValue) {
        results.push(obj)
      }
    })
    .on('end', () => {
      let commandList = []
      results.forEach((row) => {
        // process.argv[1] - snapshot path
        let commands = [process.argv[1]]
        argv.commands.forEach((command) => {
          if (command.toString().includes('~~')) {
            let replacement = `${command.replace(/~~/g, '')}`
            commands.push(row[replacement])
          } else {
            commands.push(command)
          }
        })
        Object.keys(argv.options).forEach((key) => {
          commands.push(`--${key}`)
          if (argv.options[key].toString().includes('~~')) {
            let replacement = argv.options[key].replace(/~~/g, '')
            commands.push(row[replacement])
          } else {
            commands.push(argv.options[key])
          }
        })
        commandList.push(commands)
      })
      // process.argv[0] - path to the executable file.
      runCmd(process.argv[0], commandList, apiThrottle)
    })
}

// https://stackoverflow.com/questions/53144401/limit-the-number-of-concurrent-child-processes-spawned-in-a-loop-in-node-js
function runCmd(method, cmds, maxAtOnce) {
  let numRunning = 0
  let index = 0

  function runMore() {
    while (numRunning < maxAtOnce && index < cmds.length) {
      ++numRunning

      let sam = spawn(method, cmds[index++])

      sam.stdout.on('data', (data) => {
        console.log(`${data}`)
      })

      sam.stderr.on('data', (data) => {
        console.log(`stderr: ${data}`)

        --numRunning
        runMore()
      })

      sam.on('close', () => {
        --numRunning
        runMore()
      })
    }
    if (numRunning === 0) {
    }
  }

  runMore()
}
