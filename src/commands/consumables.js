exports.command = 'consumables <method>'
exports.description = 'Manage your consumables.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods')
    .commandDir('consumables')
}
