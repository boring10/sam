exports.command = 'companies <method>'
exports.description = 'Manage your companies.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods')
    .commandDir('companies')
}
