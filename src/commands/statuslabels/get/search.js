const axios = require('axios')

const CommandOptions = require('../../../libs/commandOptions')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')
const outputDisplay = require('../../../libs/outputDisplay')
const getAll = require('../../../libs/getAll')

exports.command = 'search [search]'
exports.description =
  'Search for the status labels that you would like to retrieve.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('all', commandOptions.option('all'))
    .option('export_to_file', commandOptions.option('export_to_file'))
    .option('fields', commandOptions.option('fields'))
    .option('limit', commandOptions.option('limit'))
    .option('offset', commandOptions.option('offset'))
    .option('order', commandOptions.option('order'))
    .option('output', commandOptions.option('output'))
    .option('sort', commandOptions.option('sort'))
    .strict((enabled = true))
    .middleware([generateConfig])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  const fields = argv.config.data.fields || false

  if (!argv.options.all) {
    try {
      const response = await axios(argv.config)
      const data = response.data

      if (data.status === 'error') {
        log.error(data.messages)

        return
      }

      console.log(
        outputDisplay(
          data,
          argv.config.data.output,
          fields,
          argv.config.data.export_to_file
        )
      )
    } catch (error) {
      log.error(error.response)
    }
  } else {
    getAll(argv.config, fields)
  }
}
