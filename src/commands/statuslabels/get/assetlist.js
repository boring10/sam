exports.command = 'assetlist <command>'
exports.description = 'Get the assets with a specific status label.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('assetlist')
}
