const axios = require('axios')

const CommandOptions = require('../../../libs/commandOptions')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')

exports.command = 'create'
exports.description = 'Create a new maintenance.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('color', commandOptions.option('color'))
    .option('default_label', commandOptions.option('default_label'))
    .option('name', commandOptions.option('name'))
    .option('notes', commandOptions.option('notes'))
    .option('show_in_nav', commandOptions.option('show_in_nav'))
    .option('type', commandOptions.option('type'))
    .middleware([generateConfig])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data
    const payload = data.payload

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    let type
    if (payload.deployable) type = 'deployable'
    else if (payload.archived) type = 'archived'
    else if (payload.pending) type = 'pending'
    else type = 'undeployable'

    log.response(
      `Successfully created the status label "${payload.name}" with the ID "${payload.id}" with the status type of "${type}".`
    )
  } catch (err) {
    log.error(err)
  }
}
