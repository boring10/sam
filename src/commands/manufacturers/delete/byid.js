const axios = require('axios')

const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')

exports.command = 'byid <id>'
exports.description = 'Delete a specific manufacturer by the ID.'
exports.builder = function (yargs) {
  return yargs
    .strict((enabled = true))
    .middleware([generateConfig, appendToConfigUrl.id])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully deleted the manufacturer with the ID of "${response.config.url}".`
    )
  } catch (error) {
    log.error(error.response.data)
  }
}
