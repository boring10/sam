const axios = require('axios')

const CommandOptions = require('../../../libs/commandOptions')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')

exports.command = 'create'
exports.description = 'Create a new manufacturer.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('name', commandOptions.option('name'))
    .option('support_email', commandOptions.option('support_email'))
    .option('support_phone', commandOptions.option('support_phone'))
    .option('support_url', commandOptions.option('support_url'))
    .option('url', commandOptions.option('url'))
    .middleware([generateConfig])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data
    const payload = data.payload

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully created a manufacturer with the ID "${payload.id}" named "${payload.name}".`
    )
  } catch (err) {
    log.error(err)
  }
}
