const axios = require('axios')

const CommandOptions = require('../../../libs/commandOptions')
const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')

exports.command = 'byid <id>'
exports.description =
  'Partially update a manufacturer, passing only the fields you want to modify.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('name', commandOptions.option('name'))
    .option('support_email', commandOptions.option('support_email'))
    .option('support_phone', commandOptions.option('support_phone'))
    .option('support_url', commandOptions.option('support_url'))
    .option('url', commandOptions.option('url'))
    .middleware([generateConfig, appendToConfigUrl.id])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    let response = await axios(argv.config)
    let data = response.data
    const payload = data.payload

    log.response(
      `Successfully updated the manufacturer with the ID "${payload.id}" named "${payload.name}".`
    )
  } catch (err) {
    console.log(err)
  }
}
