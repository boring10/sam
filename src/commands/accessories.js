exports.command = 'accessories <method>'
exports.description = 'Manage your accessories.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods')
    .commandDir('accessories')
}
