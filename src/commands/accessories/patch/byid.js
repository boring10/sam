const CommandOptions = require('../../../libs/commandOptions')
const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const accessoriesHandlers = require('../../../yargsHandlers/accessoriesHandlers')

exports.command = 'byid <id>'
exports.description = 'Update an existing accessory.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('category_id', commandOptions.option('category_id'))
    .option('company_id', commandOptions.option('company_id'))
    .option('location_id', commandOptions.option('location_id'))
    .option('manufacturer_id', commandOptions.option('manufacturer_id'))
    .option('min_amt', commandOptions.option('min_amt'))
    .option('model_number', commandOptions.option('model_number'))
    .option('name', commandOptions.option('name'))
    .option('order_number', commandOptions.option('order_number'))
    .option('purchase_cost', commandOptions.option('purchase_cost'))
    .option('purchase_date', commandOptions.option('purchase_date'))
    .option('qty', commandOptions.option('qty'))
    .option('supplier_id', commandOptions.option('supplier_id'))
    .middleware([generateConfig, appendToConfigUrl.id])
}
exports.handler = async function (argv) {
  accessoriesHandlers.patchByid(argv)
}
