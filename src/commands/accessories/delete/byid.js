const CommandOptions = require('../../../libs/commandOptions')
const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const accessoriesHandlers = require('../../../yargsHandlers/accessoriesHandlers')

exports.command = 'byid <id>'
exports.description = 'Delete a specific accessory by the ID.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('fields', commandOptions.option('fields'))
    .option('output', commandOptions.option('output'))
    .strict((enabled = true))
    .middleware([generateConfig, appendToConfigUrl.id])
}
exports.handler = async function (argv) {
  accessoriesHandlers.deleteByid(argv)
}
