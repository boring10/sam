const CommandOptions = require('../../../libs/commandOptions')
const generateConfig = require('../../../middleware/generateConfig')
const accessoriesHandlers = require('../../../yargsHandlers/accessoriesHandlers')

exports.command = 'search [search]'
exports.description =
  'Search for the accessories that you would like to retrieve.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('all', commandOptions.option('all'))
    .option('expand', commandOptions.option('expand'))
    .option('export_to_file', commandOptions.option('export_to_file'))
    .option('fields', commandOptions.option('fields'))
    .option('limit', commandOptions.option('limit'))
    .option('offset', commandOptions.option('offset'))
    .option('order', commandOptions.option('order'))
    .option('order_number', commandOptions.option('order_number'))
    .option('output', commandOptions.option('output'))
    .option('sort', commandOptions.option('sort'))
    .strict((enabled = true))
    .middleware([generateConfig])
}
exports.handler = async function (argv) {
  accessoriesHandlers.getSearch(argv)
}
