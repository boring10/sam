const axios = require('axios')

const CommandOptions = require('../../../../libs/commandOptions')
const appendToConfigUrl = require('../../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../../middleware/generateConfig')
const log = require('../../../../libs/log')

exports.command = 'byid <id>'
exports.description = 'Checkout an asset by the asset ID.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .option('assigned_to', commandOptions.option('assigned_to'))
    .option('note', commandOptions.option('note'))
    .strict((enabled = true))
    .middleware([generateConfig, appendToConfigUrl.idBeforeLastCommand])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully checked out an accessory to a user with the ID of "${argv.config.data.assigned_to}".`
    )
  } catch (err) {
    log.error(err)
  }
}
