exports.command = 'checkout <command>'
exports.description = 'Checkout an accessory to a user.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('checkout')
}
