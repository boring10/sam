exports.command = 'checkin <command>'
exports.description = 'Checkin an accessory from a user.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('checkin')
}
