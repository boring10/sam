const appendToConfigUrl = require('../../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../../middleware/generateConfig')
const accessoriesHandlers = require('../../../../yargsHandlers/accessoriesHandlers')

exports.command = 'bypivotid <id>'
exports.description =
  'Check in an accessory using the assigned pivot ID. This is different from the accessory ID and the user ID. The ID has the fieldname "assigned_pivot_id".'
exports.builder = function (yargs) {
  return yargs
    .strict((enabled = true))
    .middleware([generateConfig, appendToConfigUrl.idBeforeLastCommand])
}
exports.handler = async function (argv) {
  accessoriesHandlers.postCheckinBypivotid(argv)
}
