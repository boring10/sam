exports.command = 'checkout <command>'
exports.description = 'Checkout a license to a user or asset.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('checkout')
}
