const axios = require('axios')

const CommandOptions = require('../../../libs/commandOptions')
const appendToConfigUrl = require('../../../middleware/appendToConfigUrl')
const generateConfig = require('../../../middleware/generateConfig')
const log = require('../../../libs/log')

exports.command = 'byid <id>'
exports.description = 'Update an existing accessory.'
exports.builder = function (yargs) {
  const commandOptions = new CommandOptions(yargs.getContext().commands)

  return yargs
    .options('category_id', commandOptions.option('category_id'))
    .option('company_id', commandOptions.option('company_id'))
    .option('expiration_date', commandOptions.option('expiration_date'))
    .option('license_email', commandOptions.option('license_email'))
    .option('license_name', commandOptions.option('license_name'))
    .option('maintained', commandOptions.option('maintained'))
    .option('manufacturer_id', commandOptions.option('manufacturer_id'))
    .option('name', commandOptions.option('name'))
    .option('notes', commandOptions.option('notes'))
    .option('order_number', commandOptions.option('order_number'))
    .option('purchase_cost', commandOptions.option('purchase_cost'))
    .option('purchase_date', commandOptions.option('purchase_date'))
    .option('purchase_order', commandOptions.option('purchase_order'))
    .option('reassignable', commandOptions.option('reassignable'))
    .option('seats', commandOptions.option('seats'))
    .option('serial', commandOptions.option('serial'))
    .option('supplier_id', commandOptions.option('supplier_id'))
    .option('termination_date', commandOptions.option('termination_date'))
    .middleware([generateConfig, appendToConfigUrl.id])
}
exports.handler = async function (argv) {
  if (argv.error) {
    log.error(argv.error)

    return
  }

  try {
    const response = await axios(argv.config)
    const data = response.data
    const payload = data.payload

    if (data.status === 'error') {
      log.error(data.messages)

      return
    }

    log.response(
      `Successfully updated a license with the ID "${payload.id}" named "${payload.name}" with "${payload.seats}" seats.`
    )
  } catch (err) {
    log.error(err)
  }
}
