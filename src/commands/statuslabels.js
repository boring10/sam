exports.command = 'statuslabels <method>'
exports.description = 'Manage your status labels.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods')
    .commandDir('statuslabels')
}
