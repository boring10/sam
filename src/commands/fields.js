exports.command = 'fields <method>'
exports.description = 'Manage your custom fields.'
exports.builder = function (yargs) {
  return yargs
    .strictCommands((enabled = true))
    .demandCommand(1)
    .commandDir('./http-methods', {
      exclude: function (path) {
        if (
          /delete\.js$/.test(path) ||
          /patch\.js$/.test(path) ||
          /post\.js$/.test(path)
        ) {
          return true
        } else {
          return false
        }
      },
    })
    .commandDir('fields')
}
