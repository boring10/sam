#!/usr/bin/env node
/**
 * Ensure that the required files are setup.
 */
const path = require('path')
require('dotenv').config({ path: path.join(__dirname, '../.env') })
require('../middleware/initSetup')()

const formatArgs = require('../middleware/formatArgs')
const checkForUpdates = require('../middleware/checkForUpdates')
const activeProfile = require('../middleware/activeProfile')
const stopProcess = require('../middleware/stopProcess')

const log = require('../libs/log')

const yargs = require('yargs')
  .scriptName('sam')
  .commandDir('../commands')
  .completion(
    'completion',
    'Generate a completion script (Linux/macOS only (WSL should work as well)) that can be used with ".bashrc", ".bash_profile", ".zshrc", or "zsh_profile" file.'
  )
  .parserConfiguration({
    'boolean-negation': true,
    'sort-commands': true,
  })
  .middleware([checkForUpdates, formatArgs.format, activeProfile, stopProcess])
  .example([
    [
      '$0 config profiles create --name "profilename" --active --url "https://domain.com" --api_key "apiKeyHere"',
      'New profile creation.',
    ],
    [
      '$0 hardware post audit bytag --notes "Everything looks good!" --next_audit_date "2021-12-31"',
      'Audit the asset and manually add the next audit date that is different from the automatic next audit date timeframe.',
    ],
    [
      '$0 hardware get search --limit 3 --fields "id,name,asset_tag,serial,model.name,model.id" --model_id 1 --output csv',
      'Return the first 3 results that have the model ID of "1" in a CSV style output limiting the fields.',
    ],
    [
      '$0 hardware patch byid 1 --expected_checkin "2021-05-20"',
      'Change the expected checkin date for the device without having to check it in and back out. This does not notify the user though.',
    ],
  ])
  .epilogue('More information may be found at https://samcli.app')

yargs.wrap(yargs.terminalWidth()).help().argv
