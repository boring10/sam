#!/bin/sh

BIN_DIR="bin"
PKG_DIR="pkg"

PKG_VERSION=$(node -pe "require('./package.json').version")

# arg1 = filename
linkAsset() {
  curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
    --upload-file "bin/${1}" "${PACKAGE_REGISTRY_URL}/${PKG_VERSION}/${1}"

  PACKAGE_ID=$(curl --header "PRIVATE-TOKEN: ${KEY}" \
    "${PACKAGE_ARRAY_URL}?pagination=keyset&sort=desc&per_page=1" \
    | jq ".[]" \
    | jq -r ".id")

  FILE_ID=$(curl --header "PRIVATE-TOKEN: ${KEY}" \
    "${PACKAGE_ARRAY_URL}/${PACKAGE_ID}/package_files?pagintation=keyset" \
    | jq ".[] | select(.file_name==\"${1}\")" \
    | jq -r ".id")

  curl --header "PRIVATE-TOKEN: ${KEY}" \
    --data "name=${1}" \
    --data "url=${CI_PROJECT_URL}/-/package_files/${FILE_ID}/download" \
    "${RELEASE_REGISTRY_URL}/${CI_PROJECT_ID}/releases/v${PKG_VERSION}/assets/links"
}

npm install

mkdir $BIN_DIR $PKG_DIR

cp src/ $PKG_DIR/ -r
cp package.json $PKG_DIR/
cd $PKG_DIR
find . -type f -name '*.test.js' -delete
rm -rf ./tests/

npm install --production

cd ..

if [[ $NEW_RELEASE == "true" ]]; then
  if [[ ${1} == "x64" ]]; then
    PKG_PREFIX="${PKG_NAME}-x64-${PKG_VERSION}"
    LINUXx64="${PKG_PREFIX}-${LINUX_PKG}"
    MACOSx64="${PKG_PREFIX}-${MACOS_PKG}"
    WINx64="${PKG_PREFIX}-${WIN_PKG}"

    npm run pkgx64

    linkAsset $LINUXx64
    linkAsset $MACOSx64
    linkAsset $WINx64
  fi
fi
