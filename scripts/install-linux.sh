#!/usr/bin/env bash

#---------------------------------------------
# Variables
#---------------------------------------------
GITLAB_API="https://gitlab.com/api/v4/projects"
PROJECT_ID="25296645"
TARGET_DIR="$HOME/bin/sam"
TARGET_NAME="sam"

showLogo() {
echo "   _____              __  __"
echo "  / ____|     /\     |  \/  |"
echo " | (___      /  \    | \  / |"
echo "  \___ \    / /\ \   | |\/| |"
echo "  ____) |  / ____ \  | |  | |"
echo " |_____/  /_/    \_\ |_|  |_|"
echo ""
echo -e "Linux/macOS Installer and Updater\n"
}

msgRequiredPackages() {
  echo -e "\nPlease ensure that you have the packages \"jq\" and \"wget\" installed on your computer before trying again."

  ec=$?; return $ec 2>/dev/null || exit $ec
}

showLogo

#---------------------------------------------
# Check for required packages for install
#---------------------------------------------
STATUS_INSTALLED="Status: install ok installed"
WGET=$(dpkg -s wget | grep "$STATUS_INSTALLED")
JQ=$(dpkg -s jq | grep "$STATUS_INSTALLED")

if [[ ! $WGET == $STATUS_INSTALLED || ! $JQ == $STATUS_INSTALLED ]]; then
  msgRequiredPackages
fi

#---------------------------------------------
# Query for the latest version to install
#---------------------------------------------
URL="${GITLAB_API}/${PROJECT_ID}/releases?pagination=keyset&sort=desc&per_page=1"
REQ=$(wget -qO - "$URL" | jq)
LATEST=$(echo $REQ | jq ".[].tag_name" | sed "s/v//" | sed "s/\"//g")

#---------------------------------------------
# Get the installed version, if available
#---------------------------------------------
if [[ -f $TARGET_DIR/$TARGET_NAME ]]; then
  VERSION=$($TARGET_DIR/./$TARGET_NAME --version)
else
  mkdir -p $TARGET_DIR
fi

#---------------------------------------------
# User script file
#---------------------------------------------
# User argument for the specified script file. (ie. .zshrc)
if [[ ! -z ${1} ]]; then
  OS=$(uname -v | grep "Darwin")
  if [[ ! -z $OS ]]; then
    SHELL_RC="$HOME/.zsh_profile"
  else
    SHELL_RC="$HOME/${1}"
  fi
else
  SHELL_RC="$HOME/.bashrc"
fi

#---------------------------------------------
# Append to script file for path and auto complete
#---------------------------------------------
EXISTS=$(cat $SHELL_RC | grep "# Begin SAM Path and Completion #")
if [[ ! $EXISTS == "# Begin SAM Path and Completion #" ]]; then
  cat >> $SHELL_RC << EOF

# Begin SAM Path and Completion #
export PATH="$TARGET_DIR:\$PATH"

# Prevent autocorrect issues with ZSH
alias sam="nocorrect sam"

# yargs command completion script
_sam_yargs_completions()
{
  local reply
  local si=\$IFS
  IFS=$'
' reply=(\$(COMP_CWORD="\$((CURRENT-1))" COMP_LINE="\$BUFFER" COMP_POINT="\$CURSOR" sam --get-yargs-completions "\${words[@]}"))
  IFS=\$si
  _describe 'values' reply
}
compdef _sam_yargs_completions sam
# End SAM Path and Completion   #
EOF
fi

#---------------------------------------------
# Download the latest version, if needed
#---------------------------------------------
if [[ $LATEST == $VERSION ]]; then
  echo "You appear to already have the latest version (v$LATEST) installed."
else
  TEMP_DIR=$(mktemp -d)

  FILE_NAME=$(echo $REQ | jq ".[].assets.links" | jq ".[].name" | grep "linux" | sed "s/\"//g")
  DIRECT_ASSET_URL=$(echo $REQ | jq ".[].assets.links" | jq ".[] | select(.name==\"${FILE_NAME}\")" | jq ".direct_asset_url" | sed "s/\"//g")

  echo "Downloading v$LATEST..."
  wget -qnv --show-progress -O ${TEMP_DIR}/sam "$DIRECT_ASSET_URL"

  cp $TEMP_DIR/$TARGET_NAME $TARGET_DIR/
  chmod +x $TARGET_DIR/$TARGET_NAME

  #---------------------------------------------
  # Cleanup the tmp directory that was created
  #---------------------------------------------
  echo -e "\nCleaning up..."
  rm $TEMP_DIR -rf

  echo -e "\nVersion $LATEST has been installed!"
  echo -e "You may need to close and re-open your terminal before being able to use the \"sam\" command."

fi
