#!/bin/sh

$DIR=$HOME/Desktop/docs

if [ ! -d "$DIR" ]; then
  mkdir $DIR
fi

# Accessories
$FILE=$DIR/accessories.md

echo "# Accessories \n" >> $FILE

sam accessories --help

echo "" >> $FILE
echo "## Delete" >> $FILE
echo "" >> $FILE

sam accessories delete --help

echo "" >> $FILE
echo "### Byid" >> $FILE
echo "" >> $FILE

sam accessories delete byid --help
