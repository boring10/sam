#!/usr/bin/env bash

# Source: https://stackoverflow.com/a/246128/12066662
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

BIN_DIR="bin"
PKG_DIR="pkg"
ARCH=$(uname -m)

showLogo() {
echo "   _____              __  __"
echo "  / ____|     /\     |  \/  |"
echo " | (___      /  \    | \  / |"
echo "  \___ \    / /\ \   | |\/| |"
echo "  ____) |  / ____ \  | |  | |"
echo " |_____/  /_/    \_\ |_|  |_|"
echo "Locally building the packages."
echo ""
}

showLogo

npm install

[[ -d $BIN_DIR ]] && rm $BIN_DIR -rf
mkdir $BIN_DIR

[[ -d $PKG_DIR ]] && rm $PKG_DIR -rf

mkdir $PKG_DIR

cp src/ $PKG_DIR/ -r
cp package.json $PKG_DIR/
cd $PKG_DIR
find . -type f -name '*.test.js' -delete
rm -rf ./tests/
npm install --production
cd ..

if [ $ARCH == "x86_64" ]; then
  if [[ ${1} == "dev" ]]; then
    npm run dev
  else
    npm run pkgx64
  fi
else
  npm run pkg
fi

rm $PKG_DIR -rf
