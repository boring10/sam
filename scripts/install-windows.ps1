#---------------------------------------------
# Variables
#---------------------------------------------
$GITLAB_API="https://gitlab.com/api/v4/projects"
$PROJECT_ID="25296645"
$TARGET_DIR="$HOME\bin\sam"
$TARGET_NAME="sam.exe"

function showLogo {
    Write-Host "   _____              __  __"
    Write-Host "  / ____|     /\     |  \/  |"
    Write-Host " | (___      /  \    | \  / |"
    Write-Host "  \___ \    / /\ \   | |\/| |"
    Write-Host "  ____) |  / ____ \  | |  | |"
    Write-Host " |_____/  /_/    \_\ |_|  |_|"
    Write-Host ""
    Write-Host "Windows Installer and Updater"
    Write-Host ""
}

showLogo

#---------------------------------------------
# Query for the latest version to install
#---------------------------------------------
$URL="${GITLAB_API}/${PROJECT_ID}/releases?pagination=keyset&sort=desc&per_page=1"
$WEB_REQUEST=Invoke-WebRequest -Uri $URL -UseBasicParsing
$CONTENT=$WEB_REQUEST.content | ConvertFrom-Json
$LATEST=($CONTENT.tag_name).replace("v", "")

#---------------------------------------------
# Get the installed version, if available
#---------------------------------------------
if ( Test-Path -Path "$TARGET_DIR" ) {
    if ( Test-Path -Path "$TARGET_DIR\$TARGET_NAME" ) {
        $VERSION=sam --version
        Write-Host "$VERSION"
    } else {
        $VERSION=0
        Write-Host "SAM does not appear to be installed on this system."
        Write-Host ""
    }
} else {
    New-Item -ItemType "Directory" -Path $TARGET_DIR | Out-Null
    Write-Host "SAM does not appear to be installed on this system."
    Write-Host ""
}

if ( -Not (Test-Path -Path $profile)) {
    Write-Host "You currently do not have a `"Microsoft.PowerShell_profile.ps1`" file. Creating it now."
    New-Item -ItemType "File" -Path $profile -force | Out-Null
}

If ( -Not ((Get-ExecutionPolicy -Scope CurrentUser) -eq "RemoteSigned") ) {
    Write-Host "Setting the execution policy to allow `"RemoteSigned`" scripts to be executed for the current user."
    Set-ExecutionPolicy RemoteSigned -Scope CurrentUser -Force
}

#---------------------------------------------
# Append the path if it doesn't already exist
#---------------------------------------------
if ( -Not (Get-Content $profile | Where-Object { $_ -match "# Begin SAM Path #"} )) {
$SAM_PATH=@"


# Begin SAM Path #
`$env:Path += `";$TARGET_DIR`"
# End SAM Path   #
"@

    $SAM_PATH.replace("`r`n","`n") | Out-File -FilePath $profile -Append -Encoding ASCII
}

#---------------------------------------------
# Download the latest version, if needed
#---------------------------------------------
if ($LATEST -eq $VERSION) {
    Write-Host "You appear to already have the latest version (v$LATEST) installed."
} else {
    $TEMP_FILE=New-TemporaryFile

    $FILE_NAME=$CONTENT.assets.links | Where-Object { $_.name -match "win" } | ForEach-Object { $_.name }
    $DIRECT_ASSET_URL=$CONTENT.assets.links | Where-Object { $_.name -eq $FILE_NAME } | ForEach-Object { $_.direct_asset_url}

    Write-Host "Downloading v$LATEST..."

    $CURRENT_PROGRESS_PREFERENCE=$ProgressPreference
    $ProgressPreference='SilentlyContinue'
    Invoke-WebRequest -Uri $DIRECT_ASSET_URL -OutFile $TEMP_FILE.FullName -UseBasicParsing
    $ProgressPreference=$CURRENT_PROGRESS_PREFERENCE

    Copy-Item -Path $TEMP_FILE.FullName -Destination $TARGET_DIR/$TARGET_NAME

    #---------------------------------------------
    # Cleanup the tmp directory that was created
    #---------------------------------------------
    Remove-Item -Path $TEMP_FILE

    Write-Host ""
    Write-Host "Version $LATEST has been installed!"
    Write-Host "You may need to close and re-open your terminal before being able to use the `"sam`" command."
}
