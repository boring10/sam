#!/bin/sh

echo "NEW_RELEASE=true" >> build.env

VERSION="v$(node -pe "require('./package.json').version")"
SHIELD_BASEURL="https://img.shields.io/badge"
PROJECT_BASEURL="https://gitlab.com/boring10/sam/"

LICENSE_LINK="${PROJECT_BASEURL}/-/blob/main/LICENSE.md"
LICENSE_IMG="${SHIELD_BASEURL}/license-MIT-informational"
PLATFORM_LINK="${PROJECT_BASEURL}/-/releases"
PLATFORM_IMG="${SHIELD_BASEURL}/platform-linux%2064%20|%20osx%2064%20|%20win%2064-lightgrey"
RELEASE_LINK="${PROJECT_BASEURL}/-/releases/${VERSION}"
RELEASE_IMG="${SHIELD_BASEURL}/release-${VERSION}-success"

linkBadge() {
  BADGE_EXISTS=$(curl --header "PRIVATE-TOKEN: ${KEY}" \
    "${BADGES_URL}" \
    | jq ".[] | select(.name==\"${1}\")" \
    | jq -r ".id")

  if [[ ${1} == "release" ]]; then
    if [[ -z $BADGE_EXISTS ]]; then
      if [[ ${1} == "release" ]]; then
        curl --request POST --header "PRIVATE-TOKEN: ${KEY}" \
          "${BADGES_URL}" \
          --data "name=${1}" \
          --data "link_url=${RELEASE_LINK}" \
          --data "image_url=${RELEASE_IMG}"
      fi
    else
      curl --request PUT --header "PRIVATE-TOKEN: ${KEY}" \
        "${BADGES_URL}/${BADGE_EXISTS}" \
        --data "name=${1}" \
        --data "link_url=${RELEASE_LINK}" \
        --data "image_url=${RELEASE_IMG}"
    fi
  elif [[ ${1} == "license" ]]; then
    if [[ -z $BADGE_EXISTS ]]; then
      curl --request POST --header "PRIVATE-TOKEN: ${KEY}" \
        "${BADGES_URL}" \
        --data "name=${1}" \
        --data "link_url=${LICENSE_LINK}" \
        --data "image_url=${LICENSE_IMG}"
    fi
  elif [[ ${1} == "platform" ]]; then
    if [[ -z $BADGE_EXISTS ]]; then
      curl --request POST --header "PRIVATE-TOKEN: ${KEY}" \
        "${BADGES_URL}" \
        --data "name=${1}" \
        --data "link_url=${PLATFORM_LINK}" \
        --data "image_url=${PLATFORM_IMG}"
    fi
  fi
}

linkBadge "license"
linkBadge "platform"
linkBadge "release"
