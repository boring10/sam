$PROJECT_DIR="$PSScriptRoot\.."
$BIN_DIR="bin"
$PKG_DIR="pkg"

Set-Location -Path "$PROJECT_DIR"

function showLogo {
Write-Host "   _____              __  __"
Write-Host "  / ____|     /\     |  \/  |"
Write-Host " | (___      /  \    | \  / |"
Write-Host "  \___ \    / /\ \   | |\/| |"
Write-Host "  ____) |  / ____ \  | |  | |"
Write-Host " |_____/  /_/    \_\ |_|  |_|"
Write-Host "Locally building the packages."
Write-Host ""
}

showLogo

npm install

if (Test-Path -Path "$PROJECT_DIR/$BIN_DIR") {
    Remove-Item -Path "$PROJECT_DIR/$BIN_DIR" -Recurse -ErrorAction Ignore
}

if (Test-Path -Path "$PROJECT_DIR/$PKG_DIR") {
    Remove-Item -Path "$PKG_DIR" -Recurse -ErrorAction Ignore
}

New-Item -ItemType "Directory" -Name "$BIN_DIR" -Path "$PROJECT_DIR"
New-Item -ItemType "Directory" -Name "$PKG_DIR" -Path "$PROJECT_DIR"

Copy-Item -Path "$PROJECT_DIR/src/" -Destination "$PROJECT_DIR/$PKG_DIR" -Recurse
Copy-Item -Path "$PROJECT_DIR/package.json" -Destination "$PROJECT_DIR/$PKG_DIR"

Set-Location -Path "$PROJECT_DIR/$PKG_DIR"
Remove-Item -Path "$PROJECT_DIR/tests" -Recurse -ErrorAction Ignore

Get-ChildItem -Path "$PROJECT_DIR/$PKG_DIR" -File -Include "*.test.js" -Recurse | Remove-Item -Force

npm install --production
Set-Location -Path "$PROJECT_DIR"
npm run pkgx64-win

Remove-Item -Path "$PROJECT_DIR/$PKG_DIR" -Recurse -ErrorAction Ignore
