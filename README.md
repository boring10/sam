# SAM (Snipe-IT Asset Manager)

**DISCLAIMER:** ***This is not an official [Snipe-IT](https://snipeitapp.com) tool***

> *SAM is still a work in progress and new commands/features are being added. See commands in the menu for the current available commands and how to use them.*

More documentation can be found at <https://samcli.app>.

``` bash
# Update/Install - See beloew for more information about installation

# BASH
wget -qO - "https://gitlab.com/api/v4/projects/25296645/repository/files/scripts%2Finstall-linux.sh/raw?ref=main" | bash -

# ZSH
wget -qO - "https://gitlab.com/api/v4/projects/25296645/repository/files/scripts%2Finstall-linux.sh/raw?ref=main" | bash -s ".zshrc"

# PowerShell
Invoke-Expression (Invoke-WebRequest -Uri "https://gitlab.com/api/v4/projects/25296645/repository/files/scripts%2Finstall-windows.ps1/raw?ref=main" -UseBasicParsing)
```

SAM is a command line tool that allows for the ability to manage your Snipe-IT instance. Please be aware that not everything can be managed through the CLI due to limitations of the Snipe-IT API.

SAM does not require installation in order to be used. At this time, the build process only has 64-bit versions that are available in the releases. You can run a build on your current system so that it is built with the architecture that is required.

**Download:** <https://gitlab.com/boring10/sam/-/releases>

Scripts are available that will automatically check to see if SAM has been "installed" and whether a newer version is available to be downloaded and installed (see [Installation](https://boring10.gitlab.io/sam/installation)).

If you are on Linux/macOS then you may need to modify the binary so that it is executable after downloading it. This is only the case if you download it manually, the installation (see [Installation](https://boring10.gitlab.io/sam/installation)) script will take care of this.

``` bash
# This will ensure that the downloaded file is set as an executable
chmod +x /path/to/file/sam-x64-1.0.0-linux
```

All commands offer a `help` option that will provide you with more information about the current command and what options, if any, are available.

``` bash
sam --help
> sam [command]
> 
> Commands:
>   sam accessories <method>    Manage your accessories.
>   sam categories <method>     Manage your categories.
>   sam companies <method>      Manage your companies.
>   sam completion              Generate a completion script (Linux/macOS only (WSL should work as well)) that can be used with ".bashrc", ".bash_profile", ".zshrc", or "zsh_profile" file.
>   sam components <method>     Manage your components.
>   sam config <command>        Manage the application configuration settings.
>   sam consumables <method>    Manage your consumables.
>   sam csv <filename>          Select the CSV file that will be used.
>   sam departments <method>    Manage your departments.
>   sam hardware <method>       Manage your assets.
>   sam licenses <method>       Manage your licenses.
>   sam locations <method>      Manage your locations.
>   sam maintenances <method>   Manage your asset maintenances.
>   sam manufacturers <method>  Manage your manufacturers.
>   sam models <method>         Manage your models.
>   sam statuslabels <method>   Manage your status labels.
>   sam users <method>          Manage your users.
> 
> Options:
>   --version  Show version number                                                                                                                                                                                                     [boolean]
>   --help     Show help                                                                                                                                                                                                               [boolean]
> 
> Examples:
>   sam config profiles create --name "profilename" --active --url "https://domain.com" --api_key "apiKeyHere"           New profile creation.
>   sam hardware post audit bytag --notes "Everything looks good!" --next_audit_date "2021-12-31"                        Audit the asset and manually add the next audit date that is different from the automatic next audit date timeframe.
>   sam hardware get search --limit 3 --fields "id,name,asset_tag,serial,model.name,model.id" --model_id 1 --output csv  Return the first 3 results that have the model ID of "1" in a CSV style output limiting the fields.
>   sam hardware patch byid 1 --expected_checkin "2021-05-20"                                                            Change the expected checkin date for the device without having to check it in and back out. This does not notify the user though.

More information may be found at https://samcli.app
```

## Initial Profile Setup

A profile needs to be created that will utilize your Snipe-IT API key to connect with your Snipe-IT instance (see [Profiles](https://boring10.gitlab.io/sam/commands/config/#profiles))

```bash
# Linux/macOS
sam config profiles create \
  --name "name of your profile" \
  --active \
  --url "https://domain.com" \
  --api_key "api key here"

# Windows
sam config profiles create `
  --name "name of your profile" `
  --active `
  --url "https://domain.com" `
  --api_key "api key here"
```

