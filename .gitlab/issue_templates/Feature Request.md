#### Feature Request

Please only submit one feature request at a time. If you have multiple feature requests please submit them separately.

**Is the feature request related to an existing problem? Describe.**\
Please be concise with the description.

**Describe a possible solution.**\
Please be concise with the solution.

**Additional context.**\
Expand upon the problem/possible solution, include screenshots, etc.
