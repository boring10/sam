# [2.0.0](https://gitlab.com/boring10/sam/compare/v1.14.1...v2.0.0) (2022-09-08)


### Bug Fixes

* **bytag options:** cleanup options and hardware handler to reduce errors ([196cf31](https://gitlab.com/boring10/sam/commit/196cf316e6484f3eb9233fbb4e0fe00bbbf39834))
* **exclude commands:** exclude commands that are not available from the list ([7e36b5a](https://gitlab.com/boring10/sam/commit/7e36b5ae457f0966b636c6007e833291b7b4d043))
* **exclude commands:** excludes commands that are not available from the list ([a8d6734](https://gitlab.com/boring10/sam/commit/a8d673400a2304f0e79ec16d2c802a46287464e4))


### Features

* **bytag commands:** add commands for bytag where api only has byid ([871edb2](https://gitlab.com/boring10/sam/commit/871edb203463db56ec13e63892f72b9a806a6476))
* **feature improvements:** better hardware command support and byserial ability ([d2d7762](https://gitlab.com/boring10/sam/commit/d2d7762bdb19263e35e3dbf786c590589e7565a7))
* **hardware patch byserial:** update hardware by using the serial number ([ad39de6](https://gitlab.com/boring10/sam/commit/ad39de62277ac210762f6dc9be16ee24f668bfe8))
* **outputdisplay:** return json output in an array of objects instead of a nested object ([87fdf01](https://gitlab.com/boring10/sam/commit/87fdf0126f4c19e31b7db206f21ba118f5421e83))
* **reports get activity:** command for getting activity reports ([ceb49c2](https://gitlab.com/boring10/sam/commit/ceb49c202d663b08efd2535455069d0bc4a8435d))
* **update options:** update options and unit test ([f1fe63e](https://gitlab.com/boring10/sam/commit/f1fe63e89a2ff047ff0ffcae1548702c6af5538e))


### BREAKING CHANGES

* **outputdisplay:** returns an array instead of an object

## [1.14.1](https://gitlab.com/boring10/sam/compare/v1.14.0...v1.14.1) (2021-08-26)


### Bug Fixes

* **users patch byid:** comment out an option that is not implemented yet ([f2c1649](https://gitlab.com/boring10/sam/commit/f2c1649c4e93868995ebf40b56247b6eba57ca44))

# [1.14.0](https://gitlab.com/boring10/sam/compare/v1.13.1...v1.14.0) (2021-08-25)


### Features

* **hardware patch bytag:** command to patch an asset by the asset tag ([1b0ee9e](https://gitlab.com/boring10/sam/commit/1b0ee9e908a0257a8d37866f2bb62ba4c7901e98))

## [1.13.1](https://gitlab.com/boring10/sam/compare/v1.13.0...v1.13.1) (2021-08-24)


### Bug Fixes

* **outputdisplay:** replace line breaks with spaces ([df483a2](https://gitlab.com/boring10/sam/commit/df483a20d0c9b7ee48d15adc077c4546386b70bd))
* **outputdisplay:** replace new line characters with spaces for csv output ([5e72259](https://gitlab.com/boring10/sam/commit/5e72259f002d2ea843c45e67ea5140f21e872260))

# [1.13.0](https://gitlab.com/boring10/sam/compare/v1.12.0...v1.13.0) (2021-08-24)


### Features

* **pkg version:** new pkg version allowing for smaller executable size ([0b6ad6b](https://gitlab.com/boring10/sam/commit/0b6ad6b3bb0c53d1626a8d96c9922fafcb73c0f7))

# [1.12.0](https://gitlab.com/boring10/sam/compare/v1.11.0...v1.12.0) (2021-08-24)


### Bug Fixes

* **add default limit:** default limit set to match the limit from Snipe-IT default ([49a9897](https://gitlab.com/boring10/sam/commit/49a9897286e1470122904c1f094bd940a2a9dd2f))


### Features

* **getall:** get all results for subcommands ([445588c](https://gitlab.com/boring10/sam/commit/445588ce0ddc18c9e268c07e1a43ee8f193b11d2))
* **getall:** get all results, more then 500 at a time ([6dae1c8](https://gitlab.com/boring10/sam/commit/6dae1c8378882cc51ee0534475f5592e6bd80d80))

# [1.11.0](https://gitlab.com/boring10/sam/compare/v1.10.0...v1.11.0) (2021-06-20)


### Features

* **statuslabels get/patch/delete:** commands for managing and retrieving status labels ([498a943](https://gitlab.com/boring10/sam/commit/498a943c3ccfa1525994c7041efe387c55d761be))
* **statuslabels post create:** command for creating new status labels ([4493e27](https://gitlab.com/boring10/sam/commit/4493e274c6859c806ed5e616e1c635425cd302e3))

# [1.10.0](https://gitlab.com/boring10/sam/compare/v1.9.1...v1.10.0) (2021-06-16)


### Features

* **consumables commands:** commands for managing consumables ([fc016ac](https://gitlab.com/boring10/sam/commit/fc016accc20fec47f1aa84f9ca948ea048995ed4))

## [1.9.1](https://gitlab.com/boring10/sam/compare/v1.9.0...v1.9.1) (2021-05-13)


### Bug Fixes

* **components get assets byid:** add missing command ([e30843f](https://gitlab.com/boring10/sam/commit/e30843f402aa06ce493d0865d460995b98032b4f))

# [1.9.0](https://gitlab.com/boring10/sam/compare/v1.8.1...v1.9.0) (2021-05-13)


### Features

* **accessories get checkedout byid:** add limit, offset, order, sort options for command ([b0c1e53](https://gitlab.com/boring10/sam/commit/b0c1e53ae358447efdccba792751d9c5c02add72))
* **components delete byid:** command for deleting an existing component by the ID ([9e28a4f](https://gitlab.com/boring10/sam/commit/9e28a4f8a5a6316abe8384b03c9fc9d8ef6bcf4f))
* **components get byid/search:** command to get existing components ([f31bb3f](https://gitlab.com/boring10/sam/commit/f31bb3f43bb36c78732f11277ad6c7749b081b02))
* **components patch byid:** command for updating existing components by the ID ([ce46835](https://gitlab.com/boring10/sam/commit/ce46835de1ba09ff5c951de9ba8b8dc253ce3ffa))
* **components post create:** command for creating new components ([f9a6858](https://gitlab.com/boring10/sam/commit/f9a68585425082e7f25e5f666f908131e7f5209e))
* **hardware get audit due:** add limit, offset, order, sort options for command ([f806c5a](https://gitlab.com/boring10/sam/commit/f806c5adf92b977a35b5c6c1bb1124e216cf7508))
* **licenses byid/ audit overdue:** add limit, offset, order, sort options ([4c807ec](https://gitlab.com/boring10/sam/commit/4c807ec2804c8f6f1d846be8a2a6804e05d92f39))
* **users get accessories byid:** add limit, offset, order, sort options ([f2e74c3](https://gitlab.com/boring10/sam/commit/f2e74c3b6780e0d4ccbfc042a1212c5199aea3a4))
* **users get assets byid:** add limit, offset, order, sort option ([2b23b84](https://gitlab.com/boring10/sam/commit/2b23b84626b7742fca297f45ef5dc3305884f5b5))
* **users get licenses byid:** add limit, offset, order, sort options ([a68f38b](https://gitlab.com/boring10/sam/commit/a68f38be2790409623ab005a4fa8637274b15261))

## [1.8.1](https://gitlab.com/boring10/sam/compare/v1.8.0...v1.8.1) (2021-04-27)


### Bug Fixes

* **checkforupdates:** check for valid date and default to current date if not ([f123c0a](https://gitlab.com/boring10/sam/commit/f123c0a0231ef2540dc7727220d6643e45ad6fb2))

# [1.8.0](https://gitlab.com/boring10/sam/compare/v1.7.0...v1.8.0) (2021-04-27)


### Bug Fixes

* **manufacturers get search:** correct description for command ([3974f69](https://gitlab.com/boring10/sam/commit/3974f699f248b3efb24aaf6c51a974c23478b4bc))


### Features

* **maintenances delete byid:** command for deleting an existing maintenance ([ffd0084](https://gitlab.com/boring10/sam/commit/ffd0084d1d74657c16b91fd1e22c6f6d7ed7db9d))
* **maintenances get byid/search:** commands for getting existing maintenances ([d3121f0](https://gitlab.com/boring10/sam/commit/d3121f0c60e8d7bfc2f7f1ecdf5826abddf063b3))
* **maintenances post create:** command for creating new maintenances ([e4b8a72](https://gitlab.com/boring10/sam/commit/e4b8a729dc55e1e55e25b9b12abad38cac06d742))

# [1.7.0](https://gitlab.com/boring10/sam/compare/v1.6.0...v1.7.0) (2021-04-20)


### Bug Fixes

* **models patch byid:** remove temporary console.log messages ([b35902b](https://gitlab.com/boring10/sam/commit/b35902bf9e734b3562c69a756f7e2e2f696ebd39))
* **requestable:** remove requestable option since API does not accept it at this time ([0ff2f98](https://gitlab.com/boring10/sam/commit/0ff2f98c092a4bc9bb3b57778a76ecedbe5b8349))


### Features

* **models delete byid:** command to delete a model by the ID ([b0604e4](https://gitlab.com/boring10/sam/commit/b0604e446e0cbb529709cd1552f94589d8e10085))
* **models get byid/search:** commands to get existing models information ([9ecdbbb](https://gitlab.com/boring10/sam/commit/9ecdbbb211af44bd18bbe26d0e5742b5eea34bab))
* **models patch/post:** commands for creating and editing models ([25ab3a5](https://gitlab.com/boring10/sam/commit/25ab3a54bcce4879e95e7b2ac21014361f42f699))

# [1.6.0](https://gitlab.com/boring10/sam/compare/v1.5.0...v1.6.0) (2021-04-19)


### Bug Fixes

* **command syntax:** change incorrect command syntax from optional to required ([d784e37](https://gitlab.com/boring10/sam/commit/d784e377c77c3e91551f678e9cf0d52c390ba999))
* **csv:** trim keys and values in csv file ([de438c4](https://gitlab.com/boring10/sam/commit/de438c4dc8356313a88b39c11da1cd7894a24df9))


### Features

* **license patch byid:** command for updating existing licenses ([b8082e0](https://gitlab.com/boring10/sam/commit/b8082e00707e4321c4383c7e9734a77c8f0ca1f6))
* **licenses delete byid:** command for deleting existing licenses ([7ea110e](https://gitlab.com/boring10/sam/commit/7ea110e8d99a6bd25922ff0c115121cc883d035b))
* **licenses get byid:** command for getting an existing license by the ID ([47e2f4b](https://gitlab.com/boring10/sam/commit/47e2f4bfb89a9849723144018fa311362c1343d6))
* **licenses get search:** command for searching existing licenses ([a746514](https://gitlab.com/boring10/sam/commit/a7465148d2e532120d09394b4e80c65f2abaf1ef))
* **licenses post create:** command for creating new licenses ([631b76b](https://gitlab.com/boring10/sam/commit/631b76b8986b3f133af5fedc972cca8e9906e027))
* **users delete byid:** command for deleting an existing user ([b160594](https://gitlab.com/boring10/sam/commit/b1605948153fed93ae7c846611aa640eabd7433d))
* **users get:** commands for getting users and checked out items ([820630f](https://gitlab.com/boring10/sam/commit/820630f8a5b7e949daa9b85f4aa7fac44b06f12e))
* **users get byid/search:** commands for getting/searching for users ([8ca9e84](https://gitlab.com/boring10/sam/commit/8ca9e841feb940c09bcd2882fa09cc7808ad7a53))
* **users patch byid:** command to update an existing user ([e19273a](https://gitlab.com/boring10/sam/commit/e19273a14b4c44716ce38786822dab9cc716b730))
* **users post create:** command to create a new user ([42a2675](https://gitlab.com/boring10/sam/commit/42a26755816ac44588a5e795706c556279503ced))

# [1.5.0](https://gitlab.com/boring10/sam/compare/v1.4.0...v1.5.0) (2021-04-13)


### Features

* **csv:** add csv command for pkg instead of unpkged only ([4942823](https://gitlab.com/boring10/sam/commit/4942823dfe4cd8da73d5c93688e9283d558053f3))

# [1.4.0](https://gitlab.com/boring10/sam/compare/v1.3.1...v1.4.0) (2021-04-12)


### Bug Fixes

* **option fields:** fix includes to handle entered fields for option better ([4887c25](https://gitlab.com/boring10/sam/commit/4887c256831a5e8c1aa556ac4ccee5698a34f5f9))


### Features

* **categories delete byid:** command to delete existing categories ([be68d59](https://gitlab.com/boring10/sam/commit/be68d5996e23cc23ffc4e138594bf035d4708999))
* **categories get byid/search:** commands for querying existing categories ([c035dd2](https://gitlab.com/boring10/sam/commit/c035dd24e2ddd05f71dc5b946ec428c6d5ea68ba))
* **categories post create:** command for creating new categories ([7ce5489](https://gitlab.com/boring10/sam/commit/7ce5489794192a489773300ab93ce20fae8f07b7))
* **category patch byid:** command for modifying existing categories by ID ([53fb35d](https://gitlab.com/boring10/sam/commit/53fb35d648c1169c250476dff3fdbf3e70927f29))
* **departments delete byid:** command to delete an existing department by the ID ([17886b8](https://gitlab.com/boring10/sam/commit/17886b8db6a54f34d6de07f4b8f9e8666cb68515))
* **departments delete byid:** command to delete an existing department by the ID ([d4a96ea](https://gitlab.com/boring10/sam/commit/d4a96ead422234b102f8787bdf5ecf25d30aeee4))
* **departments get byid:** command for getting a existing department by the ID ([38cca22](https://gitlab.com/boring10/sam/commit/38cca2287f8911e112cee018a5dfc6850162bdb9))
* **departments get byid:** command for getting a existing department by the ID ([5d006d4](https://gitlab.com/boring10/sam/commit/5d006d4517c107e57197063ee46db50410797457))
* **departments get search:** command for searching departments ([3cc775e](https://gitlab.com/boring10/sam/commit/3cc775e46a54ada4116d44e3f07395bfdc69886c))
* **departments get search:** command for searching departments ([73b7691](https://gitlab.com/boring10/sam/commit/73b7691026411204c4d8d45c692df9d5481e7753))
* **departments patch byid:** command to update existing departments by the department ID ([d7c0a51](https://gitlab.com/boring10/sam/commit/d7c0a5145ac2e6d137f533aab7426c05c4705ba1))
* **departments patch byid:** command to update existing departments by the department ID ([27b3677](https://gitlab.com/boring10/sam/commit/27b3677f5efd8b0cb24c86c53f3be97c4957e94b))
* **departments post create:** command for creating new departments ([92b11dd](https://gitlab.com/boring10/sam/commit/92b11dde8a18b85a1e295656125ef67723980c45))
* **departments post create:** command for creating new departments ([ceb3b2c](https://gitlab.com/boring10/sam/commit/ceb3b2c6bdd6057a88b09ca6216ed51b222ab42e))
* **manufacturers delete byid:** command to delete existing manufacturers by their ID ([910ede4](https://gitlab.com/boring10/sam/commit/910ede4fdc9bf519d981d0324bc6bba042c334c2))
* **manufacturers get search:** command to search for manufacturers ([7c475a5](https://gitlab.com/boring10/sam/commit/7c475a528f35c75b8124e3159c32bff74b030d1c))
* **manufacturers patch byid:** command to update a manufacturer by ID ([8df2be1](https://gitlab.com/boring10/sam/commit/8df2be13f0f5714e50ab3834cd23183dbfca9837))
* **manufacturers post create:** command for creating new manufacturers ([16c8b1c](https://gitlab.com/boring10/sam/commit/16c8b1c7724742fe66a7e23a28a259f5f83b0615))

## [1.3.1](https://gitlab.com/boring10/sam/compare/v1.3.0...v1.3.1) (2021-04-09)


### Bug Fixes

* **option fields:** fix includes to handle entered fields for option better ([5141203](https://gitlab.com/boring10/sam/commit/5141203dfdce265092d18c7e2e01a6e0d327cebe))

# [1.3.0](https://gitlab.com/boring10/sam/compare/v1.2.0...v1.3.0) (2021-04-08)


### Bug Fixes

* **default output:** change from include to exact match for fields ([48075d1](https://gitlab.com/boring10/sam/commit/48075d19e2f585d36a66e79c905353aae0ca7c4f))
* **output csv:** check string (cell) for comma and surround in quotes if it does ([38c5021](https://gitlab.com/boring10/sam/commit/38c502135a3414a576c059714f4510bf5cadb8a1))


### Features

* **accessories delete byid:** command for deleting existing accessories ([72b1fe2](https://gitlab.com/boring10/sam/commit/72b1fe283071b9c0b726bb0c5ad1be0bc624c149))
* **accessories get byid:** command for getting accessories by the ID ([ac187f4](https://gitlab.com/boring10/sam/commit/ac187f411891cf22828169abc1b0ccb56a8b2401))
* **accessories get checkedout byid:** command for getting users that the accessory is checked out ([f44ee37](https://gitlab.com/boring10/sam/commit/f44ee370a997a478086574a40f37477dd01bcc8a))
* **accessories get search:** command to search for existing accessories ([b955f0a](https://gitlab.com/boring10/sam/commit/b955f0ad44def23e925e1896b79f5bffd7153b07))
* **accessories min_amt:** add option for minimum accessory quantity before sending alerts ([db54719](https://gitlab.com/boring10/sam/commit/db54719254c814aaf7fcddcb1897e89bdefc71ed))
* **accessories patch byid:** command for updating existing accessories ([1f070ee](https://gitlab.com/boring10/sam/commit/1f070ee929cdbe59c45912e9c15c38826a81081c))
* **accessories post checkin bypivotid:** command for checking in accessories ([b794a40](https://gitlab.com/boring10/sam/commit/b794a40b4b38cf5af125c8374535717337cad561))
* **accessories post checkout byid:** command for checking out accessories to users ([f03383f](https://gitlab.com/boring10/sam/commit/f03383f43c512746bc47b51e51cf670a444e5dc3))
* **accessories post create:** command for creating new accessories ([0a1304e](https://gitlab.com/boring10/sam/commit/0a1304ec117f7e7e83ff4add6e444ad6aa7f3b80))

# [1.2.0](https://gitlab.com/boring10/sam/compare/v1.1.0...v1.2.0) (2021-04-07)


### Bug Fixes

* **companies-get-search:** update search string to be based off name, if entered ([a68c19e](https://gitlab.com/boring10/sam/commit/a68c19e72765d930a9c38128e4cbf0dca6f1250c))


### Features

* **locations:** commands for managing the locations on Snipe-IT ([1d5b1d5](https://gitlab.com/boring10/sam/commit/1d5b1d59012552632f00e2c79e387abbc3619692))
* **locations ldap_ou:** add ldap_ou option for locations command ([9374e02](https://gitlab.com/boring10/sam/commit/9374e02823f1a1fba75765e2be6b15597e9a39f3))

# [1.1.0](https://gitlab.com/boring10/sam/compare/v1.0.1...v1.1.0) (2021-03-25)


### Features

* **companies-delete-byid:** add ability for updating company information by the unique company ID ([a71c11a](https://gitlab.com/boring10/sam/commit/a71c11a4f7be1ad0364ade8ec0d523fa3c0314e1))
* **companies-get-byid:** add ability for getting company information by the unique company ID ([288fdc1](https://gitlab.com/boring10/sam/commit/288fdc1988ae3fd74a2dcd4d24817faa2facf10b))
* **companies-get-search:** add ability for searching for companies and returning multiple results ([2d7c503](https://gitlab.com/boring10/sam/commit/2d7c5032c46282b32cbcb63b40b3f8660d646628))
* **companies-patch-byid:** add ability for updating company information by the unique company ID ([c3098e8](https://gitlab.com/boring10/sam/commit/c3098e8e00dffc6bf3dd0f077a23b138dbd8a214))
* **companies-post-create:** add ability for creating new companies with a unique name ([3eec415](https://gitlab.com/boring10/sam/commit/3eec41541520b03659a560a8d03d49c97a544035))
* **parserconfiguration:** add parser configuration for yargs ([6559512](https://gitlab.com/boring10/sam/commit/655951226d4dbe1a534a4b5bf36686994cae366a))
* **sam.js:** include a few examples for using the application ([d19662d](https://gitlab.com/boring10/sam/commit/d19662dd09cd12db7ceef0100fa041fbc975f28a))

## [1.0.1](https://gitlab.com/boring10/sam/compare/v1.0.0...v1.0.1) (2021-03-22)


### Bug Fixes

* **remove logging:** remove console.log that was used for verification testing ([c28bfe7](https://gitlab.com/boring10/sam/commit/c28bfe7aeb4e7659a252b914ad0fc1c2a3c69153))

# 1.0.0 (2021-03-20)


### Features

* **init release:** initial release for SAM ([038270e](https://gitlab.com/boring10/sam/commit/038270eb48ef2c17f96f294facaeb6ecee03c2c8))
