# Contributing to SAM

## Reporting a bug

File bugs at [GitLab Issues](https://gitlab.com/boring10/sam/-/issues) and follow the guidelines listed below.

- Search the existing issues to make sure that the bug has not already been reported.
- Describe the issue in detail so that it may be reproducible by others.
  - It is recommended that you use the "Bug Report" template for bug related issues.
- Only report one bug per issue. This will make it easier to resolve specific bugs and help to reduce stale issues.

## Feature requests

File feature requests at [GitLab Issues](https://gitlab.com/boring10/sam/-/issues) and follow the guidelines listed below.

- Search the existing issues to make sure that the feature request has not already been reported.
- Describe the feature in detail.
  - It is recommended that you use the "Feature Request" template for feature related issues.
- Only one feature request per issue. This will make is easier to manage implementing features and help to reduce stale features that may take more time to implement.

## Contributing to SAM code

- Check the [GitLab Issues](https://gitlab.com/boring10/sam/-/issues) for any bug fixes that are needed.
- Before adding new features please submit a feature request in the [GitLab Issues](https://gitlab.com/boring10/sam/-/issues) using the "Feature Request" template.
- Pull requests that make changes to the code, outside of the ones that are used by the project itself, may be rejected. Exceptions will be made if previously discussed and agreed upon.
- If the pull request is for typos in documentation then you do not need to submit a feature request or a bug report.

## Coding style

The coding style is formatted based on a pre-commit hook that utilizes [Prettier](https://prettier.io/) and [eslint](https://eslint.org/). The pre-commit hook is installed after running `npm install` inside of the project directory.

## Commiting pull requests

- Ensure that you have submitted a bug report or feature request that has been reviewed.
- Clone the repo and perform an `npm install`.
- Create a new local branch on your computer. Keep the branch name simple but meaningful.
- Perform the code changes. Code changes should be focused specifically for the feature or bug. Try to keep commits small so that they can be rolled back if need be.
- Stage the files that were changed.
- Perform `git commit` and follow the prompts. The use of [commitizen](https://www.npmjs.com/package/commitizen) is used for structuring and formatting the commits.
